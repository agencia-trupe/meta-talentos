<?php

namespace App\Helpers;

class Tools
{

    public static function trans($termo)
    {
        $locale = app()->getLocale();
        return $locale == 'pt' ? $termo : $termo.'_'.$locale;
    }

}
