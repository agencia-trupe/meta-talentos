<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class OportunidadesRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $rules = [
            'tipo' => 'required',
            'empresa' => 'required',
            'marca' => 'required|image',
            'vaga' => 'required',
            'link' => 'required',
            'status' => 'required',
        ];

        if ($this->method() != 'POST') {
            $rules['marca'] = 'image';
        }

        return $rules;
    }
}
