<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class CasesClientesRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $rules = [
            'empresa' => 'required',
            'marca' => 'required|image',
            'titulo' => 'required',
            'titulo_en' => 'required',
            'texto' => 'required',
            'texto_en' => 'required',
        ];

        if ($this->method() != 'POST') {
            $rules['marca'] = 'image';
        }

        return $rules;
    }
}
