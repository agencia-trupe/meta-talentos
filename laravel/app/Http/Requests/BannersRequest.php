<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class BannersRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $rules = [
            'imagem' => 'required|image',
            'titulo' => 'required',
            'titulo_en' => 'required',
            'texto' => 'required',
            'texto_en' => 'required',
            'texto_botao' => 'required_with:link_botao',
            'texto_botao_en' => 'required_with:link_botao',
            'link_botao' => 'required_with:texto_botao,texto_botao_en',
        ];

        if ($this->method() != 'POST') {
            $rules['imagem'] = 'image';
        }

        return $rules;
    }
}
