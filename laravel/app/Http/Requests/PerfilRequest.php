<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class PerfilRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'texto'       => 'required',
            'texto_en'    => 'required',
            'imagem_1'    => 'image',
            'imagem_1_en' => 'image',
            'imagem_2'    => 'image',
            'imagem_2_en' => 'image',
        ];
    }
}
