<?php

Route::group(['middleware' => ['web']], function () {
    Route::get('/', 'HomeController@index')->name('home');
    Route::get('empresa/perfil', 'PerfilController@index')->name('perfil');
    Route::get('empresa/diferencial', 'DiferencialController@index')->name('diferencial');
    Route::get('solucoes/{solucao_slug?}', 'SolucoesController@index')->name('solucoes');
    Route::get('oportunidades', 'OportunidadesController@index')->name('oportunidades');
    Route::get('cases-clientes', 'CasesClientesController@index')->name('cases-clientes');
    Route::get('contato', 'ContatoController@index')->name('contato');
    Route::post('contato', 'ContatoController@post')->name('contato.post');
    Route::get('politica-de-privacidade', 'PoliticaDePrivacidadeController@index')->name('politica-de-privacidade');

    // Localização
    Route::get('lang/{idioma?}', function ($idioma = 'pt') {
        if ($idioma == 'pt' || $idioma == 'en') {
            Session::put('locale', $idioma);
        }
        return redirect()->route('home');
    })->name('lang');

    // Painel
    Route::group([
        'prefix'     => 'painel',
        'namespace'  => 'Painel',
        'middleware' => ['auth']
    ], function() {
        Route::get('/', 'PainelController@index')->name('painel');

        /* GENERATED ROUTES */
		Route::resource('link-oportunidades', 'LinkOportunidadesController', ['only' => ['index', 'update']]);
		Route::resource('oportunidades', 'OportunidadesController');
		Route::resource('perfil', 'PerfilController', ['only' => ['index', 'update']]);
		Route::resource('diferencial', 'DiferencialController', ['only' => ['index', 'update']]);
		Route::resource('cases-clientes', 'CasesClientesController');
		Route::resource('solucoes', 'SolucoesController', ['only' => ['index', 'edit', 'update']]);
		Route::resource('politica-de-privacidade', 'PoliticaDePrivacidadeController', ['only' => ['index', 'update']]);
		Route::resource('banners', 'BannersController');
        Route::resource('contato/recebidos', 'ContatosRecebidosController');
        Route::resource('contato', 'ContatoController');
        Route::resource('usuarios', 'UsuariosController');

        Route::post('order', 'PainelController@order');
        Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');
    });

    // Auth
    Route::group([
        'prefix'    => 'painel',
        'namespace' => 'Auth'
    ], function() {
        Route::get('login', 'AuthController@showLoginForm')->name('auth');
        Route::post('login', 'AuthController@login')->name('login');
        Route::get('logout', 'AuthController@logout')->name('logout');
    });
});
