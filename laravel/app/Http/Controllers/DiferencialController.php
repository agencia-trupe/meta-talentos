<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Models\Diferencial;

class DiferencialController extends Controller
{
    public function index()
    {
        $diferencial = Diferencial::first();
        return view('frontend.diferencial', compact('diferencial'));
    }
}
