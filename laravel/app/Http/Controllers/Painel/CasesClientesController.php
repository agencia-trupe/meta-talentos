<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\CasesClientesRequest;
use App\Http\Controllers\Controller;

use App\Models\CaseCliente;

class CasesClientesController extends Controller
{
    public function index()
    {
        $registros = CaseCliente::ordenados()->get();

        return view('painel.cases-clientes.index', compact('registros'));
    }

    public function create()
    {
        return view('painel.cases-clientes.create');
    }

    public function store(CasesClientesRequest $request)
    {
        try {

            $input = $request->all();

            if (isset($input['marca'])) $input['marca'] = CaseCliente::upload_marca();

            CaseCliente::create($input);
            return redirect()->route('painel.cases-clientes.index')->with('success', 'Registro adicionado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: '.$e->getMessage()]);

        }
    }

    public function edit(CaseCliente $registro)
    {
        return view('painel.cases-clientes.edit', compact('registro'));
    }

    public function update(CasesClientesRequest $request, CaseCliente $registro)
    {
        try {

            $input = $request->all();

            if (isset($input['marca'])) $input['marca'] = CaseCliente::upload_marca();

            $registro->update($input);
            return redirect()->route('painel.cases-clientes.index')->with('success', 'Registro alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);

        }
    }

    public function destroy(CaseCliente $registro)
    {
        try {

            $registro->delete();
            return redirect()->route('painel.cases-clientes.index')->with('success', 'Registro excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: '.$e->getMessage()]);

        }
    }

}
