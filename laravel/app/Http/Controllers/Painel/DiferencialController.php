<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\DiferencialRequest;
use App\Http\Controllers\Controller;

use App\Models\Diferencial;

class DiferencialController extends Controller
{
    public function index()
    {
        $registro = Diferencial::first();

        return view('painel.diferencial.edit', compact('registro'));
    }

    public function update(DiferencialRequest $request, Diferencial $registro)
    {
        try {
            $input = $request->all();


            $registro->update($input);

            return redirect()->route('painel.diferencial.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {
            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);
        }
    }
}
