<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\LinkOportunidadesRequest;
use App\Http\Controllers\Controller;

use App\Models\LinkOportunidades;

class LinkOportunidadesController extends Controller
{
    public function index()
    {
        $registro = LinkOportunidades::first();

        return view('painel.link-oportunidades.edit', compact('registro'));
    }

    public function update(LinkOportunidadesRequest $request, LinkOportunidades $registro)
    {
        try {
            $input = $request->all();


            $registro->update($input);

            return redirect()->route('painel.link-oportunidades.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {
            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);
        }
    }
}
