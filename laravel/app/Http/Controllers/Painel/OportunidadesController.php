<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\OportunidadesRequest;
use App\Http\Controllers\Controller;

use App\Models\Oportunidade;

class OportunidadesController extends Controller
{
    public function index()
    {
        $registros = Oportunidade::ordenados()->get();

        return view('painel.oportunidades.index', compact('registros'));
    }

    public function create()
    {
        return view('painel.oportunidades.create');
    }

    public function store(OportunidadesRequest $request)
    {
        try {

            $input = $request->all();

            if (isset($input['marca'])) $input['marca'] = Oportunidade::upload_marca();

            Oportunidade::create($input);
            return redirect()->route('painel.oportunidades.index')->with('success', 'Registro adicionado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: '.$e->getMessage()]);

        }
    }

    public function edit(Oportunidade $registro)
    {
        return view('painel.oportunidades.edit', compact('registro'));
    }

    public function update(OportunidadesRequest $request, Oportunidade $registro)
    {
        try {

            $input = $request->all();

            if (isset($input['marca'])) $input['marca'] = Oportunidade::upload_marca();

            $registro->update($input);
            return redirect()->route('painel.oportunidades.index')->with('success', 'Registro alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);

        }
    }

    public function destroy(Oportunidade $registro)
    {
        try {

            $registro->delete();
            return redirect()->route('painel.oportunidades.index')->with('success', 'Registro excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: '.$e->getMessage()]);

        }
    }

}
