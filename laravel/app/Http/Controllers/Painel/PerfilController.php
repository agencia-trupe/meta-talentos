<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\PerfilRequest;
use App\Http\Controllers\Controller;

use App\Models\Perfil;

class PerfilController extends Controller
{
    public function index()
    {
        $registro = Perfil::first();

        return view('painel.perfil.edit', compact('registro'));
    }

    public function update(PerfilRequest $request, Perfil $registro)
    {
        try {
            $input = $request->all();

            if (isset($input['imagem_1'])) $input['imagem_1'] = Perfil::upload_imagem_1();
            if (isset($input['imagem_1_en'])) $input['imagem_1_en'] = Perfil::upload_imagem_1_en();
            if (isset($input['imagem_2'])) $input['imagem_2'] = Perfil::upload_imagem_2();
            if (isset($input['imagem_2_en'])) $input['imagem_2_en'] = Perfil::upload_imagem_2_en();

            $registro->update($input);

            return redirect()->route('painel.perfil.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {
            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);
        }
    }
}
