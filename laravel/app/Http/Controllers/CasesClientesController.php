<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Models\CaseCliente;

class CasesClientesController extends Controller
{
    public function index()
    {
        $cases = CaseCliente::ordenados()->get();
        return view('frontend.cases-clientes', compact('cases'));
    }
}
