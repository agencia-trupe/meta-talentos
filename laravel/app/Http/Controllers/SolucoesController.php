<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Models\Solucao;

class SolucoesController extends Controller
{
    public function index($slug = '')
    {
        $solucoes = Solucao::all();
        $solucao = Solucao::whereSlug($slug)->first() ?: Solucao::first();
        return view('frontend.solucoes', compact('solucoes', 'solucao'));
    }
}
