<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Models\Oportunidade;
use App\Models\LinkOportunidades;

class OportunidadesController extends Controller
{
    public function index()
    {
        $link = LinkOportunidades::first();
        $oportunidades = Oportunidade::ordenados()->get();
        return view('frontend.oportunidades', compact('link', 'oportunidades'));
    }
}
