<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Banner;
use App\Models\Solucao;

class HomeController extends Controller
{
    public function index()
    {
        $banners = Banner::ordenados()->get();
        $solucoes = Solucao::all();

        $urlJson = 'http://www.metanews.com.br/mais-recentes';
        $str = file_get_contents($urlJson);
        $metanews = json_decode($str, true);

        return view('frontend.home', compact('banners', 'solucoes', 'metanews'));
    }
}
