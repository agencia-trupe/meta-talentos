<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

class CaseCliente extends Model
{
    protected $table = 'cases_clientes';

    protected $guarded = ['id'];

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC')->orderBy('id', 'DESC');
    }

    public static function upload_marca()
    {
        return CropImage::make('marca', [
            'width'  => 280,
            'height' => 170,
            'bg'     => '#ffffff',
            'path'   => 'assets/img/cases-clientes/'
        ]);
    }

}
