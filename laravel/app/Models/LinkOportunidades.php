<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

class LinkOportunidades extends Model
{
    protected $table = 'link_oportunidades';

    protected $guarded = ['id'];

}
