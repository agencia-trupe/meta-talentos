<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

class Oportunidade extends Model
{
    protected $table = 'oportunidades';

    protected $guarded = ['id'];

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC')->orderBy('id', 'DESC');
    }

    public static function upload_marca()
    {
        return CropImage::make('marca', [
            'width'  => 175,
            'height' => 90,
            'bg'     => '#ffffff',
            'path'   => 'assets/img/oportunidades/'
        ]);
    }

}
