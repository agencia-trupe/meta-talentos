<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

class Perfil extends Model
{
    protected $table = 'perfil';

    protected $guarded = ['id'];

    public static function upload_imagem_1()
    {
        return CropImage::make('imagem_1', [
            'width'  => 300,
            'height' => null,
            'path'   => 'assets/img/perfil/'
        ]);
    }

    public static function upload_imagem_1_en()
    {
        return CropImage::make('imagem_1_en', [
            'width'  => 300,
            'height' => null,
            'path'   => 'assets/img/perfil/'
        ]);
    }

    public static function upload_imagem_2()
    {
        return CropImage::make('imagem_2', [
            'width'  => 200,
            'height' => null,
            'path'   => 'assets/img/perfil/'
        ]);
    }

    public static function upload_imagem_2_en()
    {
        return CropImage::make('imagem_2_en', [
            'width'  => 200,
            'height' => null,
            'path'   => 'assets/img/perfil/'
        ]);
    }
}
