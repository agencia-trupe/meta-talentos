<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

class Diferencial extends Model
{
    protected $table = 'diferencial';

    protected $guarded = ['id'];

}
