@extends('frontend.common.template')

@section('content')

    <div class="page-title">
        <div class="center">
            <h1>{{ trans('frontend.solucoes') }} <b>||</b> <span>{{ $solucao->{Tools::trans('titulo')} }}</span></h1>
        </div>
    </div>

    <div class="content solucoes">
        <div class="wrapper">
            <div class="center">
                <div class="bg"></div>
                <div class="solucoes-lista">
                    @foreach($solucoes as $s)
                    <a href="{{ route('solucoes', $s->slug) }}" @if($s->slug == $solucao->slug) class="active" @endif>
                        <div class="icone {{ $s->slug }}"></div>
                        <span>{{ $s->{Tools::trans('titulo')} }}</span>
                    </a>
                    @endforeach
                </div>
                <div class="texto">
                    <h1>{{ $solucao->{Tools::trans('subtitulo')} }}</h1>
                    <div>
                        {!! $solucao->{Tools::trans('texto')} !!}
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
