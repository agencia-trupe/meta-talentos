@extends('frontend.common.template')

@section('content')

    <div class="page-title">
        <div class="center">
            <h1>{{ trans('frontend.oportunidades') }}</h1>
        </div>
    </div>

    <div class="content oportunidades">
        <div class="center">
            <a href="{{ $link->link }}" target="_blank" class="btn-oportunidades">
                <p>
                    <span>{{ trans('frontend.confira') }}</span>
                    {{ trans('frontend.todas-oportunidades') }}
                </p>
            </a>

            <h2>{{ trans('frontend.encontre') }}</h2>
            <div class="vagas">
                @foreach($oportunidades as $vaga)
                @if($vaga->status == 'Encerrada')
                <div class="vaga">
                    <span class="tipo">
                        @if($vaga->tipo == 'Trainee')
                        {{ trans('frontend.vaga-trainee') }}
                        @else
                        {{ trans('frontend.vaga-estagio') }}
                        @endif
                    </span>
                    <div class="imagem">
                        <img src="{{ asset('assets/img/oportunidades/'.$vaga->marca) }}" alt="">
                    </div>
                    <span class="texto">{{ $vaga->vaga }}</span>
                    <span class="status encerrada">{{ trans('frontend.encerradas') }} <img src="{{ asset('assets/img/layout/seta-inscricoesencerradas.png') }}" alt=""></span>
                </div>
                @else
                <a href="{{ $vaga->link }}" target="_blank" class="vaga">
                    <span class="tipo">
                        @if($vaga->tipo == 'trainee')
                        {{ trans('frontend.vaga-trainee') }}
                        @else
                        {{ trans('frontend.vaga-estagio') }}
                        @endif
                    </span>
                    <div class="imagem">
                        <img src="{{ asset('assets/img/oportunidades/'.$vaga->marca) }}" alt="">
                    </div>
                    <span class="texto">{{ $vaga->{Tools::trans('vaga')} }}</span>
                    <span class="status aberta">{{ trans('frontend.abertas') }} <img src="{{ asset('assets/img/layout/seta-inscricoesabertas.png') }}" alt=""></span>
                </a>
                @endif
                @endforeach
            </div>
        </div>
    </div>

@endsection
