@extends('frontend.common.template')

@section('content')

    <div class="page-title">
        <div class="center">
            <h1>{{ mb_strtoupper(trans('frontend.politica')) }}</h1>
        </div>
    </div>

    <div class="content privacidade">
        <div class="center">
            {!! $politica->{Tools::trans('texto')} !!}
        </div>
    </div>

@endsection
