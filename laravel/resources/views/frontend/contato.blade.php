@extends('frontend.common.template')

@section('content')

    <div class="content contato">
        <div class="mapa">{!! $contato->google_maps !!}</div>
        <div class="center">
            <form action="" id="form-contato" method="POST">
                <h2><span>{!! trans('frontend.envie-nos') !!}</span></h2>
                <input type="text" name="nome" id="nome" placeholder="{{ trans('frontend.nome') }}" required>
                <input type="email" name="email" id="email" placeholder="E-mail" required>
                <input type="text" name="telefone" id="telefone" placeholder="{{ trans('frontend.telefone') }}">
                <textarea name="mensagem" id="mensagem" placeholder="{{ trans('frontend.mensagem') }}" required></textarea>
                <input type="submit" value="{{ trans('frontend.enviar') }}">
                <div id="form-contato-response"></div>
            </form>
            <div class="info">
                <h2><span>{!! trans('frontend.entre-contato') !!}</span></h2>
                <p class="telefone">
                    <?php $telefone = explode(' ', $contato->telefone); ?>
                    {{ implode(' ', array_slice($telefone, 0, -1)) }}
                    <span>{{ array_pop($telefone) }}</span>
                </p>
                <p class="endereco">{!! $contato->{Tools::trans('endereco')} !!}</p>
                <div class="grupometa">
                    <span>{!! trans('frontend.empresa-do-br') !!}</span>
                    <img src="{{ asset('assets/img/layout/marca-grupo-contato.png') }}" alt="">
                </div>
            </div>
        </div>
    </div>

@endsection
