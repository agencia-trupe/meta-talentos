    <header>
        <div class="info">
            <div class="center">
                <div class="telefone">
                    <?php $telefone = explode(' ', $contato->telefone); ?>
                    {{ implode(' ', array_slice($telefone, 0, -1)) }}
                    <span>{{ array_pop($telefone) }}</span>
                </div>
                <div class="social">
                    @foreach(['facebook', 'twitter', 'linkedin'] as $rede)
                    @if($contato->{$rede})
                        <a href="{{ $contato->{$rede} }}" class="{{ $rede }}" target="_blank">{{ $rede }}</a>
                    @endif
                    @endforeach
                    @if(app()->getLocale() == 'pt')
                    <a href="{{ route('lang', 'en') }}" class="lang lang-en" title="English Version">english</a>
                    @else
                    <a href="{{ route('lang', 'pt') }}" class="lang lang-pt" title="Versão em Português">português</a>
                    @endif
                </div>
            </div>
        </div>
        <div class="main">
            <div class="center">
                <a href="{{ route('home') }}" class="logo">{{ config('site.name') }}</a>
                <nav id="desktop">
                    <a href="{{ route('home') }}" @if(Route::currentRouteName() == 'home') class="active" @endif>
                        <span>HOME</span>
                        <div class="seta"></div>
                    </a>
                    <a href="{{ route('perfil') }}" @if(Route::currentRouteName() == 'perfil' || Route::currentRouteName() == 'diferencial') class="active" @endif>
                        <span>{{ trans('frontend.empresa') }}</span>
                        <div class="seta"></div>
                    </a>
                    <a href="{{ route('solucoes') }}" @if(Route::currentRouteName() == 'solucoes') class="active" @endif>
                        <span>{{ trans('frontend.solucoes') }}</span>
                        <div class="seta"></div>
                    </a>
                    <a href="{{ route('oportunidades') }}" @if(Route::currentRouteName() == 'oportunidades') class="active" @endif>
                        <span>{{ trans('frontend.oportunidades') }}</span>
                        <div class="seta"></div>
                    </a>
                    <a href="{{ route('cases-clientes') }}" @if(Route::currentRouteName() == 'cases-clientes') class="active" @endif>
                        <span>{!! trans('frontend.cases-clientes-br') !!}</span>
                        <div class="seta"></div>
                    </a>
                    <a href="{{ route('contato') }}" @if(Route::currentRouteName() == 'contato') class="active" @endif>
                        <span>{{ trans('frontend.contato') }}</span>
                        <div class="seta"></div>
                    </a>
                </nav>
                <button id="mobile-toggle" type="button" role="button">
                    <span class="lines"></span>
                </button>
            </div>
        </div>
        <nav id="mobile">
            <a href="{{ route('home') }}" @if(Route::currentRouteName() == 'home') class="active" @endif>
                HOME
            </a>
            <a href="{{ route('perfil') }}" @if(Route::currentRouteName() == 'perfil' || Route::currentRouteName() == 'diferencial') class="active" @endif>
                {{ trans('frontend.empresa') }}
            </a>
            <a href="{{ route('solucoes') }}" @if(Route::currentRouteName() == 'solucoes') class="active" @endif>
                {{ trans('frontend.solucoes') }}
            </a>
            <a href="{{ route('oportunidades') }}" @if(Route::currentRouteName() == 'oportunidades') class="active" @endif>
                {{ trans('frontend.oportunidades') }}
            </a>
            <a href="{{ route('cases-clientes') }}" @if(Route::currentRouteName() == 'cases-clientes') class="active" @endif>
                {{ trans('frontend.cases-clientes') }}
            </a>
            <a href="{{ route('contato') }}" @if(Route::currentRouteName() == 'contato') class="active" @endif>
                {{ trans('frontend.contato') }}
            </a>
        </nav>
    </header>
