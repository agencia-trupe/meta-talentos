    <footer>
        <div class="center">
            <div class="col-wrapper">
            <div class="col">
                <a href="{{ route('home') }}">HOME</a>
                <a href="{{ route('perfil') }}">{{ trans('frontend.perfil-meta') }}</a>
                <a href="{{ route('solucoes') }}">{{ trans('frontend.empresas') }}</a>
                <div class="subs">
                    <a href="{{ route('solucoes', 'atracao-e-selecao') }}">{{ trans('frontend.trainees') }}</a>
                    <a href="{{ route('solucoes', 'atracao-e-selecao') }}">{{ trans('frontend.estagiarios') }}</a>
                    <a href="{{ route('solucoes', 'desenvolvimento-de-jovens-talentos') }}">{{ trans('frontend.desenvolvimento') }}</a>
                </div>
            </div>
            <div class="col">
                <a href="{{ route('oportunidades') }}">{{ trans('frontend.oportunidades') }}</a>
                <a href="{{ route('cases-clientes') }}">{{ trans('frontend.cases-clientes') }}</a>
                <a href="{{ route('contato') }}">{{ trans('frontend.contato') }}</a>

                <div class="grupo-meta">
                    <span>{{ trans('frontend.empresa-do') }}</span>
                    <img src="{{ asset('assets/img/layout/marca-grupometa.png') }}" alt="">
                </div>
            </div>
            </div>
            <div class="info">
                <div class="social">
                    @foreach(['facebook', 'twitter', 'linkedin'] as $rede)
                    @if($contato->{$rede})
                        <a href="{{ $contato->{$rede} }}" class="{{ $rede }}" target="_blank">{{ $rede }}</a>
                    @endif
                    @endforeach
                </div>
                <p class="telefone">
                    <?php $telefone = explode(' ', $contato->telefone); ?>
                    {{ implode(' ', array_slice($telefone, 0, -1)) }}
                    <span>{{ array_pop($telefone) }}</span>
                </p>
                <p class="endereco">{!! $contato->{Tools::trans('endereco')} !!}</p>
                <a href="{{ route('politica-de-privacidade') }}" class="politica">{{ trans('frontend.politica') }}</a>
                <div class="copyright">
                    <p>
                        © {{ date('Y') }} {{ config('site.name') }} - {{ trans('frontend.copyright') }}
                    </p>
                    <p>
                        <a href="http://www.trupe.net" target="_blank">{{ trans('frontend.criacao') }}</a>:
                        <a href="http://www.trupe.net" target="_blank">{{ trans('frontend.trupe') }}</a>
                    </p>
                </div>
            </div>
        </div>
    </footer>
