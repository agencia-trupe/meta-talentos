@extends('frontend.common.template')

@section('content')

    <div class="page-title">
        <div class="center">
            <h1>{{ trans('frontend.cases-clientes') }}</h1>
        </div>
    </div>

    <div class="content cases-clientes">
        <div class="listaCases">
            <?php $contador = 0; $abriu = false; ?>

            <span>
            @foreach($cases as $k => $case)
            <?php $contador++; ?>

                <a href="#" title="{{ $case->empresa }}" data-indice="{{ $contador }}" data-imagem="{{ asset('assets/img/cases-clientes/'.$case->marca) }}" data-titulo="{{ $case->{Tools::trans('titulo')} }}" data-texto="{{ htmlentities($case->{Tools::trans('texto')}) }}">
                    <img src="{{ asset('assets/img/cases-clientes/'.$case->marca) }}">
                </a>

            <?php $abriu = true; ?>

            @if($contador == 5)
            </span>
            <?php $contador = 0 ?>
                <div class="detalhes">
                    <div class="center">
                        <div class="setaUp"></div>
                        <div class="imagem"></div>
                        <div class="texto">
                            <h2></h2>
                            <div class="cke"></div>
                        </div>
                    </div>
                </div>
            <span>
            <?php $abriu = false; ?>
            @endif

            @endforeach

            @if($abriu)
            </span>
                <div class="detalhes">
                    <div class="center">
                        <div class="setaUp"></div>
                        <div class="imagem"></div>
                        <div class="texto">
                            <h2></h2>
                            <div class="cke"></div>
                        </div>
                    </div>
                </div>
            <span>
            @endif
            </span>
        </div>
    </div>

@endsection
