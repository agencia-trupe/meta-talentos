@extends('frontend.common.template')

@section('content')

    <div class="page-title">
        <div class="center">
            <h1>{{ trans('frontend.empresa') }} <b>||</b> <span>{{ trans('frontend.perfil') }}</span></h1>
        </div>
    </div>

    <div class="content perfil">
        <div class="banner">
            <div class="frase">
                <div class="center">
                    {{ trans('frontend.texto-perfil') }}
                </div>
            </div>
        </div>
        <div class="center">
            <div class="perfil-content">
                <img src="{{ asset('assets/img/perfil/'.$perfil->{Tools::trans('imagem_1')}) }}" alt="">
                <div class="texto-wrapper">
                    <div class="video-embed">
                        <iframe src="https://youtube.com/embed/AUjqu8qPPJc" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen autoplay="0"></iframe>
                    </div>
                    <div class="texto">
                        {!! $perfil->{Tools::trans('texto')} !!}
                    </div>
                    <img src="{{ asset('assets/img/perfil/'.$perfil->{Tools::trans('imagem_2')}) }}" alt="">
                </div>
            </div>
            <a href="{{ route('diferencial') }}" class="btn">{{ trans('frontend.saiba-diferenciais') }} &raquo;</a>
        </div>
    </div>

@endsection
