@extends('frontend.common.template')

@section('content')

    <div class="home">
        <div class="banners">
            @foreach($banners as $banner)
            <div class="banner" style="background-image:url('{{ asset('assets/img/banners/'.$banner->imagem) }}')">
                <div class="center">
                    <div class="bg"></div>
                    <div class="circulo"></div>
                    <div class="texto">
                        <h2>{!! $banner->{Tools::trans('titulo')} !!}</h2>
                        <p>{!! $banner->{Tools::trans('texto')} !!}</p>
                    </div>
                    @if($banner->link_botao)
                    <a href="{{ $banner->link_botao }}">{{ $banner->{Tools::trans('texto_botao')} }}</a>
                    @endif
                </div>
            </div>
            @endforeach
            <div class="cycle-pager"></div>
        </div>

        <div class="nossas-solucoes">
            <div class="center">
                <div class="bg"></div>
                <p>{{ trans('frontend.conheca-solucoes') }}</p>
                @foreach($solucoes as $s)
                <a href="{{ route('solucoes', $s->slug) }}">
                    <div class="icone {{ $s->slug }}"></div>
                    <div class="titulo"><span>{{ $s->{Tools::trans('titulo')} }}</span></div>
                </a>
                @endforeach
            </div>
        </div>

        <div class="news">
            @include('frontend.metanews')
        </div>
    </div>

@endsection
