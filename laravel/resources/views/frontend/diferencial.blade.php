@extends('frontend.common.template')

@section('content')

    <div class="page-title">
        <div class="center">
            <h1>{{ trans('frontend.empresa') }} <b>||</b> <span>{{ trans('frontend.diferencial') }}</span></h1>
        </div>
    </div>

    <div class="content diferencial">
        <div class="wrapper">
            <div class="center">
                <img src="{{ asset('assets/img/layout/img-diferencial.png') }}" alt="">
                <div class="texto">
                    {!! $diferencial->{Tools::trans('texto')} !!}
                </div>
            </div>
        </div>
        <a href="{{ route('perfil') }}" class="btn">{{ trans('frontend.saiba-perfil') }} &raquo;</a>
    </div>

@endsection
