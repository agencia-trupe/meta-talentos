@extends('painel.common.template')

@section('content')

    <a href="{{ route('painel.oportunidades.index') }}" class="btn btn-sm btn-default">
        &larr; Voltar para Oportunidades
    </a>

    <legend>
        <h2><small>Oportunidades /</small> Editar Link</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.link-oportunidades.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.link-oportunidades.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
