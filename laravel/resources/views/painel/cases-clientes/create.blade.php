@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Cases / Clientes /</small> Adicionar Case / Cliente</h2>
    </legend>

    {!! Form::open(['route' => 'painel.cases-clientes.store', 'files' => true]) !!}

        @include('painel.cases-clientes.form', ['submitText' => 'Inserir'])

    {!! Form::close() !!}

@endsection
