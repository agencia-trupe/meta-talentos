@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Cases / Clientes /</small> Editar Case / Cliente</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.cases-clientes.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.cases-clientes.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
