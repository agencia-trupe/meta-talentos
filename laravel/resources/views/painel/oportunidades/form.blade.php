@include('painel.common.flash')

<div class="form-group">
    {!! Form::label('tipo', 'Tipo') !!}
    {!! Form::select('tipo', ['Estágio' => 'Estágio', 'Trainee' => 'Trainee'], null, ['class' => 'form-control', 'placeholder' => 'Selecione']) !!}
</div>

<div class="form-group">
    {!! Form::label('empresa', 'Empresa') !!}
    {!! Form::text('empresa', null, ['class' => 'form-control']) !!}
</div>

<div class="well form-group">
    {!! Form::label('marca', 'Marca') !!}
@if($submitText == 'Alterar')
    <img src="{{ url('assets/img/oportunidades/'.$registro->marca) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
@endif
    {!! Form::file('marca', ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('vaga', 'Vaga') !!}
    {!! Form::text('vaga', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('link', 'Link') !!}
    {!! Form::text('link', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('status', 'Status') !!}
    {!! Form::select('status', ['Aberta' =>'Aberta', 'Encerrada' => 'Encerrada'], null, ['class' => 'form-control']) !!}
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}

<a href="{{ route('painel.oportunidades.index') }}" class="btn btn-default btn-voltar">Voltar</a>
