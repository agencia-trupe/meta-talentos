@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Oportunidades /</small> Editar Oportunidade</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.oportunidades.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.oportunidades.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
