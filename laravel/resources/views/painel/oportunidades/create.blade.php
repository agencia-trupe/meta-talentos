@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Oportunidades /</small> Adicionar Oportunidade</h2>
    </legend>

    {!! Form::open(['route' => 'painel.oportunidades.store', 'files' => true]) !!}

        @include('painel.oportunidades.form', ['submitText' => 'Inserir'])

    {!! Form::close() !!}

@endsection
