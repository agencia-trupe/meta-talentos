@extends('painel.common.template')

@section('content')

    @include('painel.common.flash')

    <legend>
        <h2>
            Oportunidades
            <div class="btn-group pull-right">
                <a href="{{ route('painel.oportunidades.create') }}" class="btn btn-sm btn-success"><span class="glyphicon glyphicon-plus" style="margin-right:10px;"></span>Adicionar Oportunidade</a>
                <a href="{{ route('painel.link-oportunidades.index') }}" class="btn btn-sm btn-info"><span class="glyphicon glyphicon-link" style="margin-right:10px;"></span>Editar Link</a>
            </div>
        </h2>
    </legend>

    @if(!count($registros))
    <div class="alert alert-warning" role="alert">Nenhum registro encontrado.</div>
    @else
    <table class="table table-striped table-bordered table-hover table-info table-sortable" data-table="oportunidades">
        <thead>
            <tr>
                <th>Ordenar</th>
                <th>Tipo</th>
                <th>Empresa</th>
                <th>Vaga</th>
                <th>Status</th>
                <th class="no-filter"><span class="glyphicon glyphicon-cog"></span></th>
            </tr>
        </thead>

        <tbody>
        @foreach ($registros as $registro)
            <tr class="tr-row" id="{{ $registro->id }}">
                <td>
                    <a href="#" class="btn btn-info btn-sm btn-move">
                        <span class="glyphicon glyphicon-move"></span>
                    </a>
                </td>
                <td>{{ $registro->tipo }}</td>
                <td>{{ $registro->empresa }}</td>
                <td>{{ $registro->vaga }}</td>
                <td>{{ $registro->status }}</td>
                <td class="crud-actions">
                    {!! Form::open([
                        'route'  => ['painel.oportunidades.destroy', $registro->id],
                        'method' => 'delete'
                    ]) !!}

                    <div class="btn-group btn-group-sm">
                        <a href="{{ route('painel.oportunidades.edit', $registro->id ) }}" class="btn btn-primary btn-sm pull-left">
                            <span class="glyphicon glyphicon-pencil" style="margin-right:10px;"></span>Editar
                        </a>

                        <button type="submit" class="btn btn-danger btn-sm btn-delete"><span class="glyphicon glyphicon-remove" style="margin-right:10px;"></span>Excluir</button>
                    </div>

                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>


    @endif

@endsection
