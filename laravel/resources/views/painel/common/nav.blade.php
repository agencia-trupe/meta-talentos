<ul class="nav navbar-nav">
    <li @if(str_is('painel.banners*', Route::currentRouteName())) class="active" @endif>
        <a href="{{ route('painel.banners.index') }}">Banners</a>
    </li>
    <li class="dropdown @if(str_is('painel.perfil*', Route::currentRouteName()) || str_is('painel.diferencial*', Route::currentRouteName())) active @endif">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            Empresa
            <b class="caret"></b>
        </a>
        <ul class="dropdown-menu">
            <li @if(str_is('painel.perfil*', Route::currentRouteName())) class="active" @endif>
                <a href="{{ route('painel.perfil.index') }}">Perfil</a>
            </li>
            <li @if(str_is('painel.diferencial*', Route::currentRouteName())) class="active" @endif>
                <a href="{{ route('painel.diferencial.index') }}">Diferencial</a>
            </li>
        </ul>
    </li>
    <li @if(str_is('painel.solucoes*', Route::currentRouteName())) class="active" @endif>
        <a href="{{ route('painel.solucoes.index') }}">Soluções</a>
    </li>
	<li @if(str_is('painel.oportunidades*', Route::currentRouteName())) class="active" @endif>
		<a href="{{ route('painel.oportunidades.index') }}">Oportunidades</a>
	</li>
    <li @if(str_is('painel.cases-clientes*', Route::currentRouteName())) class="active" @endif>
        <a href="{{ route('painel.cases-clientes.index') }}">Cases / Clientes</a>
    </li>
    <li @if(str_is('painel.politica-de-privacidade*', Route::currentRouteName())) class="active" @endif>
        <a href="{{ route('painel.politica-de-privacidade.index') }}">Política de Privacidade</a>
    </li>
    <li class="dropdown @if(str_is('painel.contato*', Route::currentRouteName())) active @endif">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            Contato
            @if($contatosNaoLidos >= 1)
            <span class="label label-success" style="margin-left:3px;">{{ $contatosNaoLidos }}</span>
            @endif
            <b class="caret"></b>
        </a>
        <ul class="dropdown-menu">
            <li><a href="{{ route('painel.contato.index') }}">Informações de Contato</a></li>
            <li><a href="{{ route('painel.contato.recebidos.index') }}">
                Contatos Recebidos
                @if($contatosNaoLidos >= 1)
                <span class="label label-success" style="margin-left:3px;">{{ $contatosNaoLidos }}</span>
                @endif
            </a></li>
        </ul>
    </li>
</ul>
