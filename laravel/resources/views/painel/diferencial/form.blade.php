@include('painel.common.flash')

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('texto', 'Texto') !!}
            {!! Form::textarea('texto', null, ['class' => 'form-control ckeditor', 'data-editor' => 'diferencial']) !!}
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('texto_en', 'Texto [INGLÊS]') !!}
            {!! Form::textarea('texto_en', null, ['class' => 'form-control ckeditor', 'data-editor' => 'diferencial']) !!}
        </div>
    </div>
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}
