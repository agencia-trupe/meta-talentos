@extends('painel.common.template')

@section('content')

    <legend>
        <h2>Diferencial</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.diferencial.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.diferencial.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
