(function(window, document, $, undefined) {
    'use strict';

    var App = {};

    App.bannersHome = function() {
        var $wrapper = $('.banners');
        if (!$wrapper.length) return;

        $wrapper.cycle({
            slides: '>.banner',
            pagerTemplate: '<a href=#>{{slideNum}}</a>'
        });
    };

    App.mobileToggle = function() {
        var $handle = $('#mobile-toggle'),
            $nav    = $('nav#mobile');

        $handle.on('click touchstart', function(event) {
            event.preventDefault();
            $nav.slideToggle();
            $handle.toggleClass('close');
        });
    };

    App.cases = function() {
        $('.listaCases a').click( function(e){
            e.preventDefault();
            $('.listaCases a.aberto').removeClass('aberto');
            $('.detalhes').removeClass('aberto');
            $(this).addClass('aberto');
            var indice = $(this).attr('data-indice');
            var imagem = $(this).attr('data-imagem');
            var titulo = $(this).attr('data-titulo');
            var texto = $(this).attr('data-texto');
            var target = $(this).parent().next('div.detalhes');
            var miolo = target.find('.center');
            var seta = miolo.find('.setaUp');

            var offset = $(this).position();
            var deslocamento = offset.left + ($(this).width() / 2) - 4;

            seta.css('left', deslocamento+'px');
            miolo.addClass('aberto');
            target.addClass('aberto');

            miolo.find('.imagem').html("<img src='"+imagem+"'>");
            miolo.find('.texto h2').html(titulo);
            miolo.find('.texto .cke').html(texto);
        });

        $(window).on('resize', function() {
            if($('.listaCases a.aberto').length) {
                var seta = $('.detalhes.aberto').find('.setaUp');
                var offset = $('.listaCases a.aberto').position();
                var deslocamento = offset.left + ($('.listaCases a.aberto').width() / 2) - 4;
                seta.css('left', deslocamento+'px');
            }
        });

        $('.listaCases .detalhes .fechar').click( function(){
            $(this).parent().parent().removeClass('aberto');
        });
    };

    App.envioContato = function(event) {
        event.preventDefault();

        var $form     = $(this),
            $response = $('#form-contato-response');

        $response.fadeOut('fast');

        $.ajax({
            type: "POST",
            url: $('base').attr('href') + '/contato',
            data: {
                nome: $('#nome').val(),
                email: $('#email').val(),
                telefone: $('#telefone').val(),
                mensagem: $('#mensagem').val(),
            },
            success: function(data) {
                $response.fadeOut().text(data.message).fadeIn('slow');
                $form[0].reset();
            },
            error: function(data) {
                if (data.responseJSON) {
                    var error = data.responseJSON[Object.keys(data.responseJSON)[0]];
                    $response.fadeOut().text(error).fadeIn('slow');
                }
            },
            dataType: 'json'
        });
    };

    App.init = function() {
        this.cases();
        this.bannersHome();
        this.mobileToggle();
        $('#form-contato').on('submit', this.envioContato);
    };

    $(document).ready(function() {
        App.init();

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
    });

}(window, document, jQuery));
