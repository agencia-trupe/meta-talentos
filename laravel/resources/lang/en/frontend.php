<?php

return [

    'empresa'             => 'COMPANY',
    'solucoes'            => 'SOLUTIONS',
    'oportunidades'       => 'OPPORTUNITIES',
    'cases-clientes'      => 'CASES | CLIENTS',
    'cases-clientes-br'   => 'CASES<br>CLIENTS',
    'contato'             => 'CONTACT',

    'perfil-meta'         => 'META TALENTOS PROFILE',
    'empresas'            => 'FOR COMPANIES',
    'trainees'            => 'Trainees',
    'estagiarios'         => 'Interns',
    'atracao'             => 'Attraction and Selection',
    'desenvolvimento'     => 'Development of Young Talents',
    'empresa-do'          => 'Meta Talentos is a company of',
    'empresa-do-br'       => 'Meta Talentos is<br>a company of',
    'politica'            => 'Privacy Policy',
    'copyright'           => 'All rights reserved',
    'criacao'             => 'Websites Creation',
    'trupe'               => 'Trupe Creative Agency',

    'conheca-solucoes'    => 'KNOW OUR SOLUTIONS:',
    'noticias'            => 'ALL SECTOR NEWS REUNITED IN ONE PLACE',

    'perfil'              => 'PROFILE',
    'texto-perfil'        => 'It’s the Grupo Meta RH division specialized in attraction, selection and development of Young talents.',
    'saiba-diferenciais'  => 'KNOW MORE ABOUT OUR DIFFERENTIATORS',

    'diferencial'         => 'DIFFERENTIATORS',
    'saiba-perfil'        => 'KNOW MORE ABOUT OUR PROFILE',

    'confira'             => 'CHECK OUT',
    'todas-oportunidades' => 'OUR OPPORTUNITIES',
    'encontre'            => 'FIND HERE YOUR INTERN OR TRAINEE VACANCY',
    'encerradas'          => 'APPLICATIONS CLOSED',
    'abertas'             => 'APPLICATIONS AVAILABLE',
    'vaga-trainee'        => 'Trainee Position',
    'vaga-estagio'        => 'Internship Position',

    'envie-nos'           => 'SEND US A <strong>MESSAGE</strong>',
    'nome'                => 'Name',
    'telefone'            => 'Phone',
    'mensagem'            => 'Message',
    'enviar'              => 'SEND',
    'entre-contato'       => 'CONTACT <strong>US</strong>',
    'sucesso'             => 'Message sent successfully!',
    'erro'                => 'Please fill all the fields correctly.',

];
