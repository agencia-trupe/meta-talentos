<?php

return [

    'empresa'             => 'EMPRESA',
    'solucoes'            => 'SOLUÇÕES',
    'oportunidades'       => 'OPORTUNIDADES',
    'cases-clientes'      => 'CASES | CLIENTES',
    'cases-clientes-br'   => 'CASES<br>CLIENTES',
    'contato'             => 'CONTATO',

    'perfil-meta'         => 'PERFIL META TALENTOS',
    'empresas'            => 'PARA EMPRESAS',
    'trainees'            => 'Trainees',
    'estagiarios'         => 'Estagiários',
    'atracao'             => 'Atração e Seleção',
    'desenvolvimento'     => 'Desenvolvimento de Jovens Talentos',
    'empresa-do'          => 'A Meta Talentos é uma empresa do',
    'empresa-do-br'       => 'A Meta Talentos é<br>uma empresa do',
    'politica'            => 'Política de Privacidade',
    'copyright'           => 'Todos os direitos reservados',
    'criacao'             => 'Criação de Sites',
    'trupe'               => 'Trupe Agência Criativa',

    'conheca-solucoes'    => 'CONHEÇA NOSSAS SOLUÇÕES:',
    'noticias'            => 'AS NOTÍCIAS DO SETOR REUNIDAS EM UM SÓ LUGAR',

    'perfil'              => 'PERFIL',
    'texto-perfil'        => 'É a divisão do Grupo Meta RH especializada na atração, seleção e desenvolvimento de jovens talentos.',
    'saiba-diferenciais'  => 'SAIBA MAIS SOBRE NOSSOS DIFERENCIAIS',

    'diferencial'         => 'DIFERENCIAL',
    'saiba-perfil'        => 'SAIBA MAIS SOBRE NOSSO PERFIL',

    'confira'             => 'CONFIRA AQUI',
    'todas-oportunidades' => 'TODAS AS NOSSAS OPORTUNIDADES',
    'encontre'            => 'ENCONTRE AQUI SUA VAGA DE ESTAGIÁRIO OU TRAINEE',
    'encerradas'          => 'INSCRIÇÕES ENCERRADAS',
    'abertas'             => 'INSCRIÇÕES ABERTAS',
    'vaga-trainee'        => 'Vaga de Trainee',
    'vaga-estagio'        => 'Vaga de Estágio',

    'envie-nos'           => 'ENVIE-NOS UMA <strong>MENSAGEM</strong>',
    'nome'                => 'Nome',
    'telefone'            => 'Telefone',
    'mensagem'            => 'Mensagem',
    'enviar'              => 'ENVIAR',
    'entre-contato'       => 'ENTRE EM <strong>CONTATO</strong>',
    'sucesso'             => 'Mensagem enviada com sucesso!',
    'erro'                => 'Preencha todos os campos corretamente',

];
