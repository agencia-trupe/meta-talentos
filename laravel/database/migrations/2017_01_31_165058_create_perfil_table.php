<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePerfilTable extends Migration
{
    public function up()
    {
        Schema::create('perfil', function (Blueprint $table) {
            $table->increments('id');
            $table->text('texto');
            $table->text('texto_en');
            $table->string('imagem_1');
            $table->string('imagem_1_en');
            $table->string('imagem_2');
            $table->string('imagem_2_en');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('perfil');
    }
}
