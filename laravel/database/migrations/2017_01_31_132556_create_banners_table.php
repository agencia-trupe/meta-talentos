<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBannersTable extends Migration
{
    public function up()
    {
        Schema::create('banners', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ordem')->default(0);
            $table->string('imagem');
            $table->text('titulo');
            $table->text('titulo_en');
            $table->text('texto');
            $table->text('texto_en');
            $table->string('texto_botao');
            $table->string('texto_botao_en');
            $table->string('link_botao');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('banners');
    }
}
