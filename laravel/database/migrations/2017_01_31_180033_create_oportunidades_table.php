<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOportunidadesTable extends Migration
{
    public function up()
    {
        Schema::create('oportunidades', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ordem')->default(0);
            $table->string('tipo');
            $table->string('empresa');
            $table->string('marca');
            $table->string('vaga');
            $table->string('vaga_en');
            $table->string('link');
            $table->string('status');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('oportunidades');
    }
}
