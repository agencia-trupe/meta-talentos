<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLinkOportunidadesTable extends Migration
{
    public function up()
    {
        Schema::create('link_oportunidades', function (Blueprint $table) {
            $table->increments('id');
            $table->string('link');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('link_oportunidades');
    }
}
