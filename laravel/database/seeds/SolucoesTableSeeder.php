<?php

use Illuminate\Database\Seeder;

class SolucoesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('solucoes')->insert([
            [
                'titulo'    => 'Atração e Seleção',
                'slug'      => 'atracao-e-selecao',
                'subtitulo' => 'Programas de Estágio e Trainee',
                'texto'     => ''
            ],
            [
                'titulo'    => 'Desenvolvimento de Jovens Talentos',
                'slug'      => 'desenvolvimento-de-jovens-talentos',
                'subtitulo' => 'Workshops & Desenvolvimento',
                'texto'     => ''
            ],
        ]);
    }
}
