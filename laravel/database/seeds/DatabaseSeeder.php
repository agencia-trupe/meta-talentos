<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        $this->call(UserTableSeeder::class);
		$this->call(LinkOportunidadesSeeder::class);
		$this->call(PerfilSeeder::class);
		$this->call(DiferencialSeeder::class);
		$this->call(PoliticaDePrivacidadeSeeder::class);
        $this->call(ContatoTableSeeder::class);
        $this->call(SolucoesTableSeeder::class);

        Model::reguard();
    }
}
