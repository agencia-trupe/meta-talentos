-- phpMyAdmin SQL Dump
-- version 4.5.3.1
-- http://www.phpmyadmin.net
--
-- Host: meta_talentos.mysql.dbaas.com.br
-- Generation Time: 09-Fev-2017 às 13:47
-- Versão do servidor: 5.6.33-79.0-log
-- PHP Version: 5.6.29-0+deb8u1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `meta_talentos`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `ci_sessions`
--

CREATE TABLE `ci_sessions` (
  `session_id` varchar(40) COLLATE utf8_bin NOT NULL DEFAULT '0',
  `ip_address` varchar(16) COLLATE utf8_bin NOT NULL DEFAULT '0',
  `user_agent` varchar(150) COLLATE utf8_bin NOT NULL,
  `last_activity` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `user_data` text COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Estrutura da tabela `clientes`
--

CREATE TABLE `clientes` (
  `id` int(11) NOT NULL,
  `clientes_nome` varchar(255) NOT NULL,
  `clientes_ordem` int(10) NOT NULL,
  `clientes_imagem` varchar(255) NOT NULL,
  `clientes_desafio` text NOT NULL,
  `clientes_solucao` text NOT NULL,
  `clientes_resultados` text NOT NULL,
  `clientes_desde` int(4) NOT NULL,
  `clientes_setor` varchar(255) NOT NULL,
  `clientes_unidade` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `clientes`
--

INSERT INTO `clientes` (`id`, `clientes_nome`, `clientes_ordem`, `clientes_imagem`, `clientes_desafio`, `clientes_solucao`, `clientes_resultados`, `clientes_desde`, `clientes_setor`, `clientes_unidade`) VALUES
(1, 'Baxter', 1, 'baxter-logo.png', '<p>Processo de Recrutamento e Seleção para vagas pontuais de estágio que ocorrem durante todo o ano letivo.</p>', '<p>Direcionamos uma consultora especializada em processos de estágio e trainee para atuar <em>full time</em> nestes processos, juntamente com uma assistente.</p>', '<p>Processos finalizados com êxito, solidificando a parceria para outras posições.</p>', 1984, 'Indústria Médico - Hospitalar', 'Meta Talentos'),
(2, 'Bayer', 2, 'bayer-logo.png', '<p>Processo de Recrutamento & Seleção de estagiários para 60 posições nas unidades de negócios: Health Care, CropScience e Material Science.</p>', '<p>Montamos uma equipe especializada composta por Analistas Bilíngues e Assistentes, trabalhando em tempo integral nestas posições, totalmente alinhada com o processo de Recrutamento & Seleção, desde o levantamento de perfil até a aprovação do candidato, orientando-o no processo de admissão.</p>', '<p>Todas as posições foram fechadas de acordo com prazo estipulado.</p>', 1986, 'Indústria Farmacêutica', 'Meta Talentos'),
(3, 'BECTON DICKINSON', 3, 'bd-logo.png', '<p>Processo de Recrutamento e Seleção para vagas pontuais de estágio que ocorrem durante todo o ano letivo.</p>', '<p>\r\nDirecionamos uma consultora especializada em processos de estágio e trainee para atuar <em>full time</em> nestes processos, juntamente com uma assistente.\r\n</p>', '<p>\r\n100% de assertividade nas contratações, gerando demanda para demais posições na organização.\r\n</p>', 2010, 'Indústria Médico-Hospitalar', 'Meta Talentos'),
(4, 'Diversey', 4, 'diversey-logo.png', '<p>Processo de Recrutamento & Seleção de vagas de estágio para a unidade corporativa e fabril.</p>', '<p>Equipe alinhada com todo o processo de Recrutamento & Seleção do cliente, desde o alinhamento de perfil até a aprovação do candidato, orientando-o no processo de admissão.</p>', '<p>\r\n100% de assertividade nas contratações, gerando demanda para demais posições na organização.\r\n</p>', 1992, 'Indústria Química', 'Meta talentos'),
(5, 'Nalco / Ecolab', 5, 'ecolab-logo.png', '<p>Processo de Recrutamento e Seleção para vagas pontuais de estágio que ocorrem durante todo o ano letivo em São Paulo e demais regiões brasileiras.</p>', '<p>Direcionamos uma consultora especializada em processos de estágio e trainee, juntamente com uma assistente, para atuar <em>full time</em> nestes processos, realizando parceria com universidades de outros Estados, com o objetivo de divulgar as posições e atrair novos talentos.</p>', '<p>100% de assertividade nas contratações, gerando demanda para demais posições na organização.</p>', 1983, 'Indústria Química', 'Meta Talentos'),
(6, 'Novartis', 6, 'novartis-logo.png', '<p>Processo de Recrutamento e Seleção de 10 estagiários em caráter emergencial para a planta fabril em Taboão da Serra, para as áreas: Administrativa, Laboratório, Produção e Logística.</p>', '<p>Direcionamos uma consultora especializada em processos de estágio e trainee para atuar <em>full time</em> neste processo, juntamente com uma assistente.</p>', '<p>As vagas foram fechadas dentro do prazo do acordado, suprindo as expectativas de contratação.</p>', 1984, 'Indústria Farmacêutica', 'Meta Talentos'),
(7, 'Parker', 7, 'parker-logo.png', '<p>Líder mundial da indústria aeroespacial, contratou a Meta Talentos para implementar o 1º Programa de Trainees Corporativos, realizando a seleção de 22 trainees para as principais divisões da empresa.</p>', '<p>Direcionamos um consultor especializado em processos de estágio e trainee para atuar <em>full time</em> nestes processos, realizando parceria com universidades de outros Estados, com o objetivo de divulgar as posições e atrair novos talentos.</p>', '<p>\r\nAssertividade nas contratações, gerando demanda para demais posições na organização.\r\n</p>', 2011, 'Indústria Aeroespacial', 'Meta Talentos'),
(8, 'Duratex', 7, 'duratex.jpg', '<p>Selecionar e recrutar estagiários para ocupar posições em departamento estratégicos na empresa a fim de oferecer grandes desafios e novos conhecimentos.\r\n</p>', '<p>Recrutamento e seleção com a máxima agilidade e qualidade. Pleno alinhamento do perfil da vaga com o perfil do candidato.</p>', '<p>Assertividade nas contratações, gerando demanda para demais posições na organização.<p>\r\n', 2012, 'Produtora de Metais Sanitários\n', 'Meta Talentos');

-- --------------------------------------------------------

--
-- Estrutura da tabela `home_banners`
--

CREATE TABLE `home_banners` (
  `id` int(11) NOT NULL,
  `order` int(11) NOT NULL,
  `titulo` varchar(255) CHARACTER SET utf8 NOT NULL,
  `imagem` varchar(255) CHARACTER SET utf8 NOT NULL,
  `link` varchar(255) CHARACTER SET utf8 NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `home_banners`
--

INSERT INTO `home_banners` (`id`, `order`, `titulo`, `imagem`, `link`) VALUES
(1, 5, 'Clique e confira nossas vagas de estágio!', 'banner-novo.png', 'oportunidades/estagiarios'),
(34, 0, 'Prêmio', 'premio4consecutivo-talentos.png', '#');

-- --------------------------------------------------------

--
-- Estrutura da tabela `home_bannersingles`
--

CREATE TABLE `home_bannersingles` (
  `id` int(11) NOT NULL,
  `order` int(11) NOT NULL,
  `titulo` varchar(255) CHARACTER SET utf8 NOT NULL,
  `imagem` varchar(255) CHARACTER SET utf8 NOT NULL,
  `link` varchar(255) CHARACTER SET utf8 NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `home_bannersingles`
--

INSERT INTO `home_bannersingles` (`id`, `order`, `titulo`, `imagem`, `link`) VALUES
(12, 2, 'Development of Young Professionals', 'banner21.png', 'empresas'),
(11, 1, 'Recruitment and selection of Interns and Trainees', 'banner11.png', 'empresas');

-- --------------------------------------------------------

--
-- Estrutura da tabela `home_programas`
--

CREATE TABLE `home_programas` (
  `id` int(11) NOT NULL,
  `titulo` varchar(255) CHARACTER SET utf8 NOT NULL,
  `imagem` varchar(255) CHARACTER SET utf8 NOT NULL,
  `link` varchar(255) CHARACTER SET utf8 NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `login_attempts`
--

CREATE TABLE `login_attempts` (
  `id` int(11) NOT NULL,
  `ip_address` varchar(40) COLLATE utf8_bin NOT NULL,
  `login` varchar(50) COLLATE utf8_bin NOT NULL,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Extraindo dados da tabela `login_attempts`
--

INSERT INTO `login_attempts` (`id`, `ip_address`, `login`, `time`) VALUES
(1, '200.171.223.52', 'vinicius', '2017-02-07 12:44:32');

-- --------------------------------------------------------

--
-- Estrutura da tabela `newsletters`
--

CREATE TABLE `newsletters` (
  `id` int(11) NOT NULL,
  `newsletters_nome` varchar(255) NOT NULL,
  `newsletters_email` varchar(255) NOT NULL,
  `newsletters_vagas` tinyint(1) NOT NULL,
  `newsletters_novidades` tinyint(1) NOT NULL,
  `newsletters_eventos` tinyint(1) NOT NULL,
  `newsletters_resultados` tinyint(1) NOT NULL,
  `newsletters_noticias` tinyint(1) NOT NULL,
  `newsletters_data_cadastro` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `newsletters`
--

INSERT INTO `newsletters` (`id`, `newsletters_nome`, `newsletters_email`, `newsletters_vagas`, `newsletters_novidades`, `newsletters_eventos`, `newsletters_resultados`, `newsletters_noticias`, `newsletters_data_cadastro`) VALUES
(1, 'Nilton ', 'nilton@trupe.net', 1, 1, 1, 1, 1, 127),
(2, 'Nilton ', 'nilton@trupe.net', 1, 1, 1, 1, 1, 1350904492),
(3, 'Teste', 'mail@example.com', 1, 1, 1, 1, 1, 1351014320),
(4, 'Cristina', 'cristina@metarh.com.br', 1, 1, 1, 1, 1, 1351165712),
(5, 'Roberto jr de almeida', 'robertojunioralmeida@gmail.com', 1, 1, 1, 1, 1, 1357132733),
(6, 'Cristina', 'criscarvalho255@gmail.com', 1, 1, 1, 1, 1, 1357666258),
(7, 'Ewerton', 'ton_marques_376@hotmail.com', 1, 1, 1, 1, 1, 1358356593),
(8, 'Thais Pamella Souza de Melo', 'thaispamela95@hotmail.com', 1, 1, 1, 1, 1, 1358365116),
(9, 'hugo massao', 'massaohugo@gmail.com', 1, 1, 1, 1, 1, 1358901666),
(10, 'hugo massao', 'massaohugo@gmail.com', 1, 1, 1, 1, 1, 1358901677),
(11, 'Idenilson Aparecido de Oliveira', 'oliverde07@gmail.com', 1, 1, 1, 1, 1, 1358914191),
(12, 'Idenilson Aparecido de Oliveira', 'oliverde07@gmail.com', 1, 1, 1, 1, 1, 1358914204),
(13, 'Priscilla Vieira', 'priscilla_vieira210@hotmail.com', 1, 1, 1, 1, 1, 1358974951),
(14, 'Deise ', 'deiseahta@yahoo.com.br', 1, 1, 1, 1, 1, 1359822554),
(15, 'REGINA NEMER', 're.nemer@hotmail.com', 1, 1, 1, 1, 1, 1360006708),
(16, 'amarildo ferreira', 'amarildoboy@hotmail.com', 1, 1, 1, 1, 1, 1360512868),
(17, 'Sue Ellen Cappatto', 'sue.ellencpt@folha.com.br', 1, 1, 1, 1, 1, 1361888000),
(18, 'Fabiana Abraham Cuore', 'fabiana.cuore@yahoo.com.br', 1, 1, 1, 1, 1, 1362261154),
(19, 'felipe de santana cunha', 'felipe.cunha28@hotmail.com', 1, 1, 1, 1, 1, 1362497339),
(20, 'edilaine', 'edilainemais@hotmail.com', 1, 1, 1, 1, 1, 1362521138),
(21, 'Daniela Alves', 'daniela_c.alves@hotmail.com', 1, 1, 1, 1, 1, 1362526554),
(22, 'Daniela Alves', 'daniela_c.alves@hotmail.com', 1, 1, 1, 1, 1, 1362526564),
(23, 'Daniela', 'danyelalo@hotmail.com', 1, 1, 1, 1, 1, 1362593589),
(24, 'Diogo Silva', 'diogoalves.silva@bol.com.br', 1, 1, 1, 1, 1, 1362683365),
(25, 'Natalia Martinelli', 'naathi.oliveira02@hotmail.com', 1, 1, 1, 1, 1, 1363116462),
(26, 'Renata', 'rtfilipe@gmail.com', 1, 1, 1, 1, 1, 1363182571),
(27, 'Priscila Godê de Almeida Lima', 'priscilagode@hotmail.com', 1, 1, 1, 1, 1, 1363183796),
(28, 'Priscila Godê de Almeida Lima', 'priscilagode@hotmail.com', 1, 1, 1, 1, 1, 1363183802),
(29, 'Danilo paz de aquino', 'zidanilo_danilo@hotmail.com', 1, 1, 1, 1, 1, 1363218430),
(30, 'douglas', 'douglasantana10@yahoo.com.br', 1, 1, 1, 1, 1, 1363305941),
(31, 'Vanessa Lins', 'vlbatista@live.com', 1, 1, 1, 1, 1, 1363345599),
(32, 'Matheus Silva Pinheiro', 'matheussilvapp@hotmail.com', 1, 1, 1, 1, 1, 1363361931),
(33, 'Laila Pereira Lopes', 'lailalopes@terra.com.br', 1, 1, 1, 1, 1, 1363395865),
(34, 'JONAS RODRIGUES', 'jonas.rds@hotmail.com', 1, 1, 1, 1, 1, 1363622619),
(35, 'Jéssica Maldonado Carlos', 'jessimaldonado.c@gmail.com', 1, 1, 1, 1, 1, 1363709347),
(36, 'danny', 'daniele.v.m@hotmail.com', 1, 1, 1, 1, 1, 1363785702),
(37, 'danny', 'daniele.v.m@hotmail.com', 1, 1, 1, 1, 1, 1363785750),
(38, 'Rodrigo', 'rodrigofrancoadm@hotmail.com', 1, 1, 1, 1, 1, 1363802858),
(39, 'Jaqueline dos Reis Silva', 'jaquereis2006@yahoo.com.br', 1, 1, 1, 1, 1, 1363881787),
(40, 'Nathália de Cássia', 'naahos@hotmail.com', 1, 1, 1, 1, 1, 1363889450),
(41, 'Nathália de Cássia', 'naahos@hotmail.com', 1, 1, 1, 1, 1, 1363889460),
(42, 'Elielson Batista Pinheiro', 'elielsonleko@yahoo.com.br', 1, 1, 1, 1, 1, 1363997753),
(43, 'Guatavo Henrique Neri dos Santos', 'thegu100@hotmail.com', 1, 1, 1, 1, 1, 1364232454),
(44, 'Guatavo Henrique Neri dos Santos', 'thegu100@hotmail.com', 1, 1, 1, 1, 1, 1364232513),
(45, 'suellen_sanfe@hotmail.com', 'suellen_sanfe@hotmail.com', 1, 1, 1, 1, 1, 1364251424),
(46, 'Lucas da Silva de Paiva', 'lucaspaiva7@yahoo.com.br', 1, 1, 1, 1, 1, 1364256727),
(47, 'Lucas da Silva de Paiva', 'lucaspaiva7@yahoo.com.br', 1, 1, 1, 1, 1, 1364256728),
(48, 'Lucas da Silva de Paiva', 'lucaspaiva7@yahoo.com.br', 1, 1, 1, 1, 1, 1364256728),
(49, 'Lucas da Silva de Paiva', 'lucaspaiva7@yahoo.com.br', 1, 1, 1, 1, 1, 1364256728),
(50, 'Lucas da Silva de Paiva', 'lucaspaiva7@yahoo.com.br', 1, 1, 1, 1, 1, 1364256728),
(51, 'Lucas da Silva de Paiva', 'lucaspaiva7@yahoo.com.br', 1, 1, 1, 1, 1, 1364256728),
(52, 'dany', 'daniele.v.m@hotmail.com', 1, 1, 1, 1, 1, 1364331066),
(53, 'Rodrigo Ferreira', 'fantasmaro@ig.com.br', 1, 1, 1, 1, 1, 1364470264),
(54, 'Simone', 'bitsimone@hotmail.com', 1, 1, 1, 1, 1, 1364474358),
(55, 'Anauhara Nagatani', 'fulvianagata@hotmail.com', 1, 1, 1, 1, 1, 1364479241),
(56, 'Michelle Sales da Costa', 'michellesalessz@hotmail.com', 1, 1, 1, 1, 1, 1364513196),
(57, 'Michelle Sales da Costa', 'michellesalessz@hotmail.com', 1, 1, 1, 1, 1, 1364513283),
(58, 'Michelle Sales da Costa', 'michellesalessz@hotmail.com', 1, 1, 1, 1, 1, 1364513339),
(59, 'Gabrielly  Sanches', 'gabrielly.sanches0506@gmail.com', 1, 1, 1, 1, 1, 1364965632),
(60, 'liliane gomes', 'lilianelira@hotmail.com', 1, 1, 1, 1, 1, 1365002186),
(61, 'wilque', 'wilque_souza@hotmail.com', 1, 1, 1, 1, 1, 1365043416),
(62, 'Vanessa', 'familiarodrigues5@hotmail.com', 1, 1, 1, 1, 1, 1365052861),
(63, 'AMANDA FELISBERTO NÍMIA', 'amanda.nimia@ig.com.br', 1, 1, 1, 1, 1, 1365084050),
(64, 'Isabela Figuti', 'isabela_figuti@hotmail.com', 1, 1, 1, 1, 1, 1365130058),
(65, 'Aline', 'lynnealves_12@hotmail.com', 1, 1, 1, 1, 1, 1365135672),
(66, 'Michelle Vanessa Fernandes', 'michellemvf@hotmail.com', 1, 1, 1, 1, 1, 1365166928),
(67, 'Michelle Vanessa Fernandes', 'michellemvf@hotmail.com', 1, 1, 1, 1, 1, 1365166932),
(68, 'Felipe João Mendes dos Santos', 'felipe_mendes13@hotmail.com', 1, 1, 1, 1, 1, 1365431214),
(69, 'GiovannaMendes', 'giovannamendes313@gmail.com.br', 1, 1, 1, 1, 1, 1365443181),
(70, 'JULIANA APARECIDA FELIX DA SILVA', 'juuh_felix15@hotmail.com', 1, 1, 1, 1, 1, 1365548883),
(71, 'ROSE SILVA', 'ROSE-SILVA1974@HOTMAIL.COM', 1, 1, 1, 1, 1, 1365793355),
(72, 'lucaspaiva7@yahoo.com.br', 'lucaspaiva7@yahoo.com.br', 1, 1, 1, 1, 1, 1365870523),
(73, 'Francisco de Oliveira', 'foxenglish@hotmail.com', 1, 1, 1, 1, 1, 1366048815),
(74, 'Loryane Dos Santos ', 'lory.poby@hotmail.com', 1, 1, 1, 1, 1, 1366222874),
(75, 'Fernanda Yasmin Ferreira', 'nanda_loveyas@hotmail.com', 1, 1, 1, 1, 1, 1366301345),
(76, 'alisson', 'alisson.mirandas@yahoo.com.br', 1, 1, 1, 1, 1, 1366324106),
(77, 'ÍTALA ALVES DA SILVA', 'tyta_silva1@yahoo.com.br', 1, 1, 1, 1, 1, 1366725575),
(78, 'Maira Pires', 'mairapirescarneiro@gmail.com', 1, 1, 1, 1, 1, 1366992293),
(79, 'Vaukelly da Silva', 'vau_kelly@yahoo.com.br', 1, 1, 1, 1, 1, 1367349878),
(80, 'Carolina Alves', 'carolina.18alves@gmail.com', 1, 1, 1, 1, 1, 1367499471),
(81, 'Amanda Feitoza', 'feitoza.as@gmail.com', 1, 1, 1, 1, 1, 1367708122),
(82, 'William Rodrigues', 'william_eng@hotmail.com', 1, 1, 1, 1, 1, 1367936755),
(83, 'Letícia Cabral Santos', 'leticia.cabrals@hotmail.com', 1, 1, 1, 1, 1, 1367947509),
(84, 'Beatriz Barbosa', 'beatriz.r.barbosa@hotmail.com', 1, 1, 1, 1, 1, 1367952422),
(85, 'marina', 'marina_imamura@hotmail.com', 1, 1, 1, 1, 1, 1367955430),
(86, 'Fabio Jose', 'fabio-miguel50@hotmail.com', 1, 1, 1, 1, 1, 1367967604),
(87, 'Fernanda Santos', 'nan_sants@yahoo.com.br', 1, 1, 1, 1, 1, 1368014006),
(88, 'Aline', 'aline_s_92@hotmail.com', 1, 1, 1, 1, 1, 1368114615),
(89, 'Julyana', 'julyana.rocha995@hotmail.com', 1, 1, 1, 1, 1, 1368118877),
(90, 'WILLIAM DE SOUZA ROCHA', 'william_s.r@hotmail.com', 1, 1, 1, 1, 1, 1368121181),
(91, 'Julio Cesar Valdivia da Silva', 'julio-valdivia@hotmail.com', 1, 1, 1, 1, 1, 1368497384),
(92, 'BRUNO', 'bruno.menezes@clm-med.com.br', 1, 1, 1, 1, 1, 1368723158),
(93, 'Mauricio Lima', 'biruttus@hotmail.com', 1, 1, 1, 1, 1, 1368829306),
(94, 'anntonio  lima', 'adventure_lima@hotmail.com', 1, 1, 1, 1, 1, 1368829328),
(95, 'maxima   coltro', 'mxcoltro@ig.com.br', 1, 1, 1, 1, 1, 1368829348),
(96, 'Katiane Pereira da Silva Lima', 'kathy_bbg1@hotmail.com', 1, 1, 1, 1, 1, 1368848413),
(97, 'Gabriela Carneiro', 'gabrielaolicarneiro@gmail.com', 1, 1, 1, 1, 1, 1368887552),
(98, 'Gabriela Carneiro', 'gabrielaolicarneiro@gmail.com', 1, 1, 1, 1, 1, 1368887558),
(99, 'veronica', 'veronyka.freire@hotmail.com', 1, 1, 1, 1, 1, 1368919779),
(100, 'Ana Paula Majer', 'paula.mith@yahoo.com.br', 1, 1, 1, 1, 1, 1368925514),
(101, 'JESSICA', 'jessicaerenato.2013@facebook.com', 1, 1, 1, 1, 1, 1368926519),
(102, 'Everton Bruno', 'hewertombruno@yahoo.com.br', 1, 1, 1, 1, 1, 1368994655),
(103, 'Renato de Oliveira Costa', 'renatocostas10@hotmail.com', 1, 1, 1, 1, 1, 1369013507),
(104, 'Felipe Ramos', 'epileframos@gmail.com', 1, 1, 1, 1, 1, 1369073449),
(105, 'Felipe Ramos', 'epileframos@gmail.com', 1, 1, 1, 1, 1, 1369073453),
(106, 'Edianny', 'ediannyalves@gmail.com', 1, 1, 1, 1, 1, 1369078172),
(107, 'Luis Fernanado', 'luis-nando03@hotmail.com', 1, 1, 1, 1, 1, 1369097042),
(108, 'clayton', 'clayton_souza05@hotmail.com', 1, 1, 1, 1, 1, 1369153819),
(109, 'LIGIA DA SILVA CESAR', 'ligia_lil@hotmail.com', 1, 1, 1, 1, 1, 1369160701),
(110, 'Thais Rodrigues', 'thaisdp.20@hotmail.com', 1, 1, 1, 1, 1, 1369245149),
(111, 'Jane Qin', 'jaaneqin@hotmail.com', 1, 1, 1, 1, 1, 1369255239),
(112, 'leonardo', 'leo.carvalho.uch@gmail.com', 1, 1, 1, 1, 1, 1369527087),
(113, 'Tatiana Aciole Barbosa', 'thatybonielly@hotmail.com', 1, 1, 1, 1, 1, 1369673736),
(114, 'Thaisa', 'thaisafd@hotmail.com', 1, 1, 1, 1, 1, 1369677819),
(115, 'vanessa conceição da silva', 'van_vana@ig.com.br', 1, 1, 1, 1, 1, 1369704988),
(116, 'Cibele', 'cibele.lefran@hotmail.com', 1, 1, 1, 1, 1, 1369716466),
(117, 'Daniela Pandolpho', 'daniela.pandolpho@yahoo.com.br', 1, 1, 1, 1, 1, 1369846353),
(118, 'Jailson', 'jailson_1201@hotmail.com', 1, 1, 1, 1, 1, 1369873764),
(119, 'Wania', 'Wp.piccinin@gmail.com', 1, 1, 1, 1, 1, 1369914430),
(120, 'gabriela Gomes de Oliveira', 'gabrielagomes1293@gmail.com', 1, 1, 1, 1, 1, 1369936754),
(121, 'Amanda gleice nascimento da silva', 'amanda_gns@hotmail.com', 1, 1, 1, 1, 1, 1369936823),
(122, 'Amanda gleice nascimento da silva', 'amanda_gns@hotmail.com', 1, 1, 1, 1, 1, 1369936827),
(123, 'Renata Novaes', 'renata.novaes@elmo.com.br', 1, 1, 1, 1, 1, 1370012267),
(124, 'Carolina Queiroz', 'carolpaula_silva@hotmail.com', 1, 1, 1, 1, 1, 1370018363),
(125, 'Aline Vitorino Camilo', 'alineleccioli@hotmail.com', 1, 1, 1, 1, 1, 1370204545),
(126, 'Aline Vitorino Camilo', 'alineleccioli@hotmail.com', 1, 1, 1, 1, 1, 1370204555),
(127, 'Rafael Nantes', 'nantesrp@gmail.com', 1, 1, 1, 1, 1, 1370226085),
(128, 'Wellington Rabanal Espinoza', 'wellingtonespinoza.ra@hotmail.com', 1, 1, 1, 1, 1, 1370288432),
(129, 'Flavio', 'flavio-vips@hotmail.com', 1, 1, 1, 1, 1, 1370314885),
(130, 'Flavio', 'flavio-vips@hotmail.com', 1, 1, 1, 1, 1, 1370314892),
(131, 'Fernanda', 'fernanda.sil.castro@hotmail.com', 1, 1, 1, 1, 1, 1370445478),
(132, 'Fernanda', 'fernanda.sil.castro@hotmail.com', 1, 1, 1, 1, 1, 1370445484),
(133, 'Karina Soares da Silva', 'karynaz@hotmail.com', 1, 1, 1, 1, 1, 1370449344),
(134, 'mayra', 'mayra.ramos1994@hotmail.com', 1, 1, 1, 1, 1, 1370455266),
(135, 'camilla s. b. de camargo', 'camargo.camilla@hotmail.com', 1, 1, 1, 1, 1, 1370872643),
(136, 'Jéssica', 'jessica_nira@hotmail.com', 1, 1, 1, 1, 1, 1370890340),
(137, 'Vinicius Alexander Maia Gomes', 'sr.valdeci@hotmail.com', 1, 1, 1, 1, 1, 1371554013),
(138, 'Silvia Rocha ', 'silvia_rocha_10@hotmail.com', 1, 1, 1, 1, 1, 1371755029),
(139, 'Aline', 'lynnealves_12@hotmail.com', 1, 1, 1, 1, 1, 1372008482),
(140, 'Caroline Silva de Araújo Macena', 'carolinne.4ever@hotmail.com', 1, 1, 1, 1, 1, 1372019412),
(141, 'katiane', 'kathy_bbg1@hotmail.com', 1, 1, 1, 1, 1, 1372100670),
(142, 'Daniela Ribeiro Sant Ana', 'danielarsant@yahoo.com.br', 1, 1, 1, 1, 1, 1372164529),
(143, 'Fagner Jose Edson de Queiroz Lima', 'fagner.limaa@hotmail.com', 1, 1, 1, 1, 1, 1372198920),
(144, 'Michelle Sales da Costa', 'michellesalessz@hotmail.com', 1, 1, 1, 1, 1, 1372280433),
(145, 'Michelle Sales da Costa', 'michellesalessz@hotmail.com', 1, 1, 1, 1, 1, 1372280535),
(146, 'Aline', 'aliny_mlb@hotmail.com', 1, 1, 1, 1, 1, 1372290974),
(147, 'Amanda Santana de Lima', 'ama-ee@hotmail.com', 1, 1, 1, 1, 1, 1372357575),
(148, 'Victor Augusto Vieira', 'victorv_vieira@hotmail.com', 1, 1, 1, 1, 1, 1372389516),
(149, 'Thais Rio Branco Da Silva', 'thaisrb40@hotmail.com', 1, 1, 1, 1, 1, 1372720798),
(150, 'Julia Toshiko Hissatomi', 'juliahissatomi@hotmail.com', 1, 1, 1, 1, 1, 1372776801),
(151, 'Camila', 'silva.camila2003@ig.com.br', 1, 1, 1, 1, 1, 1372899364),
(152, 'Camila', 'silva.camila2003@ig.com.br', 1, 1, 1, 1, 1, 1372899364),
(153, 'Elaine', 'laine_alkadyma@hotmail.com', 1, 1, 1, 1, 1, 1373050506),
(154, 'Patrícia Mercês Nogueira', 'patriciamerces@ymail.com', 1, 1, 1, 1, 1, 1373199473),
(155, 'GABRIELA ', 'gabriela.calmeida@bol.com.br', 1, 1, 1, 1, 1, 1373473182),
(156, 'Cristiane', 'cristiane.speedy@itelefonica.com.br', 1, 1, 1, 1, 1, 1373556924),
(157, 'GERCILAINE', 'gercilaineoliveira@yahoo.com.br', 1, 1, 1, 1, 1, 1373591021),
(158, 'Gisele', 'giselle.thuller@gmail.com', 1, 1, 1, 1, 1, 1373998362),
(159, 'Vinicius da Silva Teixeira', 'visilva.teixeira@gmail.com', 1, 1, 1, 1, 1, 1374020073),
(160, 'Gabriel ', 'gabrielsousas92@hotmail.com', 1, 1, 1, 1, 1, 1374577560),
(161, 'Aline Beatriz Moreira Dias da Conceição ', 'alinebeatriz.dias@yahoo.com.br', 1, 1, 1, 1, 1, 1374695396),
(162, 'José Henrique', 'jose.moraes1995@hotmail.com', 1, 1, 1, 1, 1, 1375137415),
(163, 'Edna de Azevedo', 'ednaazevedo18@yahoo.com.br', 1, 1, 1, 1, 1, 1375211479),
(164, 'maria joscilene de assis cobo', 'joscilene26@hotmail.com', 1, 1, 1, 1, 1, 1375639030),
(165, 'sabrina', 'bilookforyou@hotmail.com', 1, 1, 1, 1, 1, 1375724079),
(166, 'Cristiano', 'cristiano.nascimento12@gmail.com', 1, 1, 1, 1, 1, 1375824304),
(167, 'Nathalia  Melo ', 'Nathaliat.mello@hotmail.com', 1, 1, 1, 1, 1, 1376452558),
(168, 'Diego santos bezerra', 'diego.santosbezerra@hotmail.com', 1, 1, 1, 1, 1, 1376683580),
(169, 'Pamela', 'pamela.guedes@hotmail.com', 1, 1, 1, 1, 1, 1376965188),
(170, 'edson Gonçalves Medeiros', 'edson_medeiros2006@hotmail.com', 1, 1, 1, 1, 1, 1377036534),
(171, 'Cintia Daniele Evangelista de Souza', 'cintia_des@hotmail.com', 1, 1, 1, 1, 1, 1377215371),
(172, 'Cybelle Jurgenfeld Campos ', 'cybellejurgenfeld@gmail.com', 1, 1, 1, 1, 1, 1377706294),
(173, 'Vitor Botini Guvasta', 'vitor.guvasta@hotmail.com', 1, 1, 1, 1, 1, 1377879141),
(174, 'Luiza Mayumi Teodoro', 'luiza.mayumit@gmail.com', 1, 1, 1, 1, 1, 1378323285),
(175, 'Gilson Pirozzi', 'gilsonpirozzi@gmail.com', 1, 1, 1, 1, 1, 1378392554),
(176, 'FRANCIELE DOS SANTOS SOUZA', 'ssouza.franciele@gmail.com', 1, 1, 1, 1, 1, 1378593024),
(177, 'Fernando Henrique Santos De Castro', 'fernandocastro1987@hotmail.com', 1, 1, 1, 1, 1, 1378832927),
(178, 'Diego Reolon', 'diegoreolon@hotmail.com', 1, 1, 1, 1, 1, 1379004291),
(179, 'Roger Abdala', 'valentim.abdala1@gmail.com', 1, 1, 1, 1, 1, 1379076635),
(180, 'WILLIAM MORAES', 'izabel.gomes@segurosunimed.com.br', 1, 1, 1, 1, 1, 1379330433),
(181, 'simone martins', 'monymartins7@gmail.com', 1, 1, 1, 1, 1, 1379356408),
(182, 'Taina Barros Comelli', 'taina.comelli@hotmail.com', 1, 1, 1, 1, 1, 1379444429),
(183, 'Brenda Teixeira Marinho', 'brenda.marinho@yahoo.com.br', 1, 1, 1, 1, 1, 1379785294),
(184, 'Catarina Diegues', 'catarinadiegues@hotmail.com', 1, 1, 1, 1, 1, 1380075916),
(185, 'Francelino', 'engenharia.novais@hotmail.com', 1, 1, 1, 1, 1, 1380555844),
(186, 'patricia', 'patriciabratiliere@bol.com.br', 1, 1, 1, 1, 1, 1380664926),
(187, 'ADAILTON RODRIGUES RAMOS', 'r_adailton@terra.com.br', 1, 1, 1, 1, 1, 1380681016),
(188, 'Jaqueline Lima', 'jaque17_lima@hotmail.com', 1, 1, 1, 1, 1, 1380811665),
(189, 'Joab Silva', 'siilva.joab@gmail.com', 1, 1, 1, 1, 1, 1382113664),
(190, 'Joab Silva', 'silva.joab@ymail.com', 1, 1, 1, 1, 1, 1382113679),
(191, 'Rosenilde costa neres', 'rosenilde_2012@hotmail.com', 1, 1, 1, 1, 1, 1382221203),
(192, 'Angelo Padovam', 'angelo.padovam@gmail.com', 1, 1, 1, 1, 1, 1382379046),
(193, 'Su', 'carvalho12527@gmail.com', 1, 1, 1, 1, 1, 1383261317),
(194, 'Caren', 'caren_silfer@hotmail.com', 1, 1, 1, 1, 1, 1383657985),
(195, 'Miguel Hernandez', 'miguelhsouza@gmail.com', 1, 1, 1, 1, 1, 1383673539),
(196, 'Bruno Teixeira de Oliveira', 'bbbbbbto@hotmail.com', 1, 1, 1, 1, 1, 1385041656),
(197, 'Samuel Carvalho', 'carvalho.comercial@bol.com.br', 1, 1, 1, 1, 1, 1385307622),
(198, 'Dário Santos', 'dario_fluir.sp@hotmail.com', 1, 1, 1, 1, 1, 1385316602),
(199, 'Larissa', 'larissa.2704@hotmail.com', 1, 1, 1, 1, 1, 1386795424),
(200, 'mayara', 'mayarafgoncalves@ig.com.br', 1, 1, 1, 1, 1, 1388610883),
(201, 'Mônica Gabriela do Santo', 'mgabisanto@yahoo.com.br', 1, 1, 1, 1, 1, 1390267115),
(202, 'Cleber', 'cleber.pess@gmail.com', 1, 1, 1, 1, 1, 1390908061),
(203, 'Ariane Pâmila', 'anniep_13@hotmail.com', 1, 1, 1, 1, 1, 1391252704),
(204, 'Priscila figlioli', 'priscilafigliolirizzini@hotmail.com', 1, 1, 1, 1, 1, 1391513311),
(205, 'Hosana Barbosa', 'hsbarbosac@gmail.com', 1, 1, 1, 1, 1, 1391653738),
(206, 'Rahyssa Bruzaferro', 'rahyssarezende@gmail.com', 1, 1, 1, 1, 1, 1391710604),
(207, 'marcelo rufino', 'marusouza5@hotmail.com', 1, 1, 1, 1, 1, 1392038782),
(208, 'MARCO ANTONIO DA SILVA', 'marcao.arquitetura@gmail.com', 1, 1, 1, 1, 1, 1392125792),
(209, 'Dieisi', 'Di3isi@hotmail.com', 1, 1, 1, 1, 1, 1392229424),
(210, 'VALDENILTON SILVA SOUSA', 'valdenilton.silva@yahoo.com.br', 1, 1, 1, 1, 1, 1392289182),
(211, 'Amanda Ribeiro', 'amanda-rap@hotmail.com', 1, 1, 1, 1, 1, 1392464354),
(212, 'Bruna', 'brunaluna@yahoo.com.br', 1, 1, 1, 1, 1, 1392654438),
(213, 'cristina Maria ', 'cris_m_oliver@hotmail.com', 1, 1, 1, 1, 1, 1393013366),
(214, 'elisabeth cristina machado de oliveira', 'elisabeth_rr@hotmail.com', 1, 1, 1, 1, 1, 1393049452),
(215, 'ALESSANDRA BATISTA', 'allessandra@ig.com.br', 1, 1, 1, 1, 1, 1393251466),
(216, 'JULIAN FRANCISCO VICENTE RODRIGUES', 'juliannfrancisco@hotmail.com', 1, 1, 1, 1, 1, 1393257590),
(217, 'Sabrina Sabino', 'sabrina.ssabino@gmail.com', 1, 1, 1, 1, 1, 1393340923),
(218, 'Débora Ferreira de Lima', 'debora.dlima@yahoo.com.br', 1, 1, 1, 1, 1, 1393416489),
(219, 'Daniele', 'danni.mendes.c@gmail.com', 1, 1, 1, 1, 1, 1394287266),
(220, 'Key Murakami', 'key.m91@gmail.com', 1, 1, 1, 1, 1, 1394568022),
(221, 'Magali', 'tulypanegra41@gmail.com', 1, 1, 1, 1, 1, 1394621048),
(222, 'lays', 'lah_pac@hotmail.com', 1, 1, 1, 1, 1, 1394663723),
(223, 'Bruno Scolezi Macedo', 'bruno-scolezi@hotmail.com', 1, 1, 1, 1, 1, 1395180098),
(224, 'Fabiana Rampani', 'fabianarampani@hotmail.com', 1, 1, 1, 1, 1, 1395231133),
(225, 'Cristina', 'criscarvalho255@gmail.com', 1, 1, 1, 1, 1, 1395239381),
(226, 'Sergio', 'sergiosocial@hotmail.com', 1, 1, 1, 1, 1, 1395351469),
(227, 'marcos pereira da silva', 'silva.md1986@bol.com.br', 1, 1, 1, 1, 1, 1396219633),
(228, 'thiago', 'thiago_tbb@hotmail.com', 1, 1, 1, 1, 1, 1396963722),
(229, 'fernando', 'fernando_muniz16@hotmail.com', 1, 1, 1, 1, 1, 1397015906),
(230, 'Taize Maria da Silva', 'taize.ms@hotmail.com', 1, 1, 1, 1, 1, 1398345095),
(231, 'Marcos', 'marvi_2_93@hotmail.com', 1, 1, 1, 1, 1, 1398866625),
(232, 'Gabriela Silva Nunes', 'gabrielasilnunes@yahoo.com.br', 1, 1, 1, 1, 1, 1399051939),
(233, 'Rafael Boni', 'rafael.boni9471@gmail.com', 1, 1, 1, 1, 1, 1399381779),
(234, 'rani', 'ranipassos@hotmail.com', 1, 1, 1, 1, 1, 1399985697),
(235, 'Rafael da Silva', 'rafael.dasilva27@hotmail.com', 1, 1, 1, 1, 1, 1400001278),
(236, 'Daiane Sotero', 'daiane-sotero@hotmail.com', 1, 1, 1, 1, 1, 1400186665),
(237, 'Josefa oliveira', 'julia0510mendes@hotmail.com', 1, 1, 1, 1, 1, 1400556940),
(238, 'DANIELA', 'danivivobela@hotmail.com', 1, 1, 1, 1, 1, 1401372801),
(239, 'Cristalia', 'cris-caua@hotmail.com', 1, 1, 1, 1, 1, 1401396927),
(240, 'Carla Stefani Beirigo', 'carla-stefani@hotmail.com', 1, 1, 1, 1, 1, 1401808042),
(241, 'Tiago Nanci Milani', 'tiagotg.milani@hotmail.com', 1, 1, 1, 1, 1, 1401904636),
(242, 'taiara cifuentes', 'taiaracamc@gmail.com', 1, 1, 1, 1, 1, 1402878748),
(243, 'JULIANE VILELA ', 'juliane.vilela@hotmail.com', 1, 1, 1, 1, 1, 1404070871),
(244, 'KAROLLINY ALVES BEZERRA', 'karollinya@yahoo.com.br', 1, 1, 1, 1, 1, 1404739565),
(245, 'lidia mayara ferreira', 'lidiamayaraferreira@gmail.com', 1, 1, 1, 1, 1, 1405189474),
(246, 'Vanessa', 'vanessabrito1986@hotmail.com', 1, 1, 1, 1, 1, 1405455310),
(247, 'Natalie', 'nataliebartho@hotmail.com', 1, 1, 1, 1, 1, 1405707570),
(248, 'biana kaline', 'biana.kaline@hotmail.com', 1, 1, 1, 1, 1, 1406071436),
(249, 'Gabriele Aparecida Volpi', 'gabrielevolpi@hotmail.com', 1, 1, 1, 1, 1, 1406476105),
(250, 'Gabriela', 'kinder.zinha@hotmail.com', 1, 1, 1, 1, 1, 1406583522),
(251, 'Juliana', 'julliana_plima@hotmail.com', 1, 1, 1, 1, 1, 1406987319),
(252, 'Suzana Oliveira Sampaio', 'suzyline.new@globo.com', 1, 1, 1, 1, 1, 1407262626),
(253, 'Antonio', 'tony_cacao@hotmail.com', 1, 1, 1, 1, 1, 1407353967),
(254, 'AMANDA SALES MAGALHAES', 'amandasalesmagalhaes@gmail.com', 1, 1, 1, 1, 1, 1407364985),
(255, 'Arthur Durigon', 'arthur_henrique_2@msn.com', 1, 1, 1, 1, 1, 1407763375),
(256, 'junior amorim soares', 'amorimjunior18@hotmail.com', 1, 1, 1, 1, 1, 1407788096),
(257, 'Beatriz Mattos', 'bya.96-lima@hotmail.com', 1, 1, 1, 1, 1, 1408222139),
(258, 'caroline', 'caroline_soaresloiola@hotmail.com', 1, 1, 1, 1, 1, 1409312838),
(259, 'Michel ', 'michel.carmo@hotmail.com', 1, 1, 1, 1, 1, 1409602300),
(260, 'leonardo moreto', 'leumoretto@gmail.com', 1, 1, 1, 1, 1, 1409753419),
(261, 'Leandro Lima', 'leandro-llima@live.com', 1, 1, 1, 1, 1, 1409847362),
(262, 'Caroline', 'caroline_barrosx@hotmail.com', 1, 1, 1, 1, 1, 1409854697),
(263, 'Donaldo', 'alcarocha@ig.com.br', 1, 1, 1, 1, 1, 1410957561),
(264, 'Aline', 'aline.monte@live.com', 1, 1, 1, 1, 1, 1411390094),
(265, 'silma', 'silma.assis@outlook.com', 1, 1, 1, 1, 1, 1411745325),
(266, 'Larissa Franco', 'larissaf_santos@outlook.com', 1, 1, 1, 1, 1, 1412374729),
(267, 'Fernanda Sales', 'fea_sales@hotmail.com', 1, 1, 1, 1, 1, 1412868355),
(268, 'ezequiel s. brito', 'ezequielsantos.brito@terra.com.br', 1, 1, 1, 1, 1, 1413208188),
(269, 'alan santos', 'alansaopaulino26@hotmail.com', 1, 1, 1, 1, 1, 1413498023),
(270, 'Debora', 'debora10lopes@hotmail.com', 1, 1, 1, 1, 1, 1413991747),
(271, 'Yuri Larissa Okama', 'yuri@korum.com.br', 1, 1, 1, 1, 1, 1414779224),
(272, 'Edson Jovencio Alves', 'edjovencio1@hotmail.com', 1, 1, 1, 1, 1, 1415035033),
(273, 'Cíntia Rodrigues Gonçalves', 'ci_rgoncalves@hotmail.com', 1, 1, 1, 1, 1, 1415659110),
(274, 'Thiago Deivid', 'thiagodeivid.alves@yahoo.com.br', 1, 1, 1, 1, 1, 1416599858);

-- --------------------------------------------------------

--
-- Estrutura da tabela `noticias`
--

CREATE TABLE `noticias` (
  `id` int(11) NOT NULL,
  `noticia_titulo` varchar(255) NOT NULL,
  `noticia_conteudo` text NOT NULL,
  `noticia_imagem` varchar(255) NOT NULL,
  `noticia_data_publicacao` int(10) NOT NULL,
  `noticia_resumo` varchar(400) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `noticias`
--

INSERT INTO `noticias` (`id`, `noticia_titulo`, `noticia_conteudo`, `noticia_imagem`, `noticia_data_publicacao`, `noticia_resumo`) VALUES
(14, 'Candidatos revelam as melhores e piores perguntas nas entrevistas de emprego', '<p>Uma das etapas mais temidas pelos candidatos &eacute; a entrevista de emprego. &Eacute; nela que os selecionadores v&atilde;o decidir se aquela pessoa tem potencial para ocupar o cargo t&atilde;o almejado por diversos profissionais. No entanto, nesse encontro h&aacute; momentos de desconforto, e a Curriculum, maior site de empregos da Am&eacute;rica Latina, encomendou uma pesquisa &agrave; Gentis Panel a fim de saber a opini&atilde;o de mais de 2.500 pessoas sobre as melhores e as piores perguntas feitas por quem contrata.</p>\n<p><strong>AS MAIS BEM AVALIADAS </strong></p>\n<p>1) Fale-me um pouco mais da sua &uacute;ltima experi&ecirc;ncia<br /> 2) Quais s&atilde;o suas habilidades?<br /> 3) Quais suas expectativas em assumir este novo desafio?<br /> 4) Por que devemos contratar voc&ecirc;?<br /> 5) Como voc&ecirc; se imagina daqui a cinco anos?</p>\n<p><strong>AS PIORES AVALIADAS </strong></p>\n<p>1) Voc&ecirc; corre o risco de chegar atrasado (pelo local onde mora)?<br /> 2) Vai aceitar ganhar menos que no outro emprego?<br /> 3) Fale-me sobre tr&ecirc;s defeitos seus<br /> 4) Fale-me sobre tr&ecirc;s qualidades suas<br /> 5) Por que voc&ecirc; saiu do outro emprego?</p>\n<p>&ldquo;&Eacute; sempre dif&iacute;cil falar sobre si mesmo, ainda mais num momento em que as suas caracter&iacute;sticas e, principalmente, aquilo que voc&ecirc; fala podem ser decisivos para a sua contrata&ccedil;&atilde;o ou n&atilde;o. Por isso, os candidatos tendem a gostar menos desse tipo de pergunta. Questionar sobre as ambi&ccedil;&otilde;es e a vis&atilde;o de futuro sempre garantem mais conforto aos avaliados, como mostra nosso levantamento&rdquo;, analisa Marcelo Abrileri, presidente da Curriculum.</p>\n<p>Por: Vanderlei Abreu</p>\n<p>Mais RH - Blog sobre Gest&atilde;o de Pessoas</p>', '', 1351735200, 'Uma das etapas mais temidas pelos candidatos é a entrevista de emprego. É nela que os selecionadores vão decidir se aquela pessoa tem potencial para ocupar o cargo tão almejado por diversos profissionais. No entanto,'),
(15, 'Caso de amor com o trabalho?', '<p>&ldquo;Trabalhe em algo que voc&ecirc; goste e nunca mais precisar&aacute; trabalhar na vida&rdquo;. Com certeza em algum momento da vida profissional, voc&ecirc; ir&aacute; escutar essa frase. No Brasil cresceu o n&uacute;mero de profissionais que aprenderam a gostar do que fazem. Uma pesquisa recente da Regus encomendada &agrave; MindMetre revelou que os profissionais brasileiros est&atilde;o mais satisfeitos, mesmo trabalhando mais horas durante a semana, pois&nbsp; encontraram um ponto de equil&iacute;brio.</p>\n<p><br /> Com isso, os relacionamentos e a fam&iacute;lia sa&iacute;ram ganhando. Para essa pesquisa foram entrevistados mais de 16 mil profissionais em 90 pa&iacute;ses. Os profissionais brasileiros est&atilde;o satisfeitos com a quantidade de tempo dispon&iacute;vel para ficarem em casa ou para cuidar de assuntos pessoais (51 %) e mais da metade declaram que gostam do seu trabalho (81%).</p>\n<p><br /> As empresas perceberam na pr&aacute;tica que o funcion&aacute;rio feliz &eacute; mais produtivo e o risco de trocar de emprego &eacute; menor. Para 83%&nbsp; dos entrevistados no Brasil, em compara&ccedil;&atilde;o a 2010, o momento atual &eacute; de mais conquistas profissionais.</p>\n<p><br /> A rela&ccedil;&atilde;o profissionais e empresas avan&ccedil;ou nos &uacute;ltimos anos, uma exig&ecirc;ncia do pr&oacute;prio mercado que busca reter seus talentos e procura driblar as dificuldades do tr&acirc;nsito e das condi&ccedil;&otilde;es do transporte p&uacute;blico nas grandes cidades brasileiras, oferecendo condi&ccedil;&otilde;es flex&iacute;veis de trabalho. De acordo com a pesquisa da Regus, 59 % dos entrevistados no Pa&iacute;s, disseram que as companhias em que trabalham est&atilde;o se esfor&ccedil;ando para diminuir o tempo gasto&nbsp; o percurso de casa ao trabalho.<br /> Para o diretor geral da Regus no Brasil, Guilherme Ribeiro, &ldquo;as pr&aacute;ticas de trabalho v&ecirc;m mudando no pa&iacute;s. Medidas simples, como permitir que o funcion&aacute;rio gerencie seu pr&oacute;prio hor&aacute;rio, reduza o tempo de deslocamento ou possibilite ao empregado que trabalhe de outros locais, ajudam no desempenho dos profissionais, reduzindo o n&iacute;vel de estresse&nbsp; e aumentando a produtividade&rdquo;, destaca.</p>\n<p><br /> Com menos estresse di&aacute;rio, esse clima de paz tem tudo para se expandir. De acordo com a pesquisa, o Brasil &eacute; o segundo pa&iacute;s, com o melhor &Iacute;ndice de Equil&iacute;brio entre Vida Profissional e Pessoal (151 pontos), atr&aacute;s apenas do M&eacute;xico (153). E a m&eacute;dia global entre os pa&iacute;ses que participaram da amostragem &eacute; de 124 pontos.</p>\n<p>Por: Vanderlei Abreu</p>\n<p>Mais RH - Blog sobre Gest&atilde;o de Pessoas</p>', '', 1351735200, '“Trabalhe em algo que você goste e nunca mais precisará trabalhar na vida”. Com certeza em algum momento da vida profissional, você irá escutar essa frase. No Brasil cresceu o número de profissionais '),
(16, 'Grupo Meta RH recebe cinco distinções no Prêmio Melhores Fornecedores para RH', '<p>Al&eacute;m de figurar entre os 10, 100 e 300 Melhores Fornecedores para RH, consultoria tamb&eacute;m recebeu dois pr&ecirc;mios na categoria Talentos</p>\n<p>O Grupo Meta RH recebeu no dia 5 de fevereiro cinco distin&ccedil;&otilde;es no Pr&ecirc;mio 100 Melhores Fornecedores para RH, organizado pela Gest&atilde;o &amp; RH Editora. Al&eacute;m de figurar entre os 10, 100 e 300 Melhores Fornecedores para RH, a consultoria tamb&eacute;m recebeu os pr&ecirc;mios de Melhor Avaliada em Recrutamento e Sele&ccedil;&atilde;o e Trabalho Efetivo e Tempor&aacute;rio na categoria Talentos. Abigair Ribeiro Costa Leite, diretora administrativa e financeira, representou o Grupo Meta RH na cerim&ocirc;nia de premia&ccedil;&atilde;o. Organizado pela revista Gest&atilde;o &amp; RH, o evento tem por objetivo reconhecer o trabalho, atendimento e qualidade das empresas que atendem ao setor de Recursos Humanos.</p>\n<p>A premia&ccedil;&atilde;o &eacute; realizada em duas fases. Na primeira a vota&ccedil;&atilde;o envolve os profissionais que atuam na &aacute;rea de Gest&atilde;o de Pessoas, que escolhem at&eacute; dez empresas dos 40 segmentos selecionados no estudo &ldquo;300 Melhores Fornecedores para RH&rdquo;. As empresas mais votadas s&atilde;o classificadas para a segunda fase, na qual s&atilde;o votadas pelos seus pr&oacute;prios clientes. A cerim&ocirc;nia de premia&ccedil;&atilde;o tamb&eacute;m contou com a palestra da escritora Leila Navarro.</p>', '', 1360893600, 'Além de figurar entre os 10, 100 e 300 Melhores Fornecedores para RH, consultoria também recebeu dois prêmios na categoria Talentos'),
(17, 'Mitos e verdades sobre estudar inglês pela internet', '<p>Segundo a Associa&ccedil;&atilde;o Brasileira de Ensino a Dist&acirc;ncia (ABED), nos &uacute;ltimos quatro anos o Brasil teve um aumento de 30% na procura por cursos de idiomas a dist&acirc;ncia. Nesse per&iacute;odo foi percept&iacute;vel uma evolu&ccedil;&atilde;o gradual na vis&atilde;o da sociedade sobre essa metodologia de ensino, por&eacute;m ela ainda gera alguns questionamentos.</p>\n<p class="Normal2">Para desvendar os mitos que rondam o tema, Andr&eacute; Marques, diretor geral da <strong>EF Englishtown (</strong><a href="http://www.englishtown.com.br"><strong>www.englishtown.com.br</strong></a><strong>)</strong>, maior escola de ingl&ecirc;s online do mundo, reponde as principais d&uacute;vidas sobre estudar ingl&ecirc;s pela internet:</p>\n<p class="Normal2">&nbsp;</p>\n<p class="Normal2"><strong>- As empresas preferem candidatos com cursos presenciais.</strong></p>\n<p class="Normal2"><strong>Mito. </strong>No mercado de trabalho j&aacute; foi identificado que quem cursa aulas a dist&acirc;ncia desenvolve habilidades valiosas para o mundo dos neg&oacute;cios, como responsabilidade, organiza&ccedil;&atilde;o, disciplina, autonomia na resolu&ccedil;&atilde;o de problemas e, principalmente, percep&ccedil;&atilde;o para analisar e filtrar informa&ccedil;&otilde;es.</p>\n<p class="Normal2"><strong>&nbsp;</strong></p>\n<p class="Normal2"><strong>- &Eacute; preciso ter acesso &agrave; tecnologia.</strong></p>\n<p class="Normal2"><strong>Verdade.</strong> Grande parte dos cursos de ensino a dist&acirc;ncia utiliza a internet como plataforma. A EF Englistown oferece v&iacute;deos e transmiss&otilde;es em tempo real aos seus alunos, por meio do acesso on-line desenvolvido exclusivamente para os estudantes.</p>\n<p class="Normal2">&nbsp;</p>\n<p class="Normal2"><strong>- Para fazer ingl&ecirc;s online &eacute; preciso ser disciplinado.</strong></p>\n<p class="Normal2"><strong>Verdade.</strong> Estudar online exige disciplina. O aluno precisa acessar todas as aulas frequentemente e fazer as atividades propostas.</p>\n<p class="Normal2"><strong>&nbsp;</strong></p>\n<p class="Normal2"><strong>- O aluno n&atilde;o interage com outras pessoas. </strong></p>\n<p class="Normal2"><strong>Mito.</strong> N&atilde;o ter momentos de conviv&ecirc;ncia e intera&ccedil;&atilde;o entre alunos &eacute; uma op&ccedil;&atilde;o do estudante. No momento de escolher o curso, a pessoa deve pesquisar detalhes sobre a metodologia da escola. A EF Englishtown, por exemplo, oferece aulas particulares com professores nativos e aulas mensais de conversa&ccedil;&atilde;o em grupo com pessoas do mundo inteiro, tudo isso pela internet.</p>\n<p class="Normal2">&nbsp;</p>\n<p class="Normal2"><strong>- N&atilde;o &eacute; poss&iacute;vel estudar somente quando quiser.</strong></p>\n<p class="Normal2"><strong>Verdade.</strong> O aluno pode programar os hor&aacute;rios das aulas e atividades complementares de acordo com sua disponibilidade. Por&eacute;m, se n&atilde;o houver dedica&ccedil;&atilde;o, ele poder&aacute; encontrar problemas para avan&ccedil;ar no curso.</p>\n<p class="Normal2"><strong>&nbsp;</strong></p>\n<p class="Normal2"><strong>- Conseguir o diploma &eacute; f&aacute;cil.</strong></p>\n<p class="Normal2"><strong>Mito.</strong> A maioria dos cursos de ingl&ecirc;s online &eacute; feita no mesmo tempo que numa escola presencial, de acordo com o tempo dispon&iacute;vel do aluno. Algumas pessoas optam pelo ensino a dist&acirc;ncia por acharem que &eacute; mais simples, mas ao iniciar as aulas percebem que precisam se dedicar para aprender o idioma.</p>\n<p class="Normal2"><strong>&nbsp;</strong></p>\n<p class="Normal2"><strong>&nbsp;</strong></p>\n<p class="Normal2"><strong>&nbsp;</strong></p>\n<p class="Normal2"><strong>- Os alunos aprendem o idioma como na aula presencial.</strong></p>\n<p><strong>Verdade. </strong>O ensino on-line oferece conte&uacute;dos de qualidade, assim como nas aulas presenciais.Segundo Fredric Litto, presidente da ABED, cerca de 3,5 milh&otilde;es de pessoas estudam a dist&acirc;ncia no Brasil. As inova&ccedil;&otilde;es tecnol&oacute;gicas e, principalmente, o aumento da qualidade dessa modalidade, tem levado muitas pessoas a escolherem esse m&eacute;todo de ensino.</p>', '', 1363748400, 'Estudar pela internet tem se tornado muito útil nos dias atuais. Veja quais são os prós e contras de do ensino a distância.'),
(18, 'Expo CIEE: 25 mil estudantes inscritos em duas semanas', '<p>Apenas duas semanas ap&oacute;s abrir as inscri&ccedil;&otilde;es gratuitas pelo <a href="http://www.ciee.org.br">www.ciee.org.br</a>, a <strong>16&ordf;</strong> <strong>Feira do Estudante - Expo CIEE 2013 </strong>j&aacute; conta com 25 mil estudantes cadastrados para participar do evento que ocorre de 17 a 19 de maio, a partir das 10 horas, no Pavilh&atilde;o da Bienal, no Parque do Ibirapuera, em S&atilde;o Paulo/SP. Inscrevendo-se pela internet, o estudante agiliza sua entrada e evita filas. Ap&oacute;s a inscri&ccedil;&atilde;o pela web, &eacute; gerada uma credencial que deve ser impressa e apresentada na entrada da Feira.</p>\n<p class="Normal2">A programa&ccedil;&atilde;o inclui a apresenta&ccedil;&atilde;o de cerca de 80 palestras sobre carreira, mercado de trabalho, profiss&otilde;es promissoras e orienta&ccedil;&atilde;o para participa&ccedil;&atilde;o em processos seletivos, a cargo de especialistas. Al&eacute;m disso, os organizadores oferecer&atilde;o eventos culturais, concursos e outras atra&ccedil;&otilde;es.</p>\n<p class="Normal2">Outras vantagens para os visitantes da feira s&atilde;o a oferta de 5 mil vagas de est&aacute;gio e 2 mil para aprendizagem, al&eacute;m do encaminhamento preferencial para vagas de est&aacute;gio nos seis meses seguintes ao evento.</p>\n<p class="Normal2">Os visitantes tamb&eacute;m ter&atilde;o oportunidade de entrar em contato com expositores, divididos em institui&ccedil;&otilde;es de ensino, empresas e &oacute;rg&atilde;os p&uacute;blicos. A mostra conta com patroc&iacute;nio do Grupo Est&aacute;cio.</p>\n<p class="Normal2">O Grupo Meta RH ir&aacute; participar da 16&ordf; Feira do Estudante com um estande na Expo CIEE 2013.</p>\n<p class="Normal2"><strong><span style="text-decoration: underline;">&nbsp;</span></strong></p>\n<p class="Normal2"><strong><span style="text-decoration: underline;">Servi&ccedil;o</span></strong></p>\n<p class="Normal2"><strong>&nbsp;</strong></p>\n<table border="0" cellspacing="0" cellpadding="0">\n<tbody>\n<tr>\n<td valign="top" width="599">\n<p class="Normal2"><strong><em>16&ordf; Feira do Estudante &ndash; EXPO CIEE 2013</em></strong></p>\n<p class="Normal2"><strong>Data:</strong> 17, 18 e 19 de maio de 2013</p>\n<p class="Normal2"><strong>Local:</strong> Pavilh&atilde;o da Bienal, no Parque do Ibirapuera &ndash; &nbsp; S&atilde;o Paulo/SP</p>\n<p class="Normal2"><strong>Hor&aacute;rios:</strong> sexta-feira, s&aacute;bado e domingo, a partir das &nbsp; 10h</p>\n<p class="Normal2"><strong>Entrada:</strong> gratuita</p>\n<p class="Normal2"><strong>Informa&ccedil;&otilde;es:</strong> <a href="http://www.ciee.org.br">www.ciee.org.br</a></p>\n</td>\n</tr>\n</tbody>\n</table>', '', 1363748400, 'Maior feira de estágios, carreira e informação profissional espera 60 mil visitantes'),
(19, 'Feedback: formas e formas de dá-lo no processo seletivo', '<p class="Normal2">N&atilde;o costumo utilizar o espa&ccedil;o de not&iacute;cias para expor minhas ideias e experi&ecirc;ncias na &aacute;rea de Gest&atilde;o de Pessoas, mas acredito que este texto pode convidar os profissionais da &aacute;rea a refletirem um pouco sobre como t&ecirc;m atuado.</p>\n<p class="Normal2">&nbsp;</p>\n<p class="Normal2">Pesquisa divulgada em abril deste ano pelo site de empregos Curriculum, apontou que 83% dos profissionais se queixam de n&atilde;o receber nenhum retorno do processo seletivo.</p>\n<p class="Normal2">&nbsp;</p>\n<p class="Normal2">Muito pior que n&atilde;o dar nenhum feedback &eacute; faz&ecirc;-lo de forma absolutamente equivocada.</p>\n<p class="Normal2">&nbsp;</p>\n<p class="Normal2">Vou relatar um caso ocorrido com uma conhecida que tem defici&ecirc;ncia f&iacute;sica. Vou cit&aacute;-la com o nome Estela para preservar sua identidade.</p>\n<p class="Normal2">&nbsp;</p>\n<p class="Normal2">Ela recebeu aviso de um site de empregos sobre uma vaga para pessoa com defici&ecirc;ncia em uma institui&ccedil;&atilde;o privada sem fins lucrativos, integrante do chamado Sistema S. Apesar de n&atilde;o ser obrigada a organizar concursos para contrata&ccedil;&atilde;o de pessoas, por n&atilde;o se tratar de &oacute;rg&atilde;o da administra&ccedil;&atilde;o direta, a divulga&ccedil;&atilde;o de vagas &eacute; feita por meio de edital, nos mesmos moldes dos certames p&uacute;blicos, inclusive com a divulga&ccedil;&atilde;o de sal&aacute;rio.</p>\n<p class="Normal2">&nbsp;</p>\n<p class="Normal2">Estela relatou que o sal&aacute;rio era bastante convidativo e se inscreveu para a vaga, primeiramente enviando curr&iacute;culo via e-mail.</p>\n<p class="Normal2">&nbsp;</p>\n<p class="Normal2">Uma consultoria de Recursos Humanos contratada por este &oacute;rg&atilde;o para conduzir o processo seletivo entrou em contato para agendar uma entrevista, mas Estela n&atilde;o p&ocirc;de comparecer.</p>\n<p class="Normal2">&nbsp;</p>\n<p class="Normal2">Estela contou que, estranhamente, uma semana depois do contato da consultoria, a vaga foi retirada do site da institui&ccedil;&atilde;o. E duas semanas depois, a mesma vaga foi publicada novamente. E Estela foi novamente chamada para uma entrevista, desta vez, atendida pela candidata.</p>\n<p class="Normal2">&nbsp;</p>\n<p class="Normal2">Em um encontro que tive com Estela hoje, ela me relatou que, quase 15 dias depois da entrevista, a consultora entrou em contato para informar que a vaga havia passado por uma reformula&ccedil;&atilde;o e que agora a institui&ccedil;&atilde;o est&aacute; buscando um profissional com os mesmos requisitos, mas com sal&aacute;rio 60% menor que o oferecido no edital (!).</p>\n<p class="Normal2">&nbsp;</p>\n<p class="Normal2">Eu, como jornalista e profissional de Recrutamento e Sele&ccedil;&atilde;o por quase 10 anos, confesso que fiquei abismado com a falta de compromisso, tanto da consultoria quanto da empresa contratante no feedback dado aos candidatos.</p>\n<p class="Normal2">&nbsp;</p>\n<p class="Normal2">Estranhamente, quando o sal&aacute;rio era relativamente alto, a empresa alegou n&atilde;o ter encontrado nenhum candidato qualificado. Agora, pede para a consultoria retomar o contato com os participantes do processo anterior para oferecer uma vaga com sal&aacute;rio 60% menor?</p>\n<p class="Normal2">&nbsp;</p>\n<p class="Normal2">Tamb&eacute;m refleti sobre a falta total de compromisso dos profissionais de Recrutamento e Sele&ccedil;&atilde;o em oferecer feedback aos candidatos de um processo seletivo, mesmo que seja com um e-mail &ldquo;gelado&rdquo; agradecendo a participa&ccedil;&atilde;o.</p>\n<p class="Normal2">&nbsp;</p>\n<p class="Normal2">Na &eacute;poca em que trabalhei na &aacute;rea, realmente era muito complicado dar feedback a todos os participantes. Mas hoje, com toda a tecnologia dispon&iacute;vel, alegar falta de tempo &eacute; uma desculpa para l&aacute; de esfarrapada.</p>\n<p class="Normal2">&nbsp;</p>\n<p class="Normal2">Os sistemas de recrutamento e sele&ccedil;&atilde;o dispon&iacute;veis no mercado permitem em poucos minutos montar uma carta de agradecimento e com poucos cliques dispar&aacute;-la a TODOS os participantes do processo.</p>\n<p class="Normal2">&nbsp;</p>\n<p class="Normal2">Outra dificuldade que o profissional de Recrutamento e Sele&ccedil;&atilde;o precisa superar &mdash; e urgente &mdash; &eacute; a condu&ccedil;&atilde;o de processos seletivos de pessoas com defici&ecirc;ncia.</p>\n<p class="Normal2">&nbsp;</p>\n<p class="Normal2">A Lei de Cotas existe e deve ser cumprida. N&atilde;o adianta tapar o sol com a peneira. Dizer que as pessoas com defici&ecirc;ncia n&atilde;o s&atilde;o qualificadas &eacute; outra panaceia. A grande maioria dos trabalhadores com defici&ecirc;ncia &eacute; t&atilde;o boa ou at&eacute; melhor que os ditos normais.</p>\n<p class="Normal2">&nbsp;</p>\n<p class="Normal2">O que n&atilde;o d&aacute; &eacute; os profissionais da &aacute;rea tratarem essas pessoas como d&eacute;beis mentais &mdash; mesmo que sejam pessoas com defici&ecirc;ncia intelectual &mdash; e as empresas oferecerem vagas com sal&aacute;rios baix&iacute;ssimos apenas para cumprir a legisla&ccedil;&atilde;o.</p>\n<p class="Normal2">&nbsp;</p>\n<p class="Normal2">Se a &aacute;rea de Recrutamento e Sele&ccedil;&atilde;o deseja ser vista como estrat&eacute;gica dentro da organiza&ccedil;&atilde;o, n&atilde;o pode continuar com uma postura amadora como essa.</p>\n<p class="Normal2">&nbsp;</p>\n<p class="Normal2"><em><strong>Vanderlei Abreu</strong> &eacute; jornalista, graduado pela Universidade S&atilde;o Judas Tadeu (SP). Atualmente &eacute; editor da Revista T&amp;D Intelig&ecirc;ncia Corporativa, do Blog Mais RH, rep&oacute;rter colaborador da Revista Melhor Gest&atilde;o de Pessoas (<a href="http://www.revistamelhor.com.br/" target="_blank">www.revistamelhor.com.br)</a>, assessor de Comunica&ccedil;&atilde;o do Grupo Meta RH (<a href="http://www.grupometarh.com.br/">www.grupometarh.com.br</a>) e presta servi&ccedil;os na &aacute;rea de Comunica&ccedil;&atilde;o Empresarial desenvolvendo ferramentas de Comunica&ccedil;&atilde;o Interna. Foi editor de Conte&uacute;do do portal de tecnologia A&Ccedil;&Atilde;O Plugin, editor-chefe do jornal e programa de TV Profissional &amp; Neg&oacute;cios e editor-chefe das revistas setoriais da OESP M&iacute;dia (Grupo Estado). Foi assessor&nbsp;de comunica&ccedil;&atilde;o da AAPSA &ndash; Associa&ccedil;&atilde;o Paulista de Gestores de Pessoas. Atuou na &aacute;rea de Esportes com o jornalista Milton Neves nos jornais Di&aacute;rio Popular e Agora S&atilde;o Paulo.&nbsp;</em></p>', '', 1363748400, 'Feedback: formas e formas de dá-lo no processo seletivo'),
(20, 'Oi lança novo site para divulgar vagas de emprego', '<p class="Normal2">O site sobre a &aacute;rea de Gente no portal da Oi est&aacute; com cara nova. A p&aacute;gina foi totalmente reformulada para apresentar ao p&uacute;blico, de forma mais completa, como &eacute; trabalhar na Oi, tornando a companhia mais atrativa para os talentos do mercado. Al&eacute;m do link para se candidatar &agrave;s vagas de emprego, est&aacute;gio e trainee, o portal agora re&uacute;ne mais informa&ccedil;&otilde;es sobre a atua&ccedil;&atilde;o das diversas &aacute;reas da Oi, as carreiras que podem ser trilhadas dentro da companhia, as etapas dos processos seletivos, os diferentes programas de desenvolvimento oferecidos, al&eacute;m de dados quantitativos que expressam bem como a empresa investe no desenvolvimento profissional dos seus estagi&aacute;rios e colaboradores.</p>\n<p class="Normal2">&nbsp;</p>\n<p class="Normal2">Com estrutura din&acirc;mica e navega&ccedil;&atilde;o simples, o novo site tamb&eacute;m conta com depoimentos em v&iacute;deo de estagi&aacute;rios e colaboradores que comentam, na se&ccedil;&atilde;o &ldquo;Nossa Gente&rdquo;, suas trajet&oacute;rias na Oi. Na mesma &aacute;rea, &eacute; poss&iacute;vel saber mais sobre as pr&aacute;ticas e as op&ccedil;&otilde;es de atividades relacionadas &agrave; qualidade de vida disponibilizadas pela empresa, al&eacute;m das vagas para pessoas com defici&ecirc;ncia e as a&ccedil;&otilde;es de voluntariado.</p>\n<p class="Normal2">&nbsp;</p>\n<p class="Normal2">O projeto mobilizou profissionais de todas as diretorias da Oi, que contribu&iacute;ram com informa&ccedil;&otilde;es sobre as principais responsabilidades de suas &aacute;reas de atua&ccedil;&atilde;o. Esse conte&uacute;do est&aacute; dispon&iacute;vel para quem acessar o site, na se&ccedil;&atilde;o &ldquo;Quero trabalhar na Oi&rdquo;.&nbsp;&ldquo;Com o novo portal de Gente, al&eacute;m de consolidar sua marca como grande empregadora, a companhia oferece mais informa&ccedil;&otilde;es para os interessados em trabalhar na empresa, facilitando a identifica&ccedil;&atilde;o de oportunidades para estudantes e profissionais que t&ecirc;m a cara da Oi&rdquo;, comenta Julio Fonseca, diretor de Gente e Gest&atilde;o da Oi.</p>\n<p class="Normal2">&nbsp;</p>\n<p class="Normal2">Uma &aacute;rea exclusiva do site foi reservada para o Gera&ccedil;&atilde;o Oi, programa que busca suprir a necessidade de pessoas em posi&ccedil;&otilde;es-chave na organiza&ccedil;&atilde;o e re&uacute;ne o Est&aacute;gio Superior, o Est&aacute;gio T&eacute;cnico, o Jovem Vendedor, o Trainee Expert e o Trainee Executivo. Nesta mesma p&aacute;gina, o visitante do site poder&aacute; conferir como &eacute; cada um desses programas, os benef&iacute;cios oferecidos, os requisitos para se candidatar, as etapas de sele&ccedil;&atilde;o e se as inscri&ccedil;&otilde;es est&atilde;o abertas.</p>\n<p class="Normal2">&nbsp;</p>\n<p class="Normal2">O projeto partiu de um processo de benchmarking com 40 empresas do Brasil e do exterior. Nesse primeiro momento, o objetivo foi avaliar sites semelhantes de organiza&ccedil;&otilde;es com atua&ccedil;&atilde;o em diferentes setores. Em uma segunda etapa, outra rodada de benchmarking foi realizada, para analisar qual o layout ideal. O resultado pode ser conferido no endere&ccedil;o&nbsp;<a href="http://www.oi.com.br/euquerotrabalharnaoi" target="_blank">www.oi.com.br/euquerotrabalharnaoi</a>.</p>', '', 1363748400, 'Novo site ajuda a divulgar vagas de emprego entre o público da operadora'),
(21, 'RDS Multimídia implanta bônus por indicação', '<p class="Normal2">O &ldquo;Pr&ecirc;mio Ca&ccedil;a Talentos&rdquo; que a RDS Multim&iacute;dia, empresa de solu&ccedil;&otilde;es em comunica&ccedil;&atilde;o indoor para o varejo, ofereceu para funcion&aacute;rios que indicassem outros representantes para fazer parte da equipe, j&aacute; deu resultado. Colaboradores fizeram suas indica&ccedil;&otilde;es e o novo contratado gerou bonifica&ccedil;&atilde;o em dinheiro para quem o trouxe.</p>\n<p class="Normal2">&nbsp;</p>\n<p class="Normal2">A iniciativa de premiar funcion&aacute;rios que ajudem nas novas contrata&ccedil;&otilde;es veio da defici&ecirc;ncia que a RDS estava tendo para recrutar profissionais qualificados. A t&eacute;cnica &eacute; cada vez mais frequente: premiar funcion&aacute;rios que indicarem novos contratados. A bonifica&ccedil;&atilde;o da RDS foi em dinheiro. &ldquo;A sugest&atilde;o quando vem de nossos colaboradores pode resultar em uma contrata&ccedil;&atilde;o de maior qualidade, al&eacute;m de ser um incentivo para quem far&aacute; a indica&ccedil;&atilde;o&rdquo;, afirma Marcel Soares, gerente de Marketing e Comercial da RDS.</p>\n<p class="Normal2">&nbsp;</p>\n<p class="Normal2">O representante comercial M&aacute;rio Verlangieri, indicou o amigo Alvelei Vieira, que passou na sele&ccedil;&atilde;o para outra vaga tamb&eacute;m de representante comercial. Com isso, M&aacute;rio ganhou o B&ocirc;nus do programa &ldquo;Ca&ccedil;a Talentos&rdquo;.</p>\n<p class="Normal2">&nbsp;</p>\n<p class="Normal2">A procura por candidatos em consultorias n&atilde;o supriu as necessidades da RDS, por isso o programa foi uma solu&ccedil;&atilde;o para a escassez de profissionais dos setores administrativos, TI e comercial na empresa. &ldquo;Com o resultado positivo que tivemos nesta primeira campanha acredito que muitos outros colaboradores ir&atilde;o se incentivar para fazer novas indica&ccedil;&otilde;es&rdquo;, afirma Soares.</p>\n<p class="Normal2">&nbsp;</p>\n<p class="Normal2">Este tipo de contrata&ccedil;&atilde;o &eacute; eficaz tamb&eacute;m no sentido de preparo e ambienta&ccedil;&atilde;o do novo funcion&aacute;rio, j&aacute; que quem o indicou j&aacute; est&aacute; completamente inteirado do setor e sabe quais as dificuldades di&aacute;rias a serem superadas. &ldquo;Acreditamos que com isto conseguiremos aumentar nosso quadro de colaboradores e ainda incentivar os que j&aacute; est&atilde;o na empresa com a bonifica&ccedil;&atilde;o oferecida&rdquo;, completa Verlangieri.</p>', '', 1363748400, 'Saiba mais sobre o prêmio "Caça Talentos" que a RDS Multimídia ofereceu para seus funcionários'),
(22, 'Grupo Meta RH conquista Prêmio Fornecedores de Confiança 2013', '<p>&nbsp;</p>\n<p class="Normal2">O Grupo Meta RH, consultoria de solu&ccedil;&otilde;es em Recrutamento, Sele&ccedil;&atilde;o, Administra&ccedil;&atilde;o de Pessoal Terceirizado e Desenvolvimento Organizacional, foi premiado como um dos destaques do Pr&ecirc;mio Fornecedores de Confian&ccedil;a 2013, realizado pela Editora Segmento. A cerim&ocirc;nia de premia&ccedil;&atilde;o foi realizada dia 26 de mar&ccedil;o, no La Luna Club, em S&atilde;o Paulo, SP.</p>\n<p class="Normal2">O pr&ecirc;mio, direcionado a empresas que fornecem solu&ccedil;&otilde;es e prestam servi&ccedil;os para a &aacute;rea de Recursos Humanos, tem como objetivo reconhecer e destacar as institui&ccedil;&otilde;es que possuem o mais alto grau de confiabilidade. &ldquo;Estamos muito contentes com esse reconhecimento. Ser escolhida pela primeira vez como uma empresa destaque indica que as solu&ccedil;&otilde;es do Grupo Meta RH para a &aacute;rea de Recursos Humanos v&atilde;o ao encontro das necessidades de nossos clientes&rdquo;, afirma Gutemberg Leite, diretor comercial.</p>\n<p class="Normal2">O estudo para o Pr&ecirc;mio Fornecedores de Confian&ccedil;a 2013 foi realizado entre novembro de 2012 e fevereiro de 2013. Para participar, cada companhia inscrita indicou 40 clientes que responderam a uma pesquisa de satisfa&ccedil;&atilde;o. Foram considerados clientes v&aacute;lidos aqueles atendidos por um per&iacute;odo superior a seis meses.</p>\n<p class="Normal2">Em sua sexta edi&ccedil;&atilde;o, o evento premiou 50 empresas. Para chegar ao resultado, foram consultadas cerca de 4.800 empresas clientes de fornecedores de produtos e servi&ccedil;os para a &aacute;rea de RH.</p>', '', 1364439600, 'Estudo reconhece e destaca instituições com o mais alto grau de confiabilidade no segmento de Recursos Humanos');

-- --------------------------------------------------------

--
-- Estrutura da tabela `politica_privacidade`
--

CREATE TABLE `politica_privacidade` (
  `id` int(11) NOT NULL,
  `texto` text
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `politica_privacidade`
--

INSERT INTO `politica_privacidade` (`id`, `texto`) VALUES
(1, '<p>N&oacute;s, da empresa <strong>META RECRUTAMENTO E SELE&Ccedil;AO LTDA.</strong>, doravante denominada simplesmente <strong>GRUPO META RH</strong>, estamos comprometidos em resguardar sua privacidade. O intuito deste documento &eacute; esclarecer quais informa&ccedil;&otilde;es s&atilde;o coletadas dos usu&aacute;rios de nosso site <em>http://www.grupometarh.com.br/</em> &ndash; e respectivos servi&ccedil;os &ndash; e de que forma esses dados s&atilde;o utilizados.</p>\n<p>Nossa pol&iacute;tica de privacidade norteia-se pelo compromisso com seus usu&aacute;rios no que diz respeito &agrave; transpar&ecirc;ncia e a garantia de prote&ccedil;&atilde;o da privacidade de suas informa&ccedil;&otilde;es pessoais. Dessa forma, contribuem para que os usu&aacute;rios usufruam de um ambiente seguro e protegido.</p>\n<p>O site tem adotado os n&iacute;veis legalmente requeridos quanto &agrave; seguran&ccedil;a na prote&ccedil;&atilde;o de dados e procura instalar todos os meios e medidas adicionais para evitar a perda, o mau uso, a altera&ccedil;&atilde;o, o acesso n&atilde;o autorizado ou a subtra&ccedil;&atilde;o indevida dos Dados recolhidos.</p>\n<p><strong>Sobre a coleta de dados</strong></p>\n<p>Em nossos sites, as informa&ccedil;&otilde;es s&atilde;o coletadas das seguintes formas:</p>\n<ul>\n<li>Informa&ccedil;&otilde;es fornecidas por voc&ecirc; &ndash; Coletamos informa&ccedil;&otilde;es de identifica&ccedil;&atilde;o pessoal &ndash; como nome, telefone, e-mail, atrav&eacute;s de preenchimento de formul&aacute;rio para contato;</li>\n<li>Hist&oacute;rico de contato &ndash; o GRUPO META RH armazena informa&ccedil;&otilde;es a respeito de todos os contatos j&aacute; realizados a partir de nossas p&aacute;ginas e intera&ccedil;&otilde;es via e-mail.</li>\n</ul>\n<p><strong>Altera&ccedil;&otilde;es na Pol&iacute;tica de Privacidade</strong></p>\n<p>O GRUPO META RH reserva-se o direito de alterar as pol&iacute;ticas de privacidade ora estabelecidas, para adapt&aacute;-las a altera&ccedil;&otilde;es legislativas, normas relativas &agrave; utiliza&ccedil;&atilde;o de novas tecnologias, ou ainda, sempre que julgar necess&aacute;rio.</p>\n<p><strong>Pol&iacute;tica de Preserva&ccedil;&atilde;o Legal</strong></p>\n<p>O conte&uacute;do do site &eacute; protegido pelas leis e normas que regulamentam os direitos autorais, marcas registradas e patentes.</p>\n<p>O usu&aacute;rio &eacute; meramente autorizado a fazer uso dos conte&uacute;dos do site para fins estritamente pessoais, sendo-lhe expressamente proibido publicar, reproduzir, difundir, distribuir ou, por qualquer forma, tornar os conte&uacute;dos acess&iacute;veis a terceiros, para quaisquer fins, designadamente de comunica&ccedil;&atilde;o p&uacute;blica ou de comercializa&ccedil;&atilde;o, nomeadamente de disponibiliza&ccedil;&atilde;o em outro Website, servi&ccedil;o on-line, ou c&oacute;pias em papel.</p>\n<p>Ao usu&aacute;rio &eacute; vedado alterar, tentar alterar, por qualquer meio o conte&uacute;do do site, ou introduzir no mesmo quaisquer tipos de v&iacute;rus ou programas que o danifiquem ou o contaminem.</p>\n<p>Entendemos, ainda, como viola&ccedil;&atilde;o &agrave; nossa pol&iacute;tica de privacidade:</p>\n<ul>\n<li>usar o sistema com prop&oacute;sitos ilegais;</li>\n<li>forjar endere&ccedil;os de m&aacute;quinas, de rede ou de correio eletr&ocirc;nico, na tentativa de responsabilizar terceiros ou ocultar a identidade ou autoria;</li>\n<li>violar copyright ou direito autoral alheio reproduzindo material sem pr&eacute;via autoriza&ccedil;&atilde;o.</li>\n</ul>\n<p>As tentativas de invas&atilde;o ao site ser&atilde;o tratadas conforme prescri&ccedil;&atilde;o legal e ter&atilde;o a tipifica&ccedil;&atilde;o penal correspondente &agrave;s consequ&ecirc;ncias legalmente tipificadas.</p>\n<p>O conte&uacute;do deste site, incluindo designadamente os textos, imagens e sons, &eacute; de propriedade do GRUPO META RH, com exce&ccedil;&atilde;o dos conte&uacute;dos fornecidos por clientes ou parceiros comerciais que se encontrem identificados como tal, tendo apenas fins informativos.</p>\n<p>O GRUPO META RH n&atilde;o se responsabiliza pelos dados pessoais inseridos em sites parceiros do Grupo, redirecionados atrav&eacute;s do site.</p>\n<p>O usu&aacute;rio, ao continuar a utilizar este site, aceita expressamente os termos e condi&ccedil;&otilde;es acima descritos.</p>\n<p>Caso haja alguma d&uacute;vida sobre a pol&iacute;tica de privacidade, favor contatar-nos pelo e-mail <a href="\\&quot; data-mce-href=">contato@grupometarh.com.br</a>.</p>');

-- --------------------------------------------------------

--
-- Estrutura da tabela `programas`
--

CREATE TABLE `programas` (
  `id` int(11) NOT NULL,
  `programa_categoria` varchar(255) NOT NULL,
  `programa_imagem` varchar(255) NOT NULL,
  `programa_empresa` varchar(255) NOT NULL,
  `programa_link` varchar(255) NOT NULL,
  `programa_data_cadastro` int(10) NOT NULL,
  `programa_data_expiracao` int(255) NOT NULL,
  `programa_destaque` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `programas`
--

INSERT INTO `programas` (`id`, `programa_categoria`, `programa_imagem`, `programa_empresa`, `programa_link`, `programa_data_cadastro`, `programa_data_expiracao`, `programa_destaque`) VALUES
(25, 'estagiarios', 'vaga-marketing.jpg', '-', 'http://site.vagas.com.br/PagVagaDirSS.asp?v=939606&pp=https%3A//site.vagas.com.br/GoHome.asp%3Fv%3D939606%26j%3Dt%26sslon%3D1&wiw=1280', 1385727173, 1420077600, 1),
(26, 'estagiarios', 'beneficios-pequeno.png', '-', 'http://site.vagas.com.br/PagVagaDirSS.asp?v=923802&pp=https%3A//site.vagas.com.br/GoHome.asp%3Fv%3D923802%26j%3Dt%26sslon%3D1&wiw=1157', 1385727199, 1420077600, 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `programa_metodista`
--

CREATE TABLE `programa_metodista` (
  `id` int(11) NOT NULL,
  `etapa_ativa` int(10) UNSIGNED NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `programa_metodista`
--

INSERT INTO `programa_metodista` (`id`, `etapa_ativa`) VALUES
(1, 4);

-- --------------------------------------------------------

--
-- Estrutura da tabela `roles`
--

CREATE TABLE `roles` (
  `id` int(11) NOT NULL,
  `role` varchar(255) NOT NULL,
  `default` tinyint(2) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `roles`
--

INSERT INTO `roles` (`id`, `role`, `default`) VALUES
(1, 'admin', 0),
(2, 'user', 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `username` varchar(50) COLLATE utf8_bin NOT NULL,
  `password` varchar(255) COLLATE utf8_bin NOT NULL,
  `email` varchar(100) COLLATE utf8_bin NOT NULL,
  `role_id` int(11) NOT NULL,
  `activated` tinyint(1) NOT NULL DEFAULT '1',
  `banned` tinyint(1) NOT NULL DEFAULT '0',
  `ban_reason` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `new_password_key` varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `new_password_requested` datetime DEFAULT NULL,
  `new_email` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `new_email_key` varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `last_ip` varchar(40) COLLATE utf8_bin NOT NULL,
  `last_login` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Extraindo dados da tabela `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `email`, `role_id`, `activated`, `banned`, `ban_reason`, `new_password_key`, `new_password_requested`, `new_email`, `new_email_key`, `last_ip`, `last_login`, `created`, `modified`) VALUES
(1, 'trupe', '$P$BtiANtNUrn3qm2BjBTdR37M2VtSnqE0', 'nilton@trupe.net', 1, 1, 0, NULL, NULL, NULL, NULL, NULL, '201.43.105.44', '2017-01-30 14:50:54', '2012-10-18 16:47:07', '2017-01-30 16:50:54'),
(2, 'cristina', '$P$BtiANtNUrn3qm2BjBTdR37M2VtSnqE0', 'niltondefreitas@gmail.com', 1, 1, 0, NULL, NULL, NULL, NULL, NULL, '200.171.223.52', '2017-02-07 15:17:17', '2012-10-26 13:29:47', '2017-02-07 17:17:17'),
(3, 'talentos', '$P$BtiANtNUrn3qm2BjBTdR37M2VtSnqE0', 'nilton@consistere.com.br', 1, 1, 0, NULL, NULL, NULL, NULL, NULL, '200.159.74.242', '2015-02-09 16:03:23', '2012-10-26 13:30:11', '2015-02-09 18:03:23');

-- --------------------------------------------------------

--
-- Estrutura da tabela `user_autologin`
--

CREATE TABLE `user_autologin` (
  `key_id` char(32) COLLATE utf8_bin NOT NULL,
  `user_id` int(11) NOT NULL DEFAULT '0',
  `user_agent` varchar(150) COLLATE utf8_bin NOT NULL,
  `last_ip` varchar(40) COLLATE utf8_bin NOT NULL,
  `last_login` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Extraindo dados da tabela `user_autologin`
--

INSERT INTO `user_autologin` (`key_id`, `user_id`, `user_agent`, `last_ip`, `last_login`) VALUES
('42d4ceef8988e4e3ac7513fe5e313d08', 2, 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:16.0) Gecko/20100101 Firefox/16.0', '200.159.74.242', '2012-10-26 16:04:07'),
('82dbeef402323007ea3bd7c6c09a1a62', 2, 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.97 Safari/537.11', '200.159.74.242', '2013-01-04 13:38:57');

-- --------------------------------------------------------

--
-- Estrutura da tabela `user_profiles`
--

CREATE TABLE `user_profiles` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `country` varchar(20) COLLATE utf8_bin DEFAULT NULL,
  `website` varchar(255) COLLATE utf8_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Extraindo dados da tabela `user_profiles`
--

INSERT INTO `user_profiles` (`id`, `user_id`, `country`, `website`) VALUES
(1, 1, NULL, NULL),
(2, 2, NULL, NULL),
(3, 3, NULL, NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `vagas`
--

CREATE TABLE `vagas` (
  `id` int(11) NOT NULL,
  `pertence_a_programa` int(11) NOT NULL DEFAULT '0',
  `vagas_programas_id` int(11) DEFAULT NULL,
  `vagas_empresas_id` int(11) DEFAULT NULL,
  `vagas_cursos_id` varchar(255) DEFAULT NULL,
  `titulo` varchar(255) DEFAULT NULL,
  `slug` varchar(255) DEFAULT NULL,
  `link` varchar(255) DEFAULT NULL,
  `data_cadastro` date DEFAULT NULL,
  `inscricoes_abertas` int(11) NOT NULL DEFAULT '1',
  `data_expiracao` date DEFAULT NULL,
  `tipo` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `vagas`
--

INSERT INTO `vagas` (`id`, `pertence_a_programa`, `vagas_programas_id`, `vagas_empresas_id`, `vagas_cursos_id`, `titulo`, `slug`, `link`, `data_cadastro`, `inscricoes_abertas`, `data_expiracao`, `tipo`) VALUES
(9, 0, 0, 7, '4,14', 'Estágio em Benefícios', 'Estgio-em-Benefcios', 'https://www.vagas.com.br/v923802', '2014-04-01', 0, '2014-05-18', 'estagio'),
(18, 0, 0, 12, '17', 'Estágio em Jurídico', 'Estgio-em-Jurdico', '', '2014-04-01', 0, '2014-05-18', 'estagio'),
(26, 0, 0, 14, '4,19', 'Estagiário em Comércio Exterior', 'Estagirio-em-Comrcio-Exterior', 'https://www.vagas.com.br/v944892', '2014-04-19', 0, '0000-00-00', 'estagio'),
(29, 1, 5, 0, '8', 'Estágio em Marketing', 'Estgio-em-Marketing', '', '2014-05-19', 0, '2014-05-15', '0'),
(30, 1, 5, 0, '4', 'Estágio em Administração de Vendas', 'Estgio-em-Administrao-de-Vendas', '', '2014-05-08', 0, '2014-05-16', '0'),
(31, 1, 5, 0, '15', 'Estágio em RH', 'Estgio-em-RH', '', '2014-05-08', 0, '2014-05-16', '0'),
(32, 1, 5, 0, '4,19', 'Estágio em Comércio Exterior', 'Estgio-em-Comrcio-Exterior', 'https://www.vagas.com.br/v907378', '2014-05-08', 0, '2014-05-16', '0'),
(33, 1, 5, 0, '12,13', 'Estágio em Comunicação', 'Estgio-em-Comunicao', '', '2014-05-08', 0, '2014-05-16', '0'),
(34, 1, 5, 0, '12,13', 'Estágio em Atendimento', 'Estgio-em-Atendimento', '', '2014-05-08', 0, '2014-05-16', '0'),
(35, 1, 5, 0, '4,20,14,21', 'Estágio em Finanças', 'Estgio-em-Finanas', '', '2014-05-20', 0, NULL, '0'),
(36, 1, 5, 0, '4,14,12,22', 'Estágio em Inteligência de Mercado', 'Estgio-em-Inteligncia-de-Mercado', '', '2014-05-20', 0, NULL, '0'),
(37, 1, 5, 0, '18', 'Estágio em Farmacotécnica', 'Estgio-em-Farmacotcnica', '', '2014-05-20', 0, NULL, '0'),
(38, 1, 5, 0, '12,13', 'Estágio em Marketing (Farma)', 'Estgio-em-Marketing-Farma', '', '2014-05-20', 0, NULL, '0'),
(39, 1, 5, 0, '12,13', 'Estágio em Marketing (Cirúrgica)', 'Estgio-em-Marketing-Cirrgica', '', '2014-05-20', 0, NULL, '0'),
(40, 1, 5, 0, '4,13', 'Estágio em Vendas', 'Estgio-em-Vendas', '', '2014-05-20', 0, NULL, '0'),
(41, 1, 5, 0, '23', 'Estágio em Manipulação', 'Estgio-em-Manipulao', '', '2014-05-20', 0, NULL, '0'),
(42, 1, 5, 0, '4,19', 'Estágio em Comércio Exterior', 'Estgio-em-Comrcio-Exterior', '', '2014-05-20', 0, NULL, '0'),
(43, 1, 5, 0, '25,24', 'Engenharia da Qualidade', 'Engenharia-da-Qualidade', '', '2014-05-20', 0, NULL, '0'),
(44, 1, 5, 0, '5,26', 'Estágio em Manutenção', 'Estgio-em-Manuteno', '', '2014-05-20', 0, NULL, '0'),
(45, 1, 5, 0, '4,14', 'Estágio em Customer Service', 'Estgio-em-Customer-Service', '', '2014-05-20', 0, NULL, '0'),
(46, 1, 6, 0, '27,15', 'Estágio em Aderência e Ética', 'Estgio-em-Aderncia-e-tica', '', '2014-05-20', 0, NULL, '0'),
(47, 1, 6, 0, '29,28,30,31', 'Estágio em Segurança e Governança da Informação', 'Estgio-em-Segurana-e-Governana-da-Informao', '', '2014-05-20', 0, NULL, '0'),
(48, 1, 6, 0, '17', 'Estágio em Inteligência Empresarial', 'Estgio-em-Inteligncia-Empresarial', '', '2014-05-20', 0, NULL, '0'),
(56, 1, 8, 0, '15', 'Estágio em Psicologia', 'Estgio-em-Psicologia', '', '2014-05-20', 0, '0000-00-00', '0'),
(57, 1, 8, 0, '24,18', 'Estágio em Controle de Qualidade', 'Estgio-em-Controle-de-Qualidade', '', '2014-05-20', 0, NULL, '0'),
(58, 1, 8, 0, '18', 'Estagio em Compliance', 'Estagio-em-Compliance', '', '2014-05-20', 0, NULL, '0'),
(59, 1, 8, 0, '5,26', 'Estágio em Manutenção', 'Estgio-em-Manuteno', '', '2014-05-20', 0, '0000-00-00', '0'),
(60, 1, 8, 0, '7,24,18', 'Estágio em Produção', 'Estgio-em-Produo', '', '2014-05-20', 0, NULL, '0'),
(61, 1, 8, 0, '7,24', 'Estágio em Planejamento', 'Estgio-em-Planejamento', '', '2014-05-20', 0, NULL, '0'),
(62, 1, 8, 0, '4,7', 'Estágio em Logística', 'Estgio-em-Logstica', '', '2014-05-20', 0, NULL, '0'),
(63, 1, 8, 0, '4,33,12,34', 'Estágio em Comunicação', 'Estgio-em-Comunicao', '', '2014-05-20', 0, NULL, '0'),
(64, 1, 8, 0, '18', 'Estágio em Farmácia', 'Estgio-em-Farmcia', '', '2014-05-20', 0, NULL, '0'),
(65, 1, 8, 0, '4', 'Estágio em Administração', 'Estgio-em-Administrao', '', '2014-05-20', 0, NULL, '0'),
(66, 1, 8, 0, '18', 'Estágio em Estabilidade', 'Estgio-em-Estabilidade', '', '2014-05-20', 0, NULL, '0'),
(67, 1, 8, 0, '4', 'Estágio em Compras', 'Estgio-em-Compras', '', '2014-05-20', 0, NULL, '0'),
(68, 1, 8, 0, '18,23', 'Estágio em Validação de Métodos de Produto', 'Estgio-em-Validao-de-Mtodos-de-Produto', '', '2014-05-20', 0, NULL, '0'),
(69, 1, 8, 0, '18', 'Estágio em Farmacovigilância', 'Estgio-em-Farmacovigilncia', '', '2014-05-20', 0, NULL, '0'),
(70, 1, 8, 0, '18', 'Estágio em Sistema de Qualidade', 'Estgio-em-Sistema-de-Qualidade', '', '2014-05-20', 0, NULL, '0'),
(71, 1, 6, 0, '4,14', 'Estágio em Riscos e Negócios ', 'Estgio-em-Riscos-e-Negcios', '', '2014-05-21', 0, NULL, '0'),
(74, 0, 0, 17, '35', 'Estágio em Secretariado', 'Estgio-em-Secretariado', '', '2014-04-24', 0, '0000-00-00', 'estagio'),
(84, 0, 0, 19, '5,26', 'Estágio em Infraestrutura', 'Estgio-em-Infraestrutura', '', '2014-04-08', 0, '0000-00-00', 'estagio'),
(85, 0, 0, 20, '4,20,17', 'Estágio em Controladoria', 'Estgio-em-Controladoria', '', '2014-05-16', 0, '0000-00-00', 'estagio'),
(87, 0, 0, 11, '4', 'Estágio em Novos Negócios', 'Estgio-em-Novos-Negcios', '', '2014-04-04', 0, '0000-00-00', 'estagio'),
(94, 0, 0, 21, '29,30,31', 'Estágio em TI', 'Estgio-em-TI', '', '2014-04-07', 0, '0000-00-00', 'estagio'),
(95, 0, 0, 22, '4,40,14,19', 'Estágio em Compras', 'Estgio-em-Compras', '', '2014-04-02', 0, '0000-00-00', 'estagio'),
(97, 0, 0, 23, '17', 'Estágio em Licitações', 'Estgio-em-Licitaes', '', '2014-04-01', 0, '0000-00-00', 'estagio'),
(98, 1, 11, 0, '18', 'Estágio em Qualidade', 'Estgio-em-Qualidade', '', '2014-05-22', 0, NULL, '0'),
(99, 1, 11, 0, '18,23', 'Estágio em Controle da Qualidade', 'Estgio-em-Controle-da-Qualidade', '', '2014-05-22', 0, NULL, '0'),
(100, 1, 11, 0, '39,15', 'Estágio em RH', 'Estgio-em-RH', '', '2014-05-22', 0, NULL, '0'),
(101, 1, 12, 0, '4,5,12', 'Estágio em Atendimento Técnico', 'Estgio-em-Atendimento-Tcnico', '', '2014-05-22', 0, NULL, '0'),
(102, 1, 12, 0, '7,24', 'Estágio em Planejamento', 'Estgio-em-Planejamento', '', '2014-05-22', 0, NULL, '0'),
(103, 1, 12, 0, '7,24', 'Estágio em Make', 'Estgio-em-Make', '', '2014-05-22', 0, NULL, '0'),
(104, 1, 12, 0, '24,23,41', 'Estágio em Qualidade Assegurada ', 'Estgio-em-Qualidade-Assegurada', '', '2014-05-22', 0, NULL, '0'),
(105, 1, 12, 0, '4,7,24,43,42,23', 'Estágio em Suporte Técnico', 'Estgio-em-Suporte-Tcnico', '', '2014-05-22', 0, NULL, '0'),
(106, 1, 12, 0, '44', 'Estágio em Meio Ambiente ', 'Estgio-em-Meio-Ambiente', '', '2014-05-22', 0, NULL, '0'),
(107, 1, 12, 0, '4,20,14,45', 'Estágio em Crédito e Cobrança', 'Estgio-em-Crdito-e-Cobrana', '', '2014-05-22', 0, NULL, '0'),
(108, 1, 12, 0, '4,40,7,26,24', 'Estágio em Compras', 'Estgio-em-Compras', '', '2014-05-22', 0, NULL, '0'),
(109, 1, 12, 0, '4,20,14,21', 'Estágio em Finanças', 'Estgio-em-Finanas', '', '2014-05-22', 0, NULL, '0'),
(110, 1, 13, 0, '32', 'Estágio em Engenharia Civil', 'Estgio-em-Engenharia-Civil', '', '2014-05-22', 0, NULL, '0'),
(111, 1, 13, 0, '4,46,35', 'Estágio Administrativo', 'Estgio-Administrativo', '', '2014-05-22', 0, NULL, '0'),
(112, 1, 13, 0, '32', 'Estágio em Engenharia Civil - Qualidade', 'Estgio-em-Engenharia-Civil-Qualidade', '', '2014-05-22', 0, NULL, '0'),
(113, 1, 13, 0, '32', 'Engenharia Civil - Produção', 'Engenharia-Civil-Produo', '', '2014-05-22', 0, NULL, '0'),
(114, 1, 13, 0, '32', 'Estágio em Engenharia Civil - Produção/ Parque da Cidade', 'Estgio-em-Engenharia-Civil-Produo-Parque-da-Cidade', '', '2014-05-22', 0, NULL, '0'),
(115, 1, 13, 0, '32', 'Engenharia Civil - Produção/ SP', 'Engenharia-Civil-Produo-SP', '', '2014-05-22', 0, NULL, '0'),
(116, 1, 13, 0, '47,32', 'Estágio em Engenharia Civil - Incorporação ', 'Estgio-em-Engenharia-Civil-Incorporao', '', '2014-05-22', 0, NULL, '0'),
(118, 1, 14, 0, '26', 'Engenharia Mecânica', 'Engenharia-Mecnica', '', '2014-05-23', 0, NULL, '0'),
(119, 1, 14, 0, '7', 'Produção Engenharia do Produto ', 'Produo-Engenharia-do-Produto', '', '2014-05-23', 0, NULL, '0'),
(120, 1, 14, 0, '7', 'Produção Lean ', 'Produo-Lean', '', '2014-05-23', 0, NULL, '0'),
(121, 1, 14, 0, '4,14,45', 'Finanças ', 'Finanas', '', '2014-05-23', 0, NULL, '0'),
(122, 1, 14, 0, '48', 'Mecatrônica Supply Chain ', 'Mecatrnica-Supply-Chain', '', '2014-05-23', 0, NULL, '0'),
(123, 1, 14, 0, '4', 'Administração', 'Administrao', '', '2014-05-23', 0, NULL, '0'),
(124, 1, 14, 0, '4,14', 'Contabilidade', 'Contabilidade', '', '2014-05-23', 0, NULL, '0'),
(125, 1, 14, 0, '4', 'Administração Supply Chain ', 'Administrao-Supply-Chain', '', '2014-05-23', 0, NULL, '0'),
(126, 1, 14, 0, '15', 'Psicologia', 'Psicologia', '', '2014-05-23', 0, NULL, '0'),
(127, 1, 14, 0, '15', 'Recursos Humanos', 'Recursos-Humanos', '', '2014-05-23', 0, NULL, '0'),
(128, 1, 14, 0, '4', 'Administração Pricing ', 'Administrao-Pricing', '', '2014-05-23', 0, NULL, '0'),
(129, 1, 14, 0, '12', 'Marketing ', 'Marketing', '', '2014-05-23', 0, NULL, '0'),
(130, 1, 14, 0, '4', 'Vendas', 'Vendas', '', '2014-05-23', 0, NULL, '0'),
(134, 0, 0, 28, '21,44,32,28,25,7,5,11,26,24,18,23', 'Estágio em Produção', 'Estgio-em-Produo', 'https://www.vagas.com.br/v961561', '2014-05-27', 0, '0000-00-00', 'estagio'),
(153, 0, 0, 13, '47', 'Estágio em Arquitetura e Urbanismo', 'Estgio-em-Arquitetura-e-Urbanismo', 'https://www.vagas.com.br/v1001882', '2014-07-28', 0, '0000-00-00', 'estagio'),
(156, 0, 0, 31, '4,37,38,31', 'Estágio em Eletrotécnico', 'Estgio-em-Eletrotcnico', 'https://www.vagas.com.br/v984561 ', '2014-07-14', 0, '0000-00-00', 'estagio'),
(185, 0, 0, 3, '7', 'Estágio em Supply Chain', 'Estgio-em-Supply-Chain', 'https://www.vagas.com.br/v1046650', '2014-10-02', 0, '0000-00-00', 'estagio'),
(214, 0, 0, 18, '44,23', 'Estágio em Segurança do Trabalho ', 'Estgio-em-Segurana-do-Trabalho', 'https://www.vagas.com.br/v1083488', '2014-12-02', 0, '0000-00-00', 'estagio'),
(227, 0, 0, 13, '32', 'Estagiário de Engenharia Civil ', 'Estagirio-de-Engenharia-Civil', 'https://www.vagas.com.br/v1110091', '2015-01-22', 0, '0000-00-00', 'estagio'),
(260, 0, 0, 51, '17', 'Estágio em Direito - Área Tributária', 'Estgio-em-Direito-rea-Tributria', 'https://www.vagas.com.br/v1163195', '2015-06-08', 0, '0000-00-00', 'estagio'),
(265, 0, 0, 41, '67', 'Estágio em Fisioterapia (Campinas / SP) ', 'Estgio-em-Fisioterapia-Campinas-SP', 'https://www.vagas.com.br/v1155291', '2015-05-12', 0, '0000-00-00', 'estagio'),
(318, 0, 0, 6, '4,12', 'Estágio em Customer Service ', 'Estgio-em-Customer-Service', 'https://www.vagas.com.br/v1283194', '2015-12-17', 0, '0000-00-00', 'estagio'),
(319, 0, 0, 14, '21,24', 'Estágio em Química - Controle da Qualidade ', 'Estgio-em-Qumica-Controle-da-Qualidade', 'https://www.vagas.com.br/v1282212', '2015-12-17', 0, '0000-00-00', 'estagio'),
(325, 0, 0, 4, '29,28,11', 'Estágio em Segurança e Governança em TI ', 'Estgio-em-Segurana-e-Governana-em-TI', 'https://www.vagas.com.br/v1325742', '2016-03-23', 0, '0000-00-00', 'estagio'),
(326, 0, 0, 51, '4,14,7', 'Estágio em Planejamento Financeiro ', 'Estgio-em-Planejamento-Financeiro', 'https://www.vagas.com.br/v1326131', '2016-03-23', 0, '0000-00-00', 'estagio'),
(330, 0, 0, 54, '24', 'Estagiário em Produção (ênfase em Química)', 'Estagirio-em-Produo-nfase-em-Qumica', 'https://www.vagas.com.br/v1337204', '2016-04-15', 0, '0000-00-00', 'estagio'),
(333, 0, 0, 49, '4,14,21', 'Estagiário em Governanças e Processos', 'Estagirio-em-Governanas-e-Processos', 'https://www.vagas.com.br/v1459803', '2017-02-07', 1, '0000-00-00', 'estagio'),
(346, 0, 0, 4, '20,29,21,28', 'Estágio em Segurança e Governança em TI', 'Estgio-em-Segurana-e-Governana-em-TI', 'https://www.vagas.com.br/v1354054', '2016-05-23', 0, '0000-00-00', 'estagio'),
(382, 0, 0, 37, '4,12,13', 'Estagiário em Marketing - Canais digitais ', 'Estagirio-em-Marketing-Canais-digitais', 'https://www.vagas.com.br/v1403360', '2016-09-13', 0, '0000-00-00', 'estagio'),
(384, 0, 0, 38, '58', 'Estagiário em Logística ', 'Estagirio-em-Logstica', 'https://www.vagas.com.br/v1438877', '2017-01-09', 0, '0000-00-00', 'estagio'),
(385, 0, 0, 59, '12,13', 'Estágio em Marketing Digital', 'Estgio-em-Marketing-Digital', 'https://www.vagas.com.br/v1426286', '2016-11-17', 0, '0000-00-00', 'estagio'),
(386, 0, 0, 49, '17', 'Estagiário em Direito', 'Estagirio-em-Direito', 'https://www.vagas.com.br/v1426193', '2016-11-17', 0, '0000-00-00', 'estagio'),
(387, 0, 0, 39, '29,31', 'Estagiário em Suporte', 'Estagirio-em-Suporte', 'https://www.vagas.com.br/v1436974', '2016-11-24', 0, '0000-00-00', 'estagio'),
(388, 0, 0, 39, '29,31', 'Estagiário em Desenvolvimento Web ', 'Estagirio-em-Desenvolvimento-Web', 'https://www.vagas.com.br/v1436954', '2016-11-24', 0, '0000-00-00', 'estagio'),
(389, 0, 0, 57, '17', 'Estagiário Jurídico', 'Estagirio-Jurdico', 'https://www.vagas.com.br/v1430813', '2016-12-29', 0, '0000-00-00', 'estagio'),
(390, 0, 0, 4, '29,28,69', 'Estagiário em Segurança da Informação', 'Estagirio-em-Segurana-da-Informao', 'https://www.vagas.com.br/v1450122', '2017-01-02', 0, '0000-00-00', 'estagio'),
(391, 0, 0, 49, '12,13', 'Estagiário de Marketing em Consumer', 'Estagirio-de-Marketing-em-Consumer', 'https://www.vagas.com.br/v1450806', '2016-12-27', 0, '0000-00-00', 'estagio'),
(392, 0, 0, 14, '23', 'Estagiário em Química', 'Estagirio-em-Qumica', 'https://www.vagas.com.br/v1456640', '2017-02-07', 1, '0000-00-00', 'estagio'),
(393, 0, 0, 37, '17', 'Estagiário em Direito', 'Estagirio-em-Direito', 'https://www.vagas.com.br/v1465525', '2017-01-30', 0, '0000-00-00', 'estagio'),
(394, 0, 0, 34, '59,13', 'Estagiário em Comunicação e Marketing', 'Estagirio-em-Comunicao-e-Marketing', 'https://vagas.com.br/v1468222', '2017-02-07', 1, NULL, 'estagio');

-- --------------------------------------------------------

--
-- Estrutura da tabela `vagas_cursos`
--

CREATE TABLE `vagas_cursos` (
  `id` int(11) NOT NULL,
  `titulo` varchar(255) DEFAULT NULL,
  `slug` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `vagas_cursos`
--

INSERT INTO `vagas_cursos` (`id`, `titulo`, `slug`) VALUES
(4, 'Administração', 'administrao'),
(5, 'Engenharia Elétrica', 'engenharia-eltrica'),
(7, 'Engenharia de Produção', 'engenharia-de-produo'),
(8, 'Matemática', 'matemtica'),
(9, 'Estatística', 'estatstica'),
(10, 'Física', 'fsica'),
(11, 'Engenharia Eletrônica', 'engenharia-eletrnica'),
(12, 'Marketing', 'marketing'),
(13, 'Publicidade e Propaganda', 'publicidade-e-propaganda'),
(14, 'Economia', 'economia'),
(15, 'Psicologia', 'psicologia'),
(16, 'Biologia', 'biologia'),
(17, 'Direito', 'direito'),
(18, 'Farmácia', 'farmcia'),
(19, 'Relações Internacionais', 'relaes-internacionais'),
(20, 'Ciências Contábeis', 'cincias-contbeis'),
(21, 'Engenharia', 'engenharia'),
(22, 'Processamento de Dados', 'processamento-de-dados'),
(23, 'Química', 'qumica'),
(24, 'Engenharia Química', 'engenharia-qumica'),
(25, 'Engenharia de Alimentos', 'engenharia-de-alimentos'),
(26, 'Engenharia Mecânica', 'engenharia-mecnica'),
(27, 'Ciências Sociais', 'cincias-sociais'),
(28, 'Engenharia da Computação', 'engenharia-da-computao'),
(29, 'Ciências da Computação ', 'cincias-da-computao'),
(30, 'Informática', 'informtica'),
(31, 'Tecnologia da Informação', 'tecnologia-da-informao'),
(32, 'Engenharia Civil', 'engenharia-civil'),
(33, 'Comunicação', 'comunicao'),
(34, 'Relações Públicas', 'relaes-pblicas'),
(35, 'Secretariado', 'secretariado'),
(36, 'Gestão de Processos ', 'gesto-de-processos'),
(37, 'Análise de Desenvolvimento de Sistemas', 'anlise-de-desenvolvimento-de-sistemas'),
(38, 'Segurança da Informação', 'segurana-da-informao'),
(39, 'Gestão de Recursos Humanos', 'gesto-de-recursos-humanos'),
(40, 'Comércio Exterior', 'comrcio-exterior'),
(41, 'Química Industrial ', 'qumica-industrial'),
(42, 'Mecânica ', 'mecnica'),
(43, 'Manutenção', 'manuteno'),
(44, 'Engenharia Ambiental ', 'engenharia-ambiental'),
(45, 'Gestão Financeira', 'gesto-financeira'),
(46, 'Letras', 'letras'),
(47, 'Arquitetura', 'arquitetura'),
(48, 'Mecatrônica ', 'mecatrnica'),
(49, 'Técnico em Refrigeração', 'tcnico-em-refrigerao'),
(50, 'Técnico em Elétrica', 'tcnico-em-eltrica'),
(51, 'Técnico em Eletrônica', 'tcnico-em-eletrnica'),
(52, 'Técnico em Eletrotécnico ', 'tcnico-em-eletrotcnico'),
(53, 'Refrigeração', 'refrigerao'),
(54, 'Elétrica', 'eltrica'),
(55, 'Eletrotécnica', 'eletrotcnica'),
(56, 'Eletromecânica', 'eletromecnica'),
(57, 'Eletroeletrônica', 'eletroeletrnica'),
(58, 'Logística', 'logstica'),
(59, 'Jornalismo', 'jornalismo'),
(60, 'T.I', 'ti'),
(61, 'Finanças', 'finanas'),
(62, 'Recursos Humanos', 'recursos-humanos'),
(63, 'Gestão Ambiental', 'gesto-ambiental'),
(64, 'Ciências Econômicas', 'cincias-econmicas'),
(65, 'Engenharia Biomédica', 'engenharia-biomdica'),
(66, 'Pedagogia', 'pedagogia'),
(67, 'Fisioterapia', 'fisioterapia'),
(68, 'Biomedicina', 'biomedicina'),
(69, 'Sistema de Informações', 'sistema-de-informaes'),
(70, 'Engenharia de Processos', 'engenharia-de-processos'),
(71, 'Técnico em Mecânica', 'tcnico-em-mecnica'),
(72, 'Bioquímica', 'bioqumica'),
(73, 'Contabilidade', 'contabilidade'),
(74, 'Turismo ', 'turismo'),
(75, 'Engenharia Metalúrgica', 'engenharia-metalrgica'),
(76, 'Hotelaria', 'hotelaria'),
(77, 'Estátisticas', 'esttisticas');

-- --------------------------------------------------------

--
-- Estrutura da tabela `vagas_empresas`
--

CREATE TABLE `vagas_empresas` (
  `id` int(11) NOT NULL,
  `titulo` varchar(255) DEFAULT NULL,
  `slug` varchar(255) DEFAULT NULL,
  `imagem` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `vagas_empresas`
--

INSERT INTO `vagas_empresas` (`id`, `titulo`, `slug`, `imagem`) VALUES
(3, 'Novartis', 'novartis', 'novartislogo.jpg'),
(4, 'ICTS', 'icts', 'icts3.png'),
(6, 'Alcon', 'alcon', 'alcon4.jpg'),
(7, 'Ingredion', 'ingredion', 'ingredionlogofeature3.jpg'),
(11, 'São Carlos', 'so-carlos', 'são_carlos_empreendimentos_e_participações.jpg'),
(12, 'Ticket', 'ticket', 'ticket.gif'),
(13, 'Odebrecht', 'odebrecht', 'odebrecht_engenharia.jpg'),
(14, 'Eastman', 'eastman', 'eastman.jpg'),
(16, 'Duratex', 'duratex', 'duratex.jpg'),
(17, 'Eldorado', 'eldorado', 'eldorado_brasil.jpg'),
(18, 'MSD', 'msd', 'msdjpg.jpg'),
(19, 'NET', 'net', 'net.jpg'),
(20, 'Nidera', 'nidera', 'nidera.jpg'),
(21, 'GS1', 'gs1', 'gs1_brasil.jpg'),
(22, 'Legrand', 'legrand', 'legrand.jpg'),
(23, 'Edenred', 'edenred', 'edenred.jpg'),
(24, 'ICL', 'icl', 'icl_brasil.gif'),
(25, 'Diversey', 'diversey', 'diversey.jpg'),
(27, 'Parker', 'parker', 'parker.jpg'),
(28, 'diosynth', 'diosynth', 'diosynth.gif'),
(30, 'BestCenter', 'bestcenter', 'bestcenter.jpg'),
(31, 'Aceco', 'aceco', 'aceco.jpg'),
(32, 'Empresa Confidencial', 'empresa-confidencial', 'confidencial.gif'),
(34, 'Grupo Meta RH ', 'grupo-meta-rh', 'marca_grupo_metarh.png'),
(37, 'Multinacional Financeira', 'multinacional-financeira', 'oi1.png'),
(38, 'Pantos', 'pantos', 'pantos.jpg'),
(39, 'Multiedro', 'multiedro', 'multiedro_horiz_2d_cmyk-1-1-convertido-300x114.jpg'),
(40, 'Anovis', 'anovis', 'anovis.jpg'),
(41, 'Air Liquide', 'air-liquide', 'air_liquide.svg_.png'),
(42, 'Bocater', 'bocater', 'bccs_logo_1111114_1532.jpg'),
(43, 'Varian', 'varian', 'varian_medical_systems_logo.jpg'),
(44, 'Air Liquide 1', 'air-liquide-1', 'air_liquide1.png'),
(45, 'Importante empresa global', 'importante-empresa-global', '11111111111111111.png'),
(46, 'Farmacêutica', 'farmacutica', 'multinacional_farmacêutica.png'),
(47, 'Empresa Global', 'empresa-global', 'global.png'),
(48, 'Empresa de comunicação', 'empresa-de-comunicao', 'comunicação.png'),
(49, 'Empresa Multinacional ', 'empresa-multinacional', 'global.png'),
(50, 'Serasa Experian', 'serasa-experian', 'logo.png'),
(51, 'Fleury', 'fleury', 'fleury.gif'),
(52, 'ABBI', 'abbi', 'abbi.png'),
(53, 'SANOFI', 'sanofi', 'sanofi.svg_.png'),
(54, 'Brasmetal', 'brasmetal', 'logo-brasmetal-300x300.png'),
(57, 'Confidence Câmbio', 'confidence-cmbio', 'confidence-atual.png'),
(59, 'Consultoria Empresarial', 'consultoria-empresarial', 'alo2.png');

-- --------------------------------------------------------

--
-- Estrutura da tabela `vagas_novo`
--

CREATE TABLE `vagas_novo` (
  `id` int(11) NOT NULL,
  `vaga_categoria` varchar(255) NOT NULL,
  `vaga_imagem` varchar(255) DEFAULT NULL,
  `vaga_empresa` varchar(255) NOT NULL,
  `vaga_titulo` varchar(255) NOT NULL,
  `vaga_descritivo` text NOT NULL,
  `vaga_quantidade` int(10) NOT NULL,
  `vaga_link` varchar(255) NOT NULL,
  `vaga_data_cadastro` int(10) NOT NULL,
  `vaga_data_expiracao` int(255) NOT NULL,
  `vaga_destaque` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `vagas_novo`
--

INSERT INTO `vagas_novo` (`id`, `vaga_categoria`, `vaga_imagem`, `vaga_empresa`, `vaga_titulo`, `vaga_descritivo`, `vaga_quantidade`, `vaga_link`, `vaga_data_cadastro`, `vaga_data_expiracao`, `vaga_destaque`) VALUES
(144, 'estagiarios', 'NovartisLOGO.jpg', 'Multinacional (Segmento farmacêutico)', 'Estágio em Compras', '<p align="justify">Somando a excel&ecirc;ncia, credibilidade e solidez do Grupo Meta RH, a Meta Talentos &eacute; a divis&atilde;o de jovens talentos do Grupo Meta RH, especializada na coordena&ccedil;&atilde;o de programas de est&aacute;gios e trainees, cujo objetivo &eacute; atrair e desenvolver jovens capazes de assumir posi&ccedil;&otilde;es de lideran&ccedil;a no futuro.<br /><br />Estamos assessorando empresa multinacional, de grande porte,do segmento farmac&ecirc;utico localizada na regi&atilde;o sul de S&atilde;o Paulo, na contrata&ccedil;&atilde;o de:</p>\n<p><br /><br /><span id="Pr2Tn" style="font-family: Verdana, Arial; font-size: large;"><strong>Est&aacute;gio em Compras</strong></span><br /><br /><span id="Pr2Tn" style="font-family: Verdana, Arial; font-size: small;"><strong>Atividades:</strong>&nbsp;<br /><br />&bull; Auxiliar no monitoramento de entrega dos materiais junto aos fornecedores;<br />&bull; Acompanhar as pend&ecirc;ncias na &aacute;rea de Contas a Pagar;<br />&bull; Acompanhar a vig&ecirc;ncia dos contratos e cat&aacute;logos;<br />&bull; Dar suporte nos relat&oacute;rios e indicadores na &aacute;rea de Compras;<br />&bull; Auxiliar na Qualifica&ccedil;&atilde;o de novos fornecedores.<br /><br /><strong>Requisitos:</strong>&nbsp;<br /><br />&bull; Forma&ccedil;&atilde;o: Cursando Administra&ccedil;&atilde;o;&nbsp;<br />&bull; Inform&aacute;tica: Pacote Office Intermedi&aacute;rio;&nbsp;<br />&bull; Idiomas: Ingl&ecirc;s Intermedi&aacute;rio;&nbsp;<br />&bull; Experi&ecirc;ncia/Conhecimentos: N.A.&nbsp;<br /><br /><strong>Informa&ccedil;&otilde;es Adicionais:</strong><br /><br />&bull; Bolsa aux&iacute;lio: R$ 1350,00;<br />&bull; Benef&iacute;cios: Assist&ecirc;ncia m&eacute;dica; Assist&ecirc;ncia odontol&oacute;gica; Refeit&oacute;rio no Local; Fretado e Seguro de vida;<br />&bull; Hor&aacute;rio: Das 8h00 &agrave;s 15h00;<br />&bull; Local de trabalho: Tabo&atilde;o da Serra - S&atilde;o Paulo;&nbsp;<br />&bull; Modalidade contratual: Est&aacute;gio.</span></p>', 1, 'https://www.vagas.com.br/v943689', 1399403108, 1404097200, 1),
(145, 'estagiarios', 'ICTS3.png', 'Multinacional (Segmento consultoria e serviços)', 'Estágio em Gestão da Segurança', '<p><span id="Pr2Tn" style="font-family: Verdana, Arial; font-size: small;">Somando a excel&ecirc;ncia, credibilidade e solidez do Grupo Meta RH, a Meta Talentos &eacute; especializada em Programas de Trainees, Estagi&aacute;rios e Desenvolvimento de Jovens Talentos. Selecionamos jovens com potencial, oferecendo oportunidades de desenvolvimento e crescimento em empresas renomadas, que os preparam para assumir no futuro, posi&ccedil;&otilde;es desafiadoras.<br /><br />Estamos assessorando empresa multinacional, de grande porte, do segmento de consultoria e servi&ccedil;os em gest&atilde;o de riscos de neg&oacute;cios, localizada na regi&atilde;o sul de S&atilde;o Paulo, na contrata&ccedil;&atilde;o de:</span><br /><br /><span id="Pr2Tn" style="font-family: Verdana, Arial; font-size: large;"><strong>Est&aacute;gio em Gest&atilde;o da Seguran&ccedil;a</strong></span><br /><br /><span id="Pr2Tn" style="font-family: Verdana, Arial; font-size: small;"><strong>Atividades:</strong>&nbsp;<br /><br />&bull; Realizar o diagn&oacute;stico atual sobre os riscos atrelados a cada elemento de seguran&ccedil;a;<br />&bull; Garantir solu&ccedil;&otilde;es integradas e din&acirc;micas de seguran&ccedil;a com cobertura de todo o ciclo do projeto, incluindo avalia&ccedil;&atilde;o de riscos, desenvolvimento de solu&ccedil;&otilde;es, suporte na implanta&ccedil;&atilde;o e suporte cont&iacute;nuo;<br />&bull; Assegurar o cumprimento de prazos e qualidade do projeto de seguran&ccedil;a, expondo seu ponto de vista t&eacute;cnico e de precis&atilde;o da instala&ccedil;&atilde;o em rela&ccedil;&atilde;o &agrave;s especificidades dos equipamentos.<br /><br /><strong>Requisitos:</strong>&nbsp;<br /><br />&bull; Forma&ccedil;&atilde;o: Cursando Engenharia El&eacute;trica ou Eletr&ocirc;nica;<br />&bull; Inform&aacute;tica: Excel Avan&ccedil;ado;&nbsp;<br />&bull; Idiomas: Ingl&ecirc;s Intermedi&aacute;rio;&nbsp;<br />&bull; Experi&ecirc;ncia/Conhecimentos: N&atilde;o se aplica.&nbsp;<br /><br /><strong>Informa&ccedil;&otilde;es Adicionais:</strong><br /><br />&bull; Bolsa aux&iacute;lio: R$ 1.200,00;<br />&bull; Benef&iacute;cios: Vale transporte, Vale refei&ccedil;&atilde;o (25,00) e Seguro de vida;<br />&bull; Hor&aacute;rio: Das 8h00 &agrave;s 15h00 ou das 09h00 &agrave;s 16h00;<br />&bull; Local de trabalho: Berrini &ndash; Zona Sul de S&atilde;o Paulo;<br />&bull; Modalidade contratual: Est&aacute;gio.</span></p>', 5, 'https://www.vagas.com.br/v925044', 1399403276, 1404097200, 1),
(146, 'estagiarios', 'NovartisLOGO1.jpg', 'Multinacional (Segmento farmacêutico)', 'Estágio em Qualidade', '<p align="justify">Somando a excel&ecirc;ncia, credibilidade e solidez do Grupo Meta RH, a Meta Talentos &eacute; a divis&atilde;o de jovens talentos do Grupo Meta RH, especializada na coordena&ccedil;&atilde;o de programas de est&aacute;gios e trainees, cujo objetivo &eacute; atrair e desenvolver jovens capazes de assumir posi&ccedil;&otilde;es de lideran&ccedil;a no futuro.<br /><br />Estamos assessorando empresa multinacional, de grande porte, do segmento farmac&ecirc;utico localizada na regi&atilde;o sul de S&atilde;o Paulo, na contrata&ccedil;&atilde;o de:</p>\n<p><br /><br /><span id="Pr2Tn" style="font-family: Verdana, Arial; font-size: large;"><strong>Est&aacute;gio em Qualidade</strong></span><br /><br /><span id="Pr2Tn" style="font-family: Verdana, Arial; font-size: small;"><strong>Atividades:</strong>&nbsp;<br /><br />&bull; Acompanhamento de atividades executadas na sala das c&acirc;maras clim&aacute;ticas e monitoramento no sistema supervis&oacute;rio;<br />&bull; Aux&iacute;lio na realiza&ccedil;&atilde;o de tarefas administrativas da &aacute;rea de Planning &amp; Report;<br />&bull; Cria&ccedil;&atilde;o e acompanhamento de estudos de estabilidade no sistema LIMS (SAP);<br />&bull; Aux&iacute;lio na pesquisa e gerenciamento de lotes de produtos para entrarem em estudo de FUST.<br /><br /><strong>Requisitos:</strong>&nbsp;<br /><br />&bull; Forma&ccedil;&atilde;o: Cursando Farm&aacute;cia;&nbsp;<br />&bull; Inform&aacute;tica: Pacote Office Intermedi&aacute;rio;&nbsp;<br />&bull; Idiomas: Ingl&ecirc;s Intermedi&aacute;rio;&nbsp;<br />&bull; Experi&ecirc;ncia/Conhecimentos: N.A.&nbsp;<br /><br /><strong>Informa&ccedil;&otilde;es Adicionais:</strong><br /><br />&bull; Bolsa aux&iacute;lio: R$ 1350,00;<br />&bull; Benef&iacute;cios: Assist&ecirc;ncia m&eacute;dica; Assist&ecirc;ncia odontol&oacute;gica; Refeit&oacute;rio no Local; Fretado e Seguro de vida;<br />&bull; Hor&aacute;rio: Das 8h00 &agrave;s 15h00;<br />&bull; Local de trabalho: Tabo&atilde;o da Serra - S&atilde;o Paulo;&nbsp;<br />&bull; Modalidade contratual: Est&aacute;gio.</span></p>', 5, 'https://www.vagas.com.br/v942450', 1399403354, 1404097200, 1),
(147, 'estagiarios', 'SERASA.jpg', 'Multinacional (Segmento Banco de dados)', 'Estágio na área de Recursos Humanos - Análise de Dados', '<p align="justify">Somando a excel&ecirc;ncia, credibilidade e solidez do Grupo Meta RH, a Meta Talentos &eacute; a divis&atilde;o de jovens talentos do Grupo Meta RH, especializada na coordena&ccedil;&atilde;o de programas de est&aacute;gios e trainees, cujo objetivo &eacute; atrair e desenvolver jovens capazes de assumir posi&ccedil;&otilde;es de lideran&ccedil;a no futuro.<br /><br />Estamos assessorando empresa Multinacional, de grande porte, do segmento de Banco de Dados localizada na regi&atilde;o sul de S&atilde;o Paulo, na contrata&ccedil;&atilde;o de:</p>\n<p><br /><br /><span id="Pr2Tn" style="font-family: Verdana, Arial; font-size: large;"><strong>Est&aacute;gio na &aacute;rea de Recursos Humanos - An&aacute;lise de Dados</strong></span><br /><br /><span id="Pr2Tn" style="font-family: Verdana, Arial; font-size: small;"><strong>Atividades:</strong>&nbsp;<br /><br />&bull; Desenvolvimento de modelos matem&aacute;ticos;<br />&bull; Controle dos dados estat&iacute;sticos da &aacute;rea de Recursos Humanos;<br />&bull; Elabora&ccedil;&atilde;o de relat&oacute;rios gerenciais;<br />&bull; Apontamentos de necessidades de melhorias cont&iacute;nuas;<br />&bull; An&aacute;lise do desempenho da &aacute;rea e suporte gerencial.<br /><br /><strong>Requisitos:</strong>&nbsp;<br /><br />&bull; Forma&ccedil;&atilde;o: Cursando Engenharia de Produ&ccedil;&atilde;o, Matem&aacute;tica, Estat&iacute;stica F&iacute;sica e &aacute;reas afins (Exatas);&nbsp;<br />&bull; Inform&aacute;tica:Excel avan&ccedil;ado;&nbsp;<br />&bull; Idiomas: Ingl&ecirc;s avan&ccedil;ado;&nbsp;<br />&bull; Experi&ecirc;ncia/Conhecimentos: Desej&aacute;vel conhecimento em programa&ccedil;&atilde;o Python e/ou C#.&nbsp;<br /><br /><strong>Informa&ccedil;&otilde;es Adicionais:</strong><br /><br />&bull; Bolsa aux&iacute;lio: R$ 10,00/hora;<br />&bull; Benef&iacute;cios: Vale transporte, vale refei&ccedil;&atilde;o e seguro de vida;<br />&bull; Hor&aacute;rio: &agrave; combinar;<br />&bull; Local de trabalho: Zona Sul de S&atilde;o Paulo (pr&oacute;ximo &agrave; esta&ccedil;&atilde;o S&atilde;o Judas do metr&ocirc;);<br />&bull; Modalidade contratual: Est&aacute;gio.</span></p>', 1, 'https://www.vagas.com.br/v940762', 1399403471, 1404097200, 1),
(148, 'estagiarios', 'alcon4.jpg', 'Multinacional (Segmento farmacêutico)', 'Estágio em Marketing', '<p align="justify">Somando a excel&ecirc;ncia, credibilidade e solidez do Grupo Meta RH, a Meta Talentos &eacute; a divis&atilde;o de jovens talentos do Grupo Meta RH, especializada na coordena&ccedil;&atilde;o de programas de est&aacute;gios e trainees, cujo objetivo &eacute; atrair e desenvolver jovens capazes de assumir posi&ccedil;&otilde;es de lideran&ccedil;a no futuro.<br /><br />Estamos assessorando empresa multinacional, de grande porte, do segmento farmac&ecirc;utico, localizada na regi&atilde;o do Brooklin, zona sul de S&atilde;o Paulo, na contrata&ccedil;&atilde;o de:</p>\n<p><br /><br /><span id="Pr2Tn" style="font-family: Verdana, Arial; font-size: large;"><strong>Estagi&aacute;rio em Marketing</strong></span><br /><br /><span id="Pr2Tn" style="font-family: Verdana, Arial; font-size: small;"><strong>Atividades:</strong>&nbsp;<br /><br />&bull; Suporte &agrave; estrat&eacute;gias de Marketing;<br />&bull; Desenvolvimento de materiais promocionais, suporte e follow-up a cria&ccedil;&otilde;es junto &agrave;s ag&ecirc;ncias;<br />&bull; Administra&ccedil;&atilde;o dos pedidos de compras e suporte do controle de verbas;<br />&bull; Suporte e controle sobre as necessidades da for&ccedil;a de vendas na execu&ccedil;&atilde;o das estrat&eacute;gias de MKT e ciclos promocionais.<br /><br /><br /><strong>Requisitos:</strong>&nbsp;<br /><br />&bull; Forma&ccedil;&atilde;o: Gradua&ccedil;&atilde;o em Marketing ou Publicidade e Propaganda;&nbsp;<br />&bull; Inform&aacute;tica: Excel Avan&ccedil;ado;&nbsp;<br />&bull; Idiomas: Ingl&ecirc;s Intermedi&aacute;rio;&nbsp;<br />&bull; Experi&ecirc;ncia/Conhecimentos: N.A.<br /><br /><strong>Informa&ccedil;&otilde;es Adicionais:</strong><br /><br />&bull; Bolsa aux&iacute;lio: R$ 1350,00;<br />&bull; Benef&iacute;cios: Assist&ecirc;ncia m&eacute;dica, Assist&ecirc;ncia odontol&oacute;gica, Refeit&oacute;rio no local e seguro de vida;<br />&bull; Hor&aacute;rio: Das 8h00 &agrave;s 15h00 de Segunda &agrave; Sexta;<br />&bull; Local de trabalho: Brooklin - zona sul S&atilde;o Paulo;<br />&bull; Modalidade contratual: Est&aacute;gio.</span></p>', 1, 'https://www.vagas.com.br/v939606', 1399403582, 1404097200, 1),
(149, 'estagiarios', 'Ingredionlogofeature3.jpg', 'Multinacional (Segmento agrícola)', 'Estágio em Benefícios', '<p align="justify">Somando a excel&ecirc;ncia, credibilidade e solidez do Grupo Meta RH, a Meta Talentos &eacute; especializada em Programas de Trainees, Estagi&aacute;rios e Desenvolvimento de Jovens Talentos. Selecionamos jovens com potencial, oferecendo oportunidades de desenvolvimento e crescimento em empresas renomadas, que os preparam para assumir no futuro, posi&ccedil;&otilde;es desafiadoras.<br /><br />Estamos assessorando empresa Multinacional, de grande porte, do segmento agr&iacute;cola, localizada na regi&atilde;o Sul de S&atilde;o Paulo, na contrata&ccedil;&atilde;o de:</p>\n<p><br /><br /><span id="Pr2Tn" style="font-family: Verdana, Arial; font-size: large;"><strong>Est&aacute;gio em Benef&iacute;cios</strong></span><br /><br /><span id="Pr2Tn" style="font-family: Verdana, Arial; font-size: small;"><strong>Atividades:</strong>&nbsp;<br /><br />&bull; Inclus&atilde;o e exclus&atilde;o de notas no sistema operacional SAP;<br />&bull; Controle de benef&iacute;cios (Vale Transporte; Vale Refei&ccedil;&atilde;o; Aux&iacute;lio Farm&aacute;cia);<br />&bull; Atendimento telef&ocirc;nico;<br />&bull; Atendimento pessoal ao cliente interno;<br />&bull; Esclarecimento de d&uacute;vidas.<br /><br /><strong>Requisitos:</strong>&nbsp;<br /><br />&bull; Forma&ccedil;&atilde;o: Cursando Administra&ccedil;&atilde;o de Empresa ou Economia;&nbsp;<br />&bull; Inform&aacute;tica: Excel intermedi&aacute;rio;&nbsp;<br />&bull; Idiomas: Ingl&ecirc;s intermedi&aacute;rio;&nbsp;<br />&bull; Experi&ecirc;ncia/Conhecimentos: N.A.&nbsp;<br /><br /><strong>Informa&ccedil;&otilde;es Adicionais:</strong><br /><br />&bull; Bolsa aux&iacute;lio: Vari&aacute;vel de acordo com o ano de forma&ccedil;&atilde;o;<br />&bull; Benef&iacute;cios: Vale transporte, vale refei&ccedil;&atilde;o, assist&ecirc;ncia m&eacute;dica, assist&ecirc;ncia odontol&oacute;gica e seguro de vida;<br />&bull; Hor&aacute;rio: Das 8h00 &agrave;s 15h00;<br />&bull; Local de trabalho: Zona Sul - Pr&oacute;ximo ao metr&ocirc; Concei&ccedil;&atilde;o;<br />&bull; Modalidade contratual: Est&aacute;gio.</span></p>', 1, 'https://www.vagas.com.br/v923802', 1399403692, 1404097200, 1),
(150, 'estagiarios', 'alcon5.jpg', 'Multinacional (Segmento oftalmológico)', 'Estágio em Administração de Vendas', '<table id="Pr2C" style="width: 100%; height: 100%;" border="0" cellspacing="0" cellpadding="2" align="center" bgcolor="#FFFFFF">\n<tbody>\n<tr>\n<td colspan="3" align="left" valign="top" width="80%">\n<p align="justify">Somando a excel&ecirc;ncia, credibilidade e solidez do Grupo Meta RH, a Meta Talentos &eacute; especializada em Programas de Trainees, Estagi&aacute;rios e Desenvolvimento de Jovens Talentos. Selecionamos jovens com potencial, oferecendo oportunidades de desenvolvimento e crescimento em empresas renomadas, que os preparam para assumir no futuro, posi&ccedil;&otilde;es desafiadoras.<br /><br />Estamos assessorando uma empresa L&iacute;der global em cuidados oftalmol&oacute;gicos, que desenvolve e fabrica medicamentos inovadores e dispositivos para servir o ciclo de vida completo das necessidades de cuidados dos olhos, localizada na regi&atilde;o sul de S&atilde;o Paulo, na contrata&ccedil;&atilde;o de:</p>\n<br /><br /><span id="Pr2Tn" style="font-family: Verdana, Arial; font-size: large;"><strong>Est&aacute;gio em Administra&ccedil;&atilde;o de Vendas</strong></span><br /><br /><span id="Pr2Tn" style="font-family: Verdana, Arial; font-size: small;"><strong>Atividades:</strong>&nbsp;<br /><br />&bull; Dar suporte no acompanhamento das despesas da For&ccedil;a de Vendas, gerando aprendizado em administrar budget or&ccedil;ado para um departamento;<br />&bull; Auxiliar na Provis&atilde;o e Reclassifica&ccedil;&atilde;o de Despesas da For&ccedil;a de Vendas, gerando aprendizado em planejamento e organiza&ccedil;&atilde;o;<br />&bull; Acompanhar e ter Controle de Eventos Realizados pela for&ccedil;a de Vendas, gerando habilidade de gerenciamento de atividades;<br />&bull; Dar suporte para gerar Relat&oacute;rios de trabalho de campo dos Gerentes de vendas.<br /><br /><br /><strong>Requisitos:</strong>&nbsp;<br /><br />&bull; Forma&ccedil;&atilde;o: Cursando Administra&ccedil;&atilde;o de Empresas ou Marketing;&nbsp;<br />&bull; Inform&aacute;tica: Pacote Office Intermedi&aacute;rio;&nbsp;<br />&bull; Idiomas: Ingl&ecirc;s Intermedi&aacute;rio;&nbsp;<br />&bull; Experi&ecirc;ncia/Conhecimentos: N.A.&nbsp;<br /><br /><strong>Informa&ccedil;&otilde;es Adicionais:</strong><br /><br />&bull; Bolsa aux&iacute;lio: R$ 1350,00;<br />&bull; Benef&iacute;cios: Assist&ecirc;ncia M&eacute;dica, Assist&ecirc;ncia Odontol&oacute;gica, Refeit&oacute;rio no Local e Seguro de vida;<br />&bull; Hor&aacute;rio: Das 8h00 &agrave;s 15h00 ou das 09h00 &agrave;s 16h00;<br />&bull; Local de trabalho: Santo Amaro - Zona Sul de S&atilde;o Paulo;<br />&bull; Modalidade contratual: Est&aacute;gio.</span></td>\n<td id="LtC" align="left" valign="middle" bgcolor="#3064C8" width="2%">&nbsp;</td>\n</tr>\n</tbody>\n</table>', 1, 'https://www.vagas.com.br/v900028', 1399404046, 1404097200, 1),
(151, 'estagiarios', 'alcon6.jpg', 'Multinacional (Segmento oftalmológico)', 'Estágio em Recursos Humanos', '<p align="justify">Somando a excel&ecirc;ncia, credibilidade e solidez do Grupo Meta RH, a Meta Talentos &eacute; especializada em Programas de Trainees, Estagi&aacute;rios e Menores Aprendizes, denominados Jovens Talentos. Selecionamos jovens com potencial, oferecendo oportunidades de desenvolvimento e crescimento em empresas renomadas, que os preparam para assumir no futuro, posi&ccedil;&otilde;es desafiadoras.<br /><br />Estamos assessorando uma empresa L&iacute;der global em cuidados oftalmol&oacute;gicos, que desenvolve e fabrica medicamentos inovadores e dispositivos para servir o ciclo de vida completo das necessidades de cuidados dos olhos.&nbsp;</p>\n<p><br /><br /><span id="Pr2Tn" style="font-family: Verdana, Arial; font-size: large;"><strong>Est&aacute;gio em Recursos Humanos</strong></span><br /><br /><span id="Pr2Tn" style="font-family: Verdana, Arial; font-size: small;"><strong>Atividades:</strong>&nbsp;<br /><br />&bull; Suporte no Preenchimento de Formul&aacute;rios relacionados a Servi&ccedil;os de RH;<br />&bull; Suporte no Atendimento aos associados;<br />&bull; Montagem de Fluxograma das atividades da &aacute;rea de RH;<br />&bull; Suporte na divulga&ccedil;&atilde;o e cumprimento das Pol&iacute;ticas Internas de RH;;<br />&bull; Suporte no Controle do Fluxo de Processos Seletivo da For&ccedil;a de Vendas;<br />&bull; Follow-up dos Processo de Contrata&ccedil;&atilde;o junto &agrave;s &aacute;reas de Neg&oacute;cio.<br /><br /><strong>Requisitos:</strong>&nbsp;<br /><br />&bull; Forma&ccedil;&atilde;o: Cursando Administra&ccedil;&atilde;o ou Psicologia;&nbsp;<br />&bull; Inform&aacute;tica: Pacote Office intermedi&aacute;rio;&nbsp;<br />&bull; Idiomas: Ingl&ecirc;s Intermedi&aacute;rio;&nbsp;<br />&bull; Experi&ecirc;ncia/Conhecimentos: N.A.&nbsp;<br /><br /><strong>Informa&ccedil;&otilde;es Adicionais:</strong><br /><br />&bull; Bolsa aux&iacute;lio: R$ 1350,00;<br />&bull; Benef&iacute;cios: Assist&ecirc;ncia m&eacute;dica, Assist&ecirc;ncia odontol&oacute;gica, Refeit&oacute;rio no local e Seguro de vida;<br />&bull; Hor&aacute;rio: Das 8h00 &agrave;s 15h00 ou das 09h00 &agrave;s 16h00;<br />&bull; Local de trabalho: Santo Amaro - Zona Sul de S&atilde;o Paulo;<br />&bull; Modalidade contratual: Est&aacute;gio.</span></p>', 1, 'https://www.vagas.com.br/v899983', 1399404129, 1404097200, 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `vagas_programas`
--

CREATE TABLE `vagas_programas` (
  `id` int(11) NOT NULL,
  `titulo` varchar(255) DEFAULT NULL,
  `slug` varchar(255) DEFAULT NULL,
  `vagas_empresas_id` varchar(45) DEFAULT NULL,
  `tipo` varchar(45) DEFAULT NULL,
  `data_cadastro` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `vagas_programas`
--

INSERT INTO `vagas_programas` (`id`, `titulo`, `slug`, `vagas_empresas_id`, `tipo`, `data_cadastro`) VALUES
(5, 'Programa de Estágio Alcon', 'Programa-de-Estgio-Alcon', '6', 'estagio', '2014-05-12'),
(6, 'Programa de Estágio ICTS', 'Programa-de-Estgio-ICTS', '4', 'estagio', '2014-05-15'),
(8, 'Programa de Estágio Novartis', 'Programa-de-Estgio-Novartis', '3', 'estagio', '2014-04-30'),
(11, 'Processo de Estágio ICL', 'Processo-de-Estgio-ICL', '24', 'pestagio', '2014-05-01'),
(12, 'Processo de Estágio Diversey', 'Processo-de-Estgio-Diversey', '25', 'pestagio', '2014-04-10'),
(13, 'Processo de Estágio Odebrecht', 'Processo-de-Estgio-Odebrecht', '13', 'pestagio', '2014-05-07'),
(14, 'Programa de Trainee Parker', 'Programa-de-Trainee-Parker', '27', 'trainee', '2014-04-18');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `ci_sessions`
--
ALTER TABLE `ci_sessions`
  ADD PRIMARY KEY (`session_id`);

--
-- Indexes for table `clientes`
--
ALTER TABLE `clientes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `home_banners`
--
ALTER TABLE `home_banners`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `home_bannersingles`
--
ALTER TABLE `home_bannersingles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `home_programas`
--
ALTER TABLE `home_programas`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `login_attempts`
--
ALTER TABLE `login_attempts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `newsletters`
--
ALTER TABLE `newsletters`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `noticias`
--
ALTER TABLE `noticias`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `politica_privacidade`
--
ALTER TABLE `politica_privacidade`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `programas`
--
ALTER TABLE `programas`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `programa_metodista`
--
ALTER TABLE `programa_metodista`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_autologin`
--
ALTER TABLE `user_autologin`
  ADD PRIMARY KEY (`key_id`,`user_id`);

--
-- Indexes for table `user_profiles`
--
ALTER TABLE `user_profiles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vagas`
--
ALTER TABLE `vagas`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vagas_cursos`
--
ALTER TABLE `vagas_cursos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vagas_empresas`
--
ALTER TABLE `vagas_empresas`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vagas_novo`
--
ALTER TABLE `vagas_novo`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vagas_programas`
--
ALTER TABLE `vagas_programas`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `clientes`
--
ALTER TABLE `clientes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `home_banners`
--
ALTER TABLE `home_banners`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;
--
-- AUTO_INCREMENT for table `home_bannersingles`
--
ALTER TABLE `home_bannersingles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `home_programas`
--
ALTER TABLE `home_programas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `login_attempts`
--
ALTER TABLE `login_attempts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `newsletters`
--
ALTER TABLE `newsletters`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=275;
--
-- AUTO_INCREMENT for table `noticias`
--
ALTER TABLE `noticias`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;
--
-- AUTO_INCREMENT for table `politica_privacidade`
--
ALTER TABLE `politica_privacidade`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `programas`
--
ALTER TABLE `programas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;
--
-- AUTO_INCREMENT for table `programa_metodista`
--
ALTER TABLE `programa_metodista`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `user_profiles`
--
ALTER TABLE `user_profiles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `vagas`
--
ALTER TABLE `vagas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=395;
--
-- AUTO_INCREMENT for table `vagas_cursos`
--
ALTER TABLE `vagas_cursos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=78;
--
-- AUTO_INCREMENT for table `vagas_empresas`
--
ALTER TABLE `vagas_empresas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=60;
--
-- AUTO_INCREMENT for table `vagas_novo`
--
ALTER TABLE `vagas_novo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=152;
--
-- AUTO_INCREMENT for table `vagas_programas`
--
ALTER TABLE `vagas_programas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
