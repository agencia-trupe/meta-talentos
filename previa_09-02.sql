-- phpMyAdmin SQL Dump
-- version 4.5.3.1
-- http://www.phpmyadmin.net
--
-- Host: prev_talentos.mysql.dbaas.com.br
-- Generation Time: 09-Fev-2017 às 13:49
-- Versão do servidor: 5.6.33-79.0-log
-- PHP Version: 5.6.29-0+deb8u1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `prev_talentos`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `banners`
--

CREATE TABLE `banners` (
  `id` int(10) UNSIGNED NOT NULL,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `titulo` text COLLATE utf8_unicode_ci NOT NULL,
  `texto` text COLLATE utf8_unicode_ci NOT NULL,
  `texto_botao` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `link_botao` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `banners`
--

INSERT INTO `banners` (`id`, `ordem`, `imagem`, `titulo`, `texto`, `texto_botao`, `link_botao`, `created_at`, `updated_at`) VALUES
(1, 1, 'teste_20170207195309.jpg', 'Seu futuro profissional<br />\r\ncome&ccedil;a com o Est&aacute;gio', 'Esteja preparado e sempre conectado para n&atilde;o perder as oportunidades!', 'CONFIRA NOSSAS VAGAS »', '#', '2017-02-06 18:56:19', '2017-02-07 21:53:09'),
(2, 2, 'bannerhome2_20170207200134.jpg', 'O ingresso perfeito no<br />\r\nmercado de trabalho pode<br />\r\nser como Trainee', 'Acompanhe todas as oportunidades que a Meta Talentos pode oferecer', 'CONFIRA NOSSAS VAGAS »', '#', '2017-02-06 18:57:26', '2017-02-07 22:01:34'),
(4, 3, 'banner3_20170207195600.jpg', 'O pontap&eacute; inicial da sua&nbsp;carreira &eacute; o Est&aacute;gio', 'Conte com a Meta Talentos neste&nbsp;momento importante da sua forma&ccedil;&atilde;o&nbsp;e acompanhe as oportunidades aqui!', 'Confira nossas vagas', '#', '2017-02-07 21:56:00', '2017-02-07 21:56:00'),
(5, 4, 'banner4_20170207195722.jpg', 'Desenvolvimento&nbsp;de Jovens Talentos', 'A Meta Talentos tem programas&nbsp;especiais e profissionais de alto n&iacute;vel&nbsp;para identificar e desenvolver o potencial &nbsp;de jovens em in&iacute;cio de carreira<br />\r\n&nbsp;', 'Confira nossas vagas', '#', '2017-02-07 21:57:23', '2017-02-07 21:57:23');

-- --------------------------------------------------------

--
-- Estrutura da tabela `cases_clientes`
--

CREATE TABLE `cases_clientes` (
  `id` int(10) UNSIGNED NOT NULL,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `empresa` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `marca` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `titulo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `texto` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `cases_clientes`
--

INSERT INTO `cases_clientes` (`id`, `ordem`, `empresa`, `marca`, `titulo`, `texto`, `created_at`, `updated_at`) VALUES
(1, 1, 'Alcon', 'alcon_20170207191422.png.png', 'Projetos de Estágios', '<p><strong>S&oacute;lida parceria</strong>&nbsp;h&aacute;&nbsp;<strong>mais de 30 anos</strong>&nbsp;entre a Alcon e o Grupo Meta RH tem permitido&nbsp;<strong>excelentes resultados</strong>&nbsp;nos mais variados projetos de recrutamento e sele&ccedil;&atilde;o, especialmente nos&nbsp;<strong>programas de est&aacute;gios</strong>&nbsp;conduzidos pela divis&atilde;o&nbsp;<strong>Meta Talentos</strong>&nbsp;que tem adotado&nbsp;<strong>modernas t&eacute;cnicas de atra&ccedil;&atilde;o e sele&ccedil;&atilde;o</strong>&nbsp;de jovens profissionais.</p>\r\n', '2017-02-07 21:14:22', '2017-02-07 21:14:22'),
(2, 2, 'Fleury', 'fleury_20170207191515.png.png', 'Atração de Talentos', '<p>O&nbsp;<strong>Grupo Meta RH</strong>, atrav&eacute;s da&nbsp;<strong>Meta Talentos</strong>, tem a satisfa&ccedil;&atilde;o de atender o&nbsp;<strong>Fleury &ndash; Medicina e Sa&uacute;de</strong>, empresa h&aacute; 90 anos no atuando segmento de an&aacute;lises cl&iacute;nicas. Reconhecida por sua inova&ccedil;&atilde;o e pioneirismo, conta com renomados especialistas e est&aacute; entre os maiores e mais respeitados laborat&oacute;rios do pa&iacute;s.</p>\r\n\r\n<p>Conduzindo processos de recrutamento e sele&ccedil;&atilde;o de estagi&aacute;rios, a Meta Talentos busca&nbsp;<strong>fortalecer cada vez mais</strong>&nbsp;a parceria com o Fleury, oferecendo jovens talentos capazes de, futuramente,&nbsp;<strong>assumir posi&ccedil;&otilde;es de lideran&ccedil;a</strong>.</p>\r\n', '2017-02-07 21:15:15', '2017-02-07 21:15:15'),
(3, 3, 'Air Liquide', 'air-liquide_20170207191549.png.png', 'Seleção de Estagiários', '<p>H&aacute; mais de 70 anos no Brasil, a&nbsp;<strong>Air Liquide</strong>&nbsp;&ndash; empresa francesa l&iacute;der mundial em gases, tecnologias e servi&ccedil;os para a Ind&uacute;stria - possui h&aacute; mais de 5 anos&nbsp;<strong>uma bem-sucedida parceria</strong>&nbsp;com a&nbsp;<strong>Meta Talentos</strong>, empresa do&nbsp;<strong>Grupo Meta RH</strong>, no fornecimento de m&atilde;o de obra em &acirc;mbito nacional para diversas &aacute;reas da companhia.</p>\r\n\r\n<p>A Meta Talentos se orgulha da&nbsp;<strong>parceria estrat&eacute;gica</strong>&nbsp;que possui com a Air Liquide, conduzindo processos de recrutamento e sele&ccedil;&atilde;o de estagi&aacute;rios nos mais variados campos de gradua&ccedil;&atilde;o, de forma a atrair&nbsp;<strong>jovens profissionais</strong>&nbsp;capazes de assumir&nbsp;<strong>posi&ccedil;&otilde;es de lideran&ccedil;a</strong>&nbsp;no futuro.</p>\r\n', '2017-02-07 21:15:49', '2017-02-07 21:15:49'),
(4, 4, 'Eastman', 'eastman_20170207191941.png.png', 'Vagas de estágios', '<p>A&nbsp;<strong>Meta Talentos</strong>, empresa do&nbsp;<strong>Grupo Meta RH</strong>, respons&aacute;vel pelo recrutamento e sele&ccedil;&atilde;o, treinamento e desenvolvimento de jovens talentos, possui h&aacute; mais de 10 anos uma&nbsp;<strong>s&oacute;lida parceria</strong>&nbsp;com a&nbsp;<strong>Eastman</strong>, empresa global de especialidades qu&iacute;micas h&aacute; mais de 90 anos no mercado.</p>\r\n\r\n<p>Atendendo de forma&nbsp;<strong>pontual e assertiva</strong>&nbsp;as demandas de recrutamento e sele&ccedil;&atilde;o de estagi&aacute;rios da Eastman, a Meta Talentos se orgulha de manter esta<strong>&nbsp;parceria duradoura e de sucesso.&nbsp;</strong></p>\r\n', '2017-02-07 21:19:41', '2017-02-07 21:19:41'),
(5, 5, 'ICTS', 'icts_20170207192011.jpg.png', 'Programas de Estágios', '<p>A&nbsp;<strong>ICTS</strong>&nbsp;&eacute; uma&nbsp;<strong>empresa global</strong>&nbsp;de consultoria e servi&ccedil;os em gest&atilde;o de riscos de neg&oacute;cios, &eacute;tica empresarial, intelig&ecirc;ncia empresarial, prote&ccedil;&atilde;o de informa&ccedil;&otilde;es sens&iacute;veis e prote&ccedil;&atilde;o executiva integrada. Pela natureza dos servi&ccedil;os que presta, a empresa buscava no mercado um&nbsp;<strong>parceiro confi&aacute;vel</strong>&nbsp;para recrutar jovens profissionais e&nbsp;<strong>confiou &agrave; Meta Talentos</strong>&nbsp;seu programa de est&aacute;gios num projeto que foi feito com&nbsp;<strong>alta qualidade e assertividade</strong>.</p>\r\n', '2017-02-07 21:20:11', '2017-02-07 21:20:11'),
(6, 6, 'Novartis', 'novartis_20170207192047.jpg.png', 'Projetos de Estágios', '<p>Com mais de<strong>&nbsp;30 anos de parceria</strong>, o&nbsp;<strong>Grupo Meta RH</strong>&nbsp;atende a&nbsp;<strong>Novartis</strong>&nbsp;- empresa multinacional, conceituada entre as maiores do mundo no segmento farmac&ecirc;utico - nas variadas demandas de Recrutamento e Sele&ccedil;&atilde;o.</p>\r\n\r\n<p>Por meio da&nbsp;<strong>Meta Talentos</strong>&nbsp;atende, de forma eficiente e r&aacute;pida, na contrata&ccedil;&atilde;o de estagi&aacute;rios para assumirem posi&ccedil;&otilde;es em diversas &aacute;reas na companhia, resultando em coloca&ccedil;&otilde;es&nbsp;<strong>assertivas</strong>&nbsp;e no fortalecimento desta&nbsp;<strong>bem-sucedida parceria</strong>.</p>\r\n', '2017-02-07 21:20:47', '2017-02-07 21:20:47'),
(7, 7, 'Odebrecht', 'odebrecht_20170207192150.jpg.png', 'Seleção de Estagiários', '<p>A&nbsp;<strong>Meta Talentos</strong>&nbsp;tem sido&nbsp;<strong>parceira</strong>&nbsp;da Odebrecht na sele&ccedil;&atilde;o de&nbsp;<strong>jovens profissionais</strong>&nbsp;para diversas &aacute;reas da empresa, buscando atrair talentos de renomadas universidades para que possam&nbsp;<strong>desenvolver suas carreiras</strong>&nbsp;num ambiente que oferece muitos&nbsp;<strong>desafios</strong>&nbsp;de aprendizagem e&nbsp;<strong>oportunidades</strong>&nbsp;de crescimento</p>\r\n', '2017-02-07 21:21:50', '2017-02-07 21:21:50'),
(8, 8, 'Parker', 'parker_20170207192216.jpg.png', 'Programa Trainee', '<p>A Divis&atilde;o de Talentos do Grupo Meta RH conduziu o processo do&nbsp;<strong>Programa de Trainees</strong>, com o objetivo de trazer &agrave; empresa Novos Talentos que pudessem ocupar&nbsp;<strong>cargos de lideran&ccedil;a</strong>&nbsp;ao t&eacute;rmino desse programa. Adotando uma pol&iacute;tica de efici&ecirc;ncia, qualidade e rapidez o programa&nbsp;<strong>recrutou 25 trainees</strong>&nbsp;em um processo assertivo e bem sucedido.</p>\r\n', '2017-02-07 21:22:16', '2017-02-07 21:22:16'),
(9, 9, 'Sealed Air', 'sealed-air_20170207192314.png.png', 'Processos de Recrutamento e Seleção', '<p>O&nbsp;<strong>Grupo Meta RH</strong>, possu&iacute; h&aacute; mais de 20 anos uma s&oacute;lida parceria com o grupo&nbsp;<strong>Sealed Air</strong>, empresa l&iacute;der global de solu&ccedil;&otilde;es de embalagens h&aacute; mais de 50 anos no mercado.</p>\r\n\r\n<p>Atendendo de forma pontual e assertiva as demandas de recrutamento e sele&ccedil;&atilde;o de estagi&aacute;rios da Sealed Air, a Meta Talentos se orgulha de manter esta parceria duradoura e de sucesso.&nbsp;</p>\r\n', '2017-02-07 21:23:14', '2017-02-07 21:23:14'),
(10, 10, 'Serasa', 'serasa_20170207192341.jpg.png', 'Atração e Seleção', '<p>A&nbsp;<strong>Serasa Experian</strong>, um dos maiores provedores de solu&ccedil;&otilde;es de cr&eacute;dito do mundo, &eacute; parceira do&nbsp;<strong>Grupo Meta RH</strong>&nbsp;nos projetos de Recrutamento e Sele&ccedil;&atilde;o de profissionais efetivos para as &aacute;reas de capta&ccedil;&atilde;o de dados, administrativa, jur&iacute;dica e vendas.</p>\r\n\r\n<p><strong>Ampliando parceria de sucesso</strong>&nbsp;com a Serasa Experian, o Grupo Meta RH &ndash; atrav&eacute;s da divis&atilde;o Meta Talentos &ndash; atende as demandas de estagi&aacute;rios na corporativa com&nbsp;<strong>processos personalizados, assertivos e com foco no neg&oacute;cio do cliente</strong>.</p>\r\n', '2017-02-07 21:23:41', '2017-02-07 21:23:41');

-- --------------------------------------------------------

--
-- Estrutura da tabela `contato`
--

CREATE TABLE `contato` (
  `id` int(10) UNSIGNED NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `telefone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `endereco` text COLLATE utf8_unicode_ci NOT NULL,
  `google_maps` text COLLATE utf8_unicode_ci NOT NULL,
  `facebook` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `twitter` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `linkedin` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `contato`
--

INSERT INTO `contato` (`id`, `email`, `telefone`, `endereco`, `google_maps`, `facebook`, `twitter`, `linkedin`, `created_at`, `updated_at`) VALUES
(1, 'contato@trupe.net', '+55 11 5525-2711', 'Av. Adolfo Pinheiro, 1001<br />\r\nSanto Amaro<br />\r\n04733-100&nbsp;&middot; S&atilde;o Paulo/SP', '<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3654.8741877156017!2d-46.7029260850206!3d-23.644676284641683!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x94ce50f9b692f44d%3A0x186bbf6503ad4ecc!2sAv.+Adolfo+Pinheiro%2C+1001+-+Santo+Amaro%2C+S%C3%A3o+Paulo+-+SP!5e0!3m2!1spt-BR!2sbr!4v1486400050127" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>', '#', '#', '#', NULL, '2017-02-06 18:54:15');

-- --------------------------------------------------------

--
-- Estrutura da tabela `contatos_recebidos`
--

CREATE TABLE `contatos_recebidos` (
  `id` int(10) UNSIGNED NOT NULL,
  `nome` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `telefone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `mensagem` text COLLATE utf8_unicode_ci NOT NULL,
  `lido` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `diferencial`
--

CREATE TABLE `diferencial` (
  `id` int(10) UNSIGNED NOT NULL,
  `texto` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `diferencial`
--

INSERT INTO `diferencial` (`id`, `texto`, `created_at`, `updated_at`) VALUES
(1, '<p>Prezamos por:&nbsp;</p>\r\n\r\n<ul>\r\n	<li><strong>Inova&ccedil;&atilde;o </strong>permanente dos processos utilizando novas tecnologias e conceitos para a fase presencial e on line.</li>\r\n	<li><strong>Gest&atilde;o </strong>de v&aacute;rias etapas do processo seletivo via web</li>\r\n	<li><strong>Ferramentas de sele&ccedil;&atilde;o</strong> mais atrativas com jogos e/ou cases atrelados ao neg&oacute;cio da empresa.</li>\r\n	<li><strong>Metodologia </strong>de sele&ccedil;&atilde;o por compet&ecirc;ncias e valores.</li>\r\n	<li><strong>Atrair </strong>e selecionar profissionais aderentes a cultura das empresas</li>\r\n	<li>&Eacute; uma premissa para n&oacute;s darmos <strong>retorno </strong>a todos os candidatos em cada etapa do processo</li>\r\n	<li><strong>Relacionamento </strong>com o cliente embasado nos pilares: &Eacute;tica, respeito &agrave; diversidade, transpar&ecirc;ncia, perenidade nas rela&ccedil;&otilde;es e busca de objetivos compartilhados e sin&eacute;rgicos.</li>\r\n</ul>\r\n', NULL, '2017-02-07 21:47:21');

-- --------------------------------------------------------

--
-- Estrutura da tabela `link_oportunidades`
--

CREATE TABLE `link_oportunidades` (
  `id` int(10) UNSIGNED NOT NULL,
  `link` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `link_oportunidades`
--

INSERT INTO `link_oportunidades` (`id`, `link`, `created_at`, `updated_at`) VALUES
(1, 'http://metatalentos.selecty.com.br/', NULL, '2017-02-08 20:07:20');

-- --------------------------------------------------------

--
-- Estrutura da tabela `migrations`
--

CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2016_02_01_000000_create_users_table', 1),
('2016_03_01_000000_create_contato_table', 1),
('2016_03_01_000000_create_contatos_recebidos_table', 1),
('2017_01_31_132556_create_banners_table', 1),
('2017_01_31_133015_create_politica_de_privacidade_table', 1),
('2017_01_31_133539_create_solucoes_table', 1),
('2017_01_31_134428_create_cases_clientes_table', 1),
('2017_01_31_135634_create_diferencial_table', 1),
('2017_01_31_165058_create_perfil_table', 1),
('2017_01_31_180033_create_oportunidades_table', 1),
('2017_02_02_170833_create_link_oportunidades_table', 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `oportunidades`
--

CREATE TABLE `oportunidades` (
  `id` int(10) UNSIGNED NOT NULL,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `tipo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `empresa` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `marca` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `vaga` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `link` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `perfil`
--

CREATE TABLE `perfil` (
  `id` int(10) UNSIGNED NOT NULL,
  `texto` text COLLATE utf8_unicode_ci NOT NULL,
  `imagem_1` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `imagem_2` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `perfil`
--

INSERT INTO `perfil` (`id`, `texto`, `imagem_1`, `imagem_2`, `created_at`, `updated_at`) VALUES
(1, '<p><strong>A META TALENTOS</strong></p>\r\n\r\n<p>&Eacute; uma empresa especializada em Programas de Trainees, Estagi&aacute;rios e Desenvolvimento de Jovens Talentos. Selecionamos jovens com potencial, oferecendo oportunidades de desenvolvimento e crescimento em empresas renomadas, que os preparam para assumir no futuro, posi&ccedil;&otilde;es desafiadoras.</p>\r\n\r\n<p>Para cada programa, disponibilizamos uma equipe de profissionais altamente qualificados para efetuar toda a estrutura&ccedil;&atilde;o do processo: alinhamento de expectativas com o Gestor (Mentor) na empresa, divulga&ccedil;&atilde;o, inscri&ccedil;&atilde;o, provas online, din&acirc;micas, entrevistas e fechamento dos programas. Em todas as etapas fazemos o acompanhamento com a empresa cliente e com os candidatos.</p>\r\n\r\n<p>A estrutura&ccedil;&atilde;o dos programas e a elabora&ccedil;&atilde;o das atividades s&atilde;o feitas de forma criativa e de acordo com o p&uacute;blico jovem. Disponibilizamos um espa&ccedil;o moderno e ferramentas de alta qualidade. Os candidatos ser&atilde;o recebidos com todo conforto e aten&ccedil;&atilde;o que precisam para o desenvolvimento das atividades de sele&ccedil;&atilde;o.</p>\r\n', 'mapa-atuamos-20170206165449_20170206174835.png', 'fluxo-empresa-20170206165449_20170206174835.png', NULL, '2017-02-06 19:48:35');

-- --------------------------------------------------------

--
-- Estrutura da tabela `politica_de_privacidade`
--

CREATE TABLE `politica_de_privacidade` (
  `id` int(10) UNSIGNED NOT NULL,
  `texto` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `politica_de_privacidade`
--

INSERT INTO `politica_de_privacidade` (`id`, `texto`, `created_at`, `updated_at`) VALUES
(1, '<h2>Lorem ipsum dolor sit amet, consectetur adipiscing elit</h2>\r\n\r\n<p>Aliquam congue nisl a est mollis euismod. Proin tempus rhoncus erat non ultrices. Mauris ut rhoncus lectus. Sed placerat massa id odio imperdiet rhoncus vel eu metus. Morbi fermentum eget lacus eget condimentum. Etiam mollis tincidunt rutrum. Mauris eget erat quam. Nullam suscipit dapibus eros, non euismod eros porttitor id. Pellentesque a odio dolor.</p>\r\n\r\n<p>Curabitur ornare egestas nisl eu fermentum. Ut eu odio pulvinar, auctor ligula laoreet, tincidunt est</p>\r\n\r\n<h3>Sed fermentum orci a quam consectetur ultrices</h3>\r\n\r\n<p>Mauris non lacinia tellus. Donec ornare diam quis felis dignissim lobortis. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Suspendisse eros purus, sollicitudin nec mattis in, pretium quis elit. Fusce rutrum condimentum sollicitudin. Suspendisse potenti. Quisque egestas augue vel porta sodales. Sed at rhoncus ex.</p>\r\n\r\n<p>Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Suspendisse lacinia, nibh consectetur pellentesque eleifend, ipsum lectus lacinia velit, eget lacinia urna neque vitae leo. Suspendisse potenti. Maecenas dapibus dignissim ultrices. Maecenas metus urna, accumsan a feugiat quis, suscipit nec turpis. Morbi luctus volutpat mi. Nulla ut mi nec risus molestie interdum vitae quis nunc. Nam commodo orci vel faucibus eleifend. Vivamus nec eros eget nibh scelerisque mollis non sit amet neque. In consectetur mattis augue, non dapibus purus porta et. Aliquam erat volutpat. Etiam dui enim, ultricies tincidunt volutpat eget, rhoncus et ipsum. Aenean est velit, placerat tempus urna ac, gravida consectetur erat. Suspendisse ut libero sed purus varius varius. Maecenas tempor enim laoreet arcu sagittis, nec mattis ante tristique. Donec interdum sapien et mi gravida, in euismod est laoreet.</p>\r\n\r\n<p>Proin in porttitor mi, id vehicula metus. Maecenas a blandit dui, ac congue felis. Vivamus iaculis eros at lacus pulvinar pellentesque. Vestibulum sed dictum mauris. Donec nec risus vulputate, feugiat est nec, cursus est. Mauris rhoncus consectetur blandit. In sed fermentum ipsum. Duis semper felis quis neque imperdiet, at dictum sapien pellentesque. Integer fermentum dui vel nisl blandit, vel imperdiet lacus gravida. Sed aliquet, eros eget pretium pellentesque, nisl purus volutpat sem, eu hendrerit odio eros in massa. In ac tortor sed orci scelerisque sagittis lobortis eget sapien. Quisque vestibulum velit ac facilisis consequat.</p>\r\n', NULL, '2017-02-06 18:52:50');

-- --------------------------------------------------------

--
-- Estrutura da tabela `solucoes`
--

CREATE TABLE `solucoes` (
  `id` int(10) UNSIGNED NOT NULL,
  `titulo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `subtitulo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `texto` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `solucoes`
--

INSERT INTO `solucoes` (`id`, `titulo`, `slug`, `subtitulo`, `texto`, `created_at`, `updated_at`) VALUES
(1, 'Atração e Seleção', 'atracao-e-selecao', 'Programas de Estágio e Trainee', '<p>Conduzimos Programas de Estagi&aacute;rios e Trainees e contrata&ccedil;&otilde;es pontuais de Est&aacute;gio.</p>\r\n\r\n<p>Nosso processo de atra&ccedil;&atilde;o e sele&ccedil;&atilde;o &eacute; desenhado, considerando a metodologia de Sele&ccedil;&atilde;o por Compet&ecirc;ncias e an&aacute;lise de Valores pessoais e organizacionais. Cuidamos de cada fase buscando excel&ecirc;ncia no atendimento para os nossos clientes: Empresa e Candidato.&nbsp;</p>\r\n\r\n<p>A gest&atilde;o do processo &eacute; feita atrav&eacute;s de um sistema que garante a realiza&ccedil;&atilde;o e acompanhamento eficaz em cada uma das etapas do processo.</p>\r\n\r\n<p>Estabelecemos parcerias chaves no mercado, visando potencializar resultados nas fases on line e presencial, utilizando ferramentas modernas e inovadoras para garantir resultados, alinhado a estrat&eacute;gia organizacional.</p>\r\n', NULL, '2017-02-07 21:48:04'),
(2, 'Desenvolvimento de Jovens Talentos', 'desenvolvimento-de-jovens-talentos', 'Workshops & Desenvolvimento', '<h2>Workshops para Participantes do Programa de Trainees ou Estagi&aacute;rios e seus Gestores</h2>\r\n\r\n<h3>Workshop de Integra&ccedil;&atilde;o dos Participantes do Programa de Trainees ou Estagi&aacute;rios&nbsp;</h3>\r\n\r\n<p><br />\r\nAp&oacute;s a sele&ccedil;&atilde;o, os trainees/estagi&aacute;rios necessitam conhecer a empresa e o que &eacute; esperado deles tanto a n&iacute;vel t&eacute;cnico como comportamental.&nbsp;</p>\r\n\r\n<p>Oferecemos um workshop de 4 a 8 horas desenhado em parceria com o RH das empresas que fornecer&aacute; informa&ccedil;&otilde;es sobre a mesma e sobre a cultura da empresa. Utilizamos uma metodologia pr&aacute;tica baseada em Andragogia e os workshops s&atilde;o ministrados por consultores especializados. O objetivo esperado &eacute; que os participantes do programa de trainees ou estagi&aacute;rios possam conhecer a empresa, o que &eacute; esperado deles no programa tanto em n&iacute;vel t&eacute;cnico quanto comportamental, como deve ser rela&ccedil;&atilde;o com os seus gestores e quais s&atilde;o as perspectivas de futuro na empresa.&nbsp;</p>\r\n\r\n<h3>Workshop para Gestores dos Trainees ou Estagi&aacute;rios</h3>\r\n\r\n<p><br />\r\nO sucesso de um Programa de Trainees e Estagi&aacute;rios depende, em grande parte, da gest&atilde;o por parte dos mentores e gestores. Em muitos casos, os gestores dos trainees ou estagi&aacute;rios n&atilde;o conhecem o seu papel no processo e, portanto, nem sempre conseguem desenvolver os jovens para que atinjam todo o seu potencial.&nbsp;</p>\r\n\r\n<p>Oferecemos um workshop de 8 horas para que os gestores possam conhecer o seu papel, como melhor desenvolver os jovens que est&atilde;o sob sua responsabilidade. Os temas a serem tratados no workshop depender&atilde;o de diagn&oacute;stico feito junto &agrave; empresa. Temas que usualmente abordamos: objetivos do programa de trainees ou de estagi&aacute;rios para a empresa, o papel do gestor, como lidar com diferen&ccedil;as de gera&ccedil;&otilde;es, como lidar com as expectativas dos jovens trainees ou estagi&aacute;rios, desenvolvimento de talentos, no&ccedil;&otilde;es de coaching, avalia&ccedil;&atilde;o de desempenho e feedback.&nbsp;</p>\r\n\r\n<h2><br />\r\nDesenvolvimento de Talentos e Futuros L&iacute;deres</h2>\r\n\r\n<p><br />\r\nA Divis&atilde;o Meta Executivos &ndash; Desenvolvimento Organizacional oferece solu&ccedil;&otilde;es de desenvolvimento de jovens talentos e futuros l&iacute;deres incluindo palestras, workshops e coaching executivo e de carreira. Atuamos de forma customizada e com consultores especializados, a partir de um diagn&oacute;stico com a &aacute;rea de RH da empresa e os gestores dos jovens talentos para criar workshops espec&iacute;ficos ou um programa de desenvolvimento a longo prazo visando a reten&ccedil;&atilde;o e a transforma&ccedil;&atilde;o desses talentos.&nbsp;</p>\r\n', NULL, '2017-02-07 21:50:51');

-- --------------------------------------------------------

--
-- Estrutura da tabela `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'trupe', 'contato@trupe.net', '$2y$10$2uGozL7gvkcqrI.hxPH8YeFC/JxNC1lE3yJsf8qVZtLHNTwxPllve', '67GRmzgnfTJUca7hi4zoWkKswVODHnYIn94WZvhnoj9SGqcT6XUKV1gT0CsL', NULL, '2017-02-07 22:06:27'),
(2, 'meta', 'contato@metatalentos.com.br', '$2y$10$F6ElRkBKP6Bnhw.dhxt0telg82xGhiV/z2AvhyXPwaBRsL.jQK8N6', 'nQ2wl5rII2WMNarJtNUsDm8JhGer0Y5LUqbXm4cf38QSsDySWHRBMWxHq3RX', '2017-02-07 22:06:23', '2017-02-07 22:06:40');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `banners`
--
ALTER TABLE `banners`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cases_clientes`
--
ALTER TABLE `cases_clientes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contato`
--
ALTER TABLE `contato`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contatos_recebidos`
--
ALTER TABLE `contatos_recebidos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `diferencial`
--
ALTER TABLE `diferencial`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `link_oportunidades`
--
ALTER TABLE `link_oportunidades`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oportunidades`
--
ALTER TABLE `oportunidades`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `perfil`
--
ALTER TABLE `perfil`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `politica_de_privacidade`
--
ALTER TABLE `politica_de_privacidade`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `solucoes`
--
ALTER TABLE `solucoes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `banners`
--
ALTER TABLE `banners`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `cases_clientes`
--
ALTER TABLE `cases_clientes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `contato`
--
ALTER TABLE `contato`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `contatos_recebidos`
--
ALTER TABLE `contatos_recebidos`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `diferencial`
--
ALTER TABLE `diferencial`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `link_oportunidades`
--
ALTER TABLE `link_oportunidades`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `oportunidades`
--
ALTER TABLE `oportunidades`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `perfil`
--
ALTER TABLE `perfil`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `politica_de_privacidade`
--
ALTER TABLE `politica_de_privacidade`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `solucoes`
--
ALTER TABLE `solucoes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
